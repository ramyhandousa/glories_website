// ======================== header=====================
function openNav() {
    document.getElementById("mySidenav").style.width = "313px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
// ======================== show password =====================
function showLogin() {
    var x = document.getElementById("login-password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}


function showSignup() {
    var x = document.getElementById("password");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}
function showSignupVerify() {
    var x = document.getElementById("password-verify");
    if (x.type === "password") {
        x.type = "text";
    } else {
        x.type = "password";
    }
}

// ================================ signup verify password===============

$(document).ready(function () {
    password = $('#password');
    password2 = $('#password-verify');

    $( password2 ).one( "click", function() {
        showWarning("تاكد من تاكيد كلمة المرور لاستكمال البيانات");
    });

    $('#password, #password-verify').on('keyup', function () {



        if ($('#password').val() == $('#password-verify').val()) {

            $('.controlButton').show();
        } else

            $('.controlButton').hide();
    });


    // $('.next').on('click', function (e) {
    //     e.preventDefault();
    //     if (password.val() !== password2.val()) {
    //         $('.incorrectMsg').show();
    //         password2.addClass('shake').addClass('invalidPass');
    //         password2.blur(function () {
    //             // (triggers whenever the password field is unselected)
    //             password2.trigger('reset');
    //             $('.incorrectMsg').hide();
    //             password2.removeClass('shake').removeClass('invalidPass');
    //         });
    //     } else {
    //         $('.incorrectMsg').hide();
    //     }
    //
    // });



});
// ============================= toggle finance==============
jQuery.fn.extend({
    toggleText: function (a, b) {
        var isClicked = false;
        var that = this;
        this.click(function () {
            if (isClicked) { that.text(a); isClicked = false; }
            else { that.text(b); isClicked = true; }
        });
        return this;
    }
});

$('#contact-fb').toggleText("الغاء الربط", "ربط الحساب");
$('#contact-fb').click(function () {  $(this).toggleClass("contact", "disconnect");  });
$('#contact-tw').toggleText("الغاء الربط", "ربط الحساب");
$('#contact-tw').click(function () { $(this).toggleClass("contact", "disconnect"); });
$('#contact-sn').toggleText("الغاء الربط", "ربط الحساب");
$('#contact-sn').click(function () { $(this).toggleClass("contact", "disconnect"); });
// ========================================= upload profile image =====================
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function () {
    readURL(this);
});
// =================================== add more category===========================
$(document).ready(function () {

    //     $(".add-more").click(function(){
    // $(".worked-cat").append('<div class="row my-5"><h6 class="w-100 mb-3">القسم الثالث</h6><div class="form-group col-xl-4 "><select class="form-control"><option value="" disabled selected hidden class="text-white">قسم الرئيسي</option><option>مكة</option><option>الرياض</option><option>المدينة</option><option>عمان</option></select></div><div class="form-group col-xl-4"><select class="form-control"><option value="" disabled selected hidden class="text-white">قسم الفرعي</option><option>مكة</option><option>الرياض</option><option>المدينة</option><option>عمان</option></select></div><div class="form-group col-xl-4"><select class="form-control"><option value="" disabled selected hidden class="text-white">قسم فرعي الفرعي</option><option>مكة</option><option>الرياض</option><option>المدينة</option><option>عمان</option></select></div></div>');
    //       $(".add-more").hide();
    //     });
    $(".add-more").click(function () {
        $(".new-cat").show();
        $(".add-more").hide();
    });
});
// ====================================== upload product images ======================
$('input[type="file"]').each(function () {
    // Refs
    var $file = $(this),
        $label = $file.next('label'),
        $labelText = $label.find('span'),
        labelDefault = $labelText.text();

    // When a new file is selected
    $file.on('change', function (event) {
        var fileName = $file.val().split('\\').pop(),
            tmppath = URL.createObjectURL(event.target.files[0]);
        //Check successfully selection
        if (fileName) {
            $label
                .addClass('file-ok delete')
                .css('background-image', 'url(' + tmppath + ')');
            $labelText.text(fileName);
            removeUpload()
        } else {

            // $label.css("background-image", "");
            // removeUpload()
            //  $label.removeClass('file-ok ');
            // $labelText.text(labelDefault);

        }
    });

    // End loop of file input elements
});
// ================================================
// var _gaq = _gaq || [];
// _gaq.push(['_setAccount', 'UA-36251023-1']);
// _gaq.push(['_setDomainName', 'jqueryscript.net']);
// _gaq.push(['_trackPageview']);
//
// (function () {
//     var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
//     ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
//     var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
// })();
// =========================================

function removeUpload() {
    // $(".remove").show();
    $(".delete").click(function () {
        $(this).css("background-image", "");
        // $(".delete:before").css('display','none');
        // $('#image').removeClass('delete:before');

        $(this).removeClass();
        $(this).children().show();
        // $("#add").hide();
    });
}
//   ================================ signup map ====================
function myMap() {
    var mapProp = {
        center: new google.maps.LatLng(51.508742, -0.120850),
        zoom: 5,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
}

function goBack() {

    window.history.back();

}







