$('body').delegate('.itemRepeated', 'keyup', function (e) {

    e.preventDefault();

    $("#showOrHideLoader").slideDown();
    $this = $(this);
    var item = $this.val(),
        model = $this.attr('data-model'),
        column = $this.attr('data-column'),
        attrName = $this.attr('name'),
        url = "/check/column/exist"  ;

    $.ajax({
        type: 'POST',
        url:  url,
        data: {item: item, model: model, column: column},
        dataType: 'json',
        success: function (data) {

            if (data.status === true) {
                $('#' + attrName + '_exsit_message').html(data.message);
                $this.addClass('validation_style');
                $("#showOrHideLoader").slideUp();
                $("#hiddenButton").hide();

            } else {
                $('#' + attrName + '_exsit_message').html('');
                $("#showOrHideLoader").slideUp();
                $this.removeClass('validation_style');
                $("#hiddenButton").show();
            }

        }
    });

});
