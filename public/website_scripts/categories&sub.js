$(document).ready(function() {

    var  url = "/sub_categories";

    $(document).on('change', '.category', function () {
        $('.Child_City_chid').text(" ");
        var myButton = document.getElementById("hiddenButton");
        var City_id =   $(this).val();
        var div =   $(this).parent().parent().parent();
        var op =" ";
        var showElement =  div.find('.showElement');
        $.ajax({
            type:"Get",
            url: url,
            data: {'id': City_id},
            success:function (data) {

                if(data == 401){

                    myButton.classList.remove("hidden-category");
//                                div.find('.City_chid').html(" ");
                    $('.hideDiv').parent().hide();
//                                $(".hideDiv").css("display", "none");
                }else {
                    $('.hideDiv').parent().show();
//                                $(".hideDiv").css("display", "block");
                    showValidation('إختار قسم فرعي من فضلك .' )
                    myButton.classList.add("hidden-category");
                    op += '<option  disabled selected>  إختر القسم الفرعي</option>';
                    for (var i = 0; i < data.length; i++) {
                        op += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    }
                    div.find('.City_chid').html(" ");
                    div.find('.City_chid').append(op);
                    if (data == null || data == undefined || data.length == 0) {
                        showElement.delay(500).slideUp();
                    } else {
                        showElement.delay(500).slideDown();

                    }

                }

            },
            error:function (error) {

            }
        })
    });

    $(document).on('change', '.City_chid', function () {
        $('.Child_City_chid').text(" ");
        var myButton = document.getElementById("hiddenButton");
        var City_id =   $(this).val();
        var div =   $(this).parent().parent().parent();
        var op =" ";
        var showElement =  div.find('.showElement');
        $.ajax({
            type:"Get",
            url: url,
            data: {'id': City_id},
            success:function (data) {
                if(data == 401){

                    myButton.classList.remove("hidden-category");

                }else {
                    showValidation('إختار قسم فرعي فرعي من فضلك')
                    op += '<option  disabled selected>  إختر الفرعي الفرعي   </option>';
                    for (var i = 0; i < data.length; i++) {
                        op += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                    }
                    $('.Child_City_chid').html(" ");

                    $('.Child_City_chid').append(op);

                }
            },
            error:function (error) {
                console.log(error)
            }
        })
    });

    $(document).on('click', '.Child_City_chid', function () {

        var Child_City_chid =   $(this).val();
        var myButton = document.getElementById("hiddenButton");
        if (Child_City_chid != null || Child_City_chid !== undefined ){

            // $("#hiddenButton").show();
            myButton.classList.remove("hidden-category");
        }

    });

});
