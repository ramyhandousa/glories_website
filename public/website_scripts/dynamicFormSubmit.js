$('#dynamicFormSubmit').on('submit', function (e) {

    e.preventDefault();

    var formData = new FormData(this);
    var formValid= $('#dynamicFormSubmit').parsley().validate();

    if(formValid){

        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            beforeSend:function (){
                $('.hiddenLoading').show();
                $('.hiddenContent').hide();
            },
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {

                messageSuccessOrError(data)
            },
            error: function (data) {
            },
            complete:function (){
                $('.hiddenLoading').hide();
                $('.hiddenContent').show();
            }
        });

    }


});

function messageSuccessOrError(data){
    if (data.status === true || data.status === 200) {
        var shortCutFunctionMessage = 'success',
            titleMessage = 'نجاح',
            message = data.message;
    } else {
        var shortCutFunctionMessage = 'error',
            titleMessage = 'فشل',
            message = data.error[0];
    }
    var shortCutFunction = shortCutFunctionMessage;
    var msg = message;
    var title = titleMessage;
    toastr.options = {
        positionClass: 'toast-top-left',
        onclick: null,
        preventDuplicates: true,
        preventOpenDuplicates: true
    };
    var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
    $toastlast = $toast;

    if (data.url){
        setTimeout(function () {
            window.location.href = data.url;
        }, 1000);
    }

}
