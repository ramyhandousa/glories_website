const fileTypes = [
    "image/jpeg",
    "image/jpg",
    "image/png",
];

function validFileType(file) {
    return fileTypes.includes(file.type);
}

function returnFileSize(number) {
    let status ;
    let value ;
    if(number < 1024) {
        // number + 'bytes';
        value =number;
        status =true;
    } else if(number >= 1024 && number < 1048576) {
        // (number/1024).toFixed(1) + 'KB';
        value =(number/1024).toFixed(1)  + 'KB';
        status =true;
    } else if(number >= 1048576) {
        // (number/1048576).toFixed(1) + 'MB';

        value =(number/1024).toFixed(1)  + 'MB';
        status = (number / 1048576).toFixed(1) <= 11;
    }
    return status;
}

$(document).on('change','.choose_image',function (){
    let file = $(this)[0].files[0],
        checkHaveFile = validFileType(file),
        checkFileSize = returnFileSize(file.size);
    if (!checkHaveFile){
        showValidation("تأكد من رفع صورة من فضلك ")
        $(this).val('')
    }
    if (checkHaveFile && !checkFileSize){
        showValidation("من فضلك إختار صورة اقل ")
        $(this).val('')
    }

});
