@component('mail::layout')

    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
           {{$text}}
        @endcomponent
    @endslot

    {{$message}} {{$order->id}}


    Thanks,<br>
    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }}   المصمم الذكي.
        @endcomponent
    @endslot
@endcomponent
