@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')


    <body class="bg-orders text-center">

    <div class="container text-right  my-5 offers categories">
        <div>
            <ul class="row px-0 text-white">

                @if(isset($users))

                    @if(count($users) > 0)

                        @foreach($users as $user)

                            @include('.categories.data-users',[$user])

                        @endforeach

                        {{ $users->links('vendor.pagination.simple-bootstrap-4') }}

                    @else
                        <img src="{{asset('empty_data.png')}}" alt="">
                    @endif

                @endif


                @if(isset($categories))

                    @foreach($categories as  $category)

                            @include('.categories.data-categories',[$category])

                    @endforeach

                        {{ $categories->links('vendor.pagination.simple-bootstrap-4') }}
                @endif

            </ul>
        </div>
    </div>
@endsection


    @section('scripts')

        <script>

            // Check If Available Categories First
            // Check If Available Users Second
            function checkIfAvailable(category_id,categoriesCount,usersCount){

                if (categoriesCount === 0 && usersCount === 0 ){
                    showValidation('لا يوجد أقسام ولا مستخدمين ')
                    return;
                }else {
                    window.location.href = "{{url('/category')}}" + "/" + category_id + "/details";
                }
            }

            {{--function checkCategory($id) {--}}
            {{--    $.ajax({--}}
            {{--        type: 'Get',--}}
            {{--        url: '{{ route('site.allCategories') }}?type=subCategory',--}}
            {{--        data: {id: $id},--}}
            {{--        dataType: 'json',--}}
            {{--        success: function (data) {--}}
            {{--            location.reload();--}}
            {{--        }--}}
            {{--    });  --}}
            {{--}--}}
        </script>


@endsection
