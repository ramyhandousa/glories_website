@extends('layouts.master')
@section('title', 'إدارة العملاء ')
@section('styles')
    <style>

        .hidden-category{
            visibility: hidden !important;
        }
        .help-block {
            color: #e74c3c !important;
            font-size: 16px !important;

        }
    </style>
@endsection
@section('content')

    <body class="bg-orders text-center">

    <div class="container text-right  my-5">
        <div class="row">
            <div class="col-xl-4">
                <div class="golden-subscribe text-center">
                    <h4>{{$memberShip->name}}</h4>
                    <p class="px-5">
                        يؤهلك لاضافة {{$memberShip->number_product}} منتج <br>
                        يؤهلك لاضافة {{$memberShip->number_services_enjoy}}  خدمة
                    </p>
                </div>
                <div class="my-subscripe my-5">
                    <h6 class="pb-3">
                        اشتراكي
                    </h6>
                    <div class="my-4">
                        <span>اشتراك ذهبي</span>
                        <span class="text-left default-color float-left"> ينتهي  {{$memberShip->member_end}}</span>

                    </div>
                    <div class="my-4">
                        <span>الاقسام</span>
                        <span class="text-left default-color float-left">{{$user->categories_count}}/{{$memberShip->number_category}}</span>

                    </div>
                    <div class="my-4">
                        <span>المنتجات</span>
                        <span class="text-left default-color float-left">{{$user->products_count}}/{{$memberShip->number_product}}</span>

                    </div>
                    <div class="my-4">
                        <span>العروض</span>
                        <span class="text-left default-color float-left">{{$countOffer}}/{{$memberShip->number_services_enjoy}}</span>

                    </div>


                </div>
            </div>


            <div class="col-xl-8 text-white worked-cat">

                @if(count($user->categories) > 0)
                    <div class="row">
                       <h3> الأقسام الخاصه بك :</h3>
                        <div class="my-subscripe w-100 my-3">
                            <div class="row myCategories">
                                @foreach($user->categories as $category)
                                    <div class="col-md-3 text-center bl-before my-3 my_categories_data">
                                        <h4 class="m-0">
                                            {{$category->name}}
                                        </h4>
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                @endif
                @if($memberShip->number_category == $user->categories_count)
                    <h5>  لا يمكنك إضافة أقسام تبعا لصلاحيات العضوية الخاصة بك  </h5>
                @else

                    <h5>
                        الاقسام التي سأعمل بها ...
                    </h5>

                    @if($errors->has('unavailable'))
                        <p class="help-block">
                            {{ $errors->first('unavailable') }}
                        </p>
                    @endif

                    <form  method="post"   action="{{route('site.storeCategories') }}"  >
                        <div class="row my-5">
                            <h6 class="w-100 mb-3 pr-4">   إضافة قسم </h6>


                            {{ csrf_field() }}
                            <div class="form-group col-xl-4 ">
                                <select name="category" class="form-control requiredField category" required>
                                    <option value="" disabled selected hidden class="text-white" >القسم الرئيسي</option>
                                    @foreach($categories as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-xl-4">
                                <select name="category" class="form-control hideDiv City_chid">
                                    <option value="" disabled selected hidden class="text-white">القسم الفرعي</option>
                                </select>
                            </div>

                            <div class="form-group col-xl-4 ">
                                <select name="category" class="form-control hideDiv Child_City_chid">
                                    <option value="" disabled selected  hidden class="text-white">قسم فرعي الفرعي</option>
                                </select>
                            </div>

                        </div>

                        <div class="text-center col-xl-12 my-5">
                            <button   type="submit" class="btn default-bg br-50 px-5 my-4" id="hiddenButton">حفظ التعديلات </button>
                        </div>

                    </form>
                @endif
            </div>

        </div>
    </div>

    @endsection

    @section('scripts')
        <script src="{{asset('website_scripts/categories&sub.js')}}" defer></script>
        <script>
            $(".my_categories_data:last").removeClass("bl-before");
        </script>

@endsection
