<li class="col-xl-4 col-lg-6 col-md-6">

    <form class="myLoader" method="GET" action="{{ route('site.getUserById') }}"  id="changeUser{{ $user->id }}" >

        <a href="#" onclick="return $('#changeUser{{ $user->id }}').submit()">
            <input type="hidden" name="id" value="{{$user->id}}" >
            <div class="card">
                <img class="card-img-top img-fluid" src="{{ $user->image }}" alt="Card image">
                <div class="card-img-overlay">
                    <h4 class="card-title  d-inline-block "> {{$user->name}} </h4>

                    <span class="float-left default-color rate">
                                            <i class="fas fa-star"></i> {{$user->rate}}
                                        </span>
                </div>
            </div>

        </a>

    </form>
</li>
