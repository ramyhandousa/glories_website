@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')


    <body class="bg-orders text-center">

    <div class="container text-right  my-5 offers categories">
        <div>
            <ul class="row px-0 text-white">


                @foreach($categories as  $category)

                    <li class="col-xl-4 col-lg-6 col-md-6">

                        {{--                                    <a class="myLoader" href="{{ route('site.sub.category', $category->id) }}">--}}
                        <a href="#" onclick="checkIfAvailable({{$category->id}},{{$category->children->count()}},{{$category->users->count()}})">
                            <div class="card">
                                <img class="card-img-top img-fluid" src="{{$category->image?: asset('category.jpg')}}" loading="lazy" alt="Card image">
                                <div class="card-img-overlay">
                                    <span class="mb-4 show-more">عرض {{$category->users_count}} خدمة</span>
                                    <h4 class="card-title default-color">
                                        {{$category->name}}
                                    </h4>
                                    <p class="card-text w-75 d-inline-block">
                                        {{$category->description}}
                                    </p>
                                </div>
                            </div>
                        </a>
                    </li>

                @endforeach

                {{ $categories->links('vendor.pagination.simple-bootstrap-4') }}

            </ul>

        </div>
    </div>
    @endsection

    @section('scripts')

        <script>

            // Check If Available Categories First
            // Check If Available Users Second
            function checkIfAvailable(category_id,categoriesCount,usersCount){

                if (categoriesCount === 0 && usersCount === 0 ){
                    showValidation('لا يوجد أقسام ولا مستخدمين ')
                    return;
                }else {
                    window.location.href = "{{url('/category')}}" + "/" + category_id + "/details";
                }

            }


            function checkCategory($id) {
                $.ajax({
                    type: 'Get',
                    url: '{{ route('site.allCategories') }}?type=subCategory',
                    data: {id: $id},
                    dataType: 'json',
                    success: function (data) {

                        location.reload();
                    }
                });
            }

        </script>


@endsection
