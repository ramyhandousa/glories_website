<li class="col-xl-4 col-lg-6 col-md-6">

    {{--                                    <a class="myLoader" href="{{ route('site.sub.category', $category->id) }}">--}}
    <a href="#" onclick="checkIfAvailable({{$category->id}},{{$category->children->count()}},{{$category->users->count()}})">
        <div class="card">
            <img class="card-img-top img-fluid" src="{{$category->image?: asset('category.jpg')}}" loading="lazy" alt="Card image">
            <div class="card-img-overlay">
                <span class="mb-4 show-more">عرض {{$category->users_count}} خدمة</span>
                <h4 class="card-title default-color">
                    {{$category->name}}
                </h4>
                <p class="card-text w-75 d-inline-block">
                    {{$category->description}}
                </p>
            </div>
        </div>
    </a>
</li>
