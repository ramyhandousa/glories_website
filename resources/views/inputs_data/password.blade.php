<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
    <input type="password" required name="password"   class="form-control requiredField"  value="{{old('password')}}"  id="password" placeholder="كلمة المرور">
    <i class="far fa-eye show-pass"  onclick="showSignup()"></i>
    @if($errors->has('password'))
        <p class="help-block">
            {{ $errors->first('password') }}
        </p>
    @endif
</div>
