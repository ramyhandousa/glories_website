
<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">

    <input type="tel" name="email" required data-model="App\Models\User" data-column="email"
           class="form-control email itemRepeated"    placeholder="البريد الإلكتروني">

    {{--To Check if exitst in database--}}
    <span id="showOrHideLoader" style="display: none;">
                                    <img style="position: absolute;top: -4px;left: 21px;" src="{{ asset('/loadrealimg.gif') }}"/>
    </span>

    <p class="help-block" id="email_exsit_message"></p>

    <span class="email" style="font-size: 12px; color: darkred;"></span>

    {{---------- End check --------}}
    @if($errors->has('email'))
        <p class="help-block">
            {{ $errors->first('email') }}
        </p>
    @endif

</div>
