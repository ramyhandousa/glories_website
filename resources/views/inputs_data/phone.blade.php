<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
    <input type="tel" name="phone" id="phone" required value="{{old('phone')}}"
           data-model="App\Models\User" data-column="phone"
           class="form-control tenNumberOnly itemRepeated"   placeholder="رقم الجوال">
    {{--To Check if exitst in database--}}
    <span id="showOrHideLoader" style="display: none;">
                                    <img style="position: absolute;top: -4px;left: 21px;" src="{{ asset('/loadrealimg.gif') }}"/>
                                </span>
    <p class="help-block" id="phone_exsit_message"></p>
    {{--<span class="phone" style="font-size: 12px; color: darkred;"></span>--}}
    {{---------- End check --------}}
    @if($errors->has('phone'))
        <p class="help-block">
            {{ $errors->first('phone') }}
        </p>
    @endif
</div>
