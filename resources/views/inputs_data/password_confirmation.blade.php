
<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

    <input type="password" required name="password_confirmation" value="{{old('password_confirmation')}}"
           class="form-control requiredField"   id="password-verify" placeholder=" تأكيد كلمة المرور">

    <i class="far fa-eye show-pass" onclick="showSignupVerify()"></i>

    @if($errors->has('password_confirmation'))
        <p class="help-block">
            {{ $errors->first('password_confirmation') }}
        </p>
    @endif

</div>
