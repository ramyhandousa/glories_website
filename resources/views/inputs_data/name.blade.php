<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
    <input type="text" name="name" required class="form-control nameValid" value="{{old('name')}}"     placeholder="الاسم">
    @if($errors->has('name'))
        <p class="help-block">
            {{ $errors->first('name') }}
        </p>
    @endif
</div>
