<div class="form-group">
    <select name="city_id"  class="form-control requiredField"   id="select-city" required>
        <option value="" disabled selected hidden  class="text-white">المدينة</option>
        @foreach($cities as $city)
            <option value="{{$city->id}}" @if(old('city_id') == $city->id) selected @endIf>{{$city->name}}</option>
        @endforeach
    </select>
</div>
