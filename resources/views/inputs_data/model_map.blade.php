<div class="form-group" data-toggle="modal" data-target="#mapModal" {{ $errors->has('address') ? ' has-error' : '' }}">
    <input type="text" required  name="address_search"  id="address_search" value="{{old('address_search')}}" class="form-control requiredField" placeholder="الموقع على الخريطة">
    <input type="hidden" name="latitute" id="lat" value="{{old('latitute')}}"/>
    <input type="hidden" name="longitute" id="lng" value="{{old('longitute')}}"/>
    <input type="hidden" name="address" id="address" value="{{old('address')}}"/>
    @if($errors->has('address'))
        <p class="help-block">
            {{ $errors->first('address') }}
        </p>
    @endif
    <i class="far fa-compass location"></i>

</div>


<!-- map  modal -->
<div class="modal fade" id="mapModal">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content" >
            <!-- Modal body -->
            <div class="modal-body">
                <div  id="googleMap"></div>
            </div>
        </div>
    </div>
</div>
