@extends('layouts.master')
@section('title', 'نتائج البحث ')

@section('content')


    <body class="bg-provider text-center">

    <div class="container text-right  my-5 text-white ">
        @if(!\Illuminate\Support\Facades\Auth::check())

             <a href="javascript:;" onclick="goBack()" class="back"><i class="fas fa-chevron-right"></i> رجوع </a>
        @endif
        @if(count($data) > 0)

            @if(request('search') == 'user')
                @foreach($data as $user)
                    <ul>
                        <li class="col-xl-4 col-lg-6 col-md-6">
                            <form method="GET" action="{{ route('site.getUserById') }}"  id="changeUser{{ $user->id }}" >
                                <a href="#" onclick="return $('#changeUser{{ $user->id }}').submit()">
                                    <input type="hidden" name="id" value="{{$user->id}}" >
                                    <div class="card">
                                        <img class="card-img-top img-fluid" src="{{ $user->image }}" alt="Card image">
                                        <div class="card-img-overlay">
                                            <h4 class="card-title  d-inline-block "> {{$user->name}} </h4>

                                            <span class="float-left default-color rate">
                                                    <i class="fas fa-star"></i> {{$user->rate}}
                                        </span>
                                        </div>
                                    </div>
                                </a>
                            </form>
                        </li>
                    </ul>
                @endforeach
            @endif

            @if( in_array(request('search'),['offer','product']))

                @foreach($data as $product)

                    <ul class="row px-0 text-white">
                        <li>
                            <form method="GET" action="{{ route('orders.create') }}"  id="changeProduct{{ $product->id }}" >

                                <a href="#" onclick="return $('#changeProduct{{ $product->id }}').submit()">
                                    <input type="hidden" name="userId" value="{{$product->user_id}}">
                                    <input type="hidden" name="productId" value="{{$product->id}}">
                                    <div class="card ">
                                        <img class="card-img" loading="lazy" width="50%" height="50%"
                                             src="{{$product->images_path ? $product->images_path[0] : asset('category.jpg')  }}" alt="image_product">
                                        <div class="card-body">
                                            <h5 class="card-title default-color"> {{$product->name}}    </h5>
                                            <p class="card-text">
                                                {{$product->description}}
                                            </p>
                                            <div class="card-footer">
                                                <span><b class="default-color "> {{$product->price}}</b>  ريال سعودي</span>
                                                @if($product->offer)
                                                    <h4>السعر بعد العرض </h4>
                                                    <span><b class="default-color "> {{$product->offer->price}}</b>  ريال سعودي</span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </a>
                            </form>
                        </li>
                    </ul>

                @endforeach

            @endif

                {{ $data->links() }}
{{--                <div>--}}
{{--                    <ul class="pagination justify-content-center">--}}
{{--                        <li class="page-item active"><a class="page-link" href="#">1</a></li>--}}
{{--                        <li class="page-item"><a class="page-link" href="#">2</a></li>--}}
{{--                        <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
{{--                    </ul>--}}
{{--                </div>--}}

        @else
            <img loading="lazy" style="width: inherit" src="{{ asset('empty_data.png')  }}">
        @endif
    </div>

@endsection
