@extends('layouts.master')
@section('title', 'إدارة العملاء ')
@section('styles')
    <style>

        .hidden-category{
            visibility: hidden !important;
        }


    </style>
@endsection
@section('content')
    <body class="bg-orders text-center">

    <div class="container text-right profile-info my-5">
        <div class="row">
            <div class="col-xl-5 mx-auto">

                <div class="my-info my-5 search">

                    <form  action="{{route('site.searchItems')}}" method="GET">
                        <h6 >
                            النتيجة المرجوه
                        </h6>
                        <ul class="row btn-group btn-group-toggle d-block my-3 text-center mx-auto" data-toggle="buttons">

                            <li class="col-xl-4 col-lg-4 col-md-4 btn btn-secondary waves-effect waves-light  ">
                                <label><input type="radio" name="search" id="option4" autocomplete="off" checked="checked" value="user">
                                    مزود الخدمة</label>

                            </li>
                            <li class="col-xl-4 col-lg-4 col-md-4 btn btn-secondary waves-effect waves-light ">
                                <label> <input type="radio" name="search" id="option4" autocomplete="off" value="product">
                                    خدمة او منتج</label>

                            </li>
                            <li class=" col-xl-4 col-lg-4 col-md-4 btn btn-secondary waves-effect waves-light ">
                                <label>
                                    <input type="radio" name="search" id="option4" autocomplete="off" value="offer">
                                    عروض
                                </label>
                            </li>
                        </ul>
                        <h6 class="pb-3">
                            القسم والمدينه
                        </h6>
                        <div class="form-group">
                            <select name="categoryId" class="form-control category" required>
                                <option value="" disabled selected hidden class="text-white">القسم الرئيسي</option>
                                @foreach($categories as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="categoryId" class="form-control hideDiv City_chid">
                                <option value="" disabled selected hidden class="text-white">القسم الفرعي</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="categoryId" class="form-control hideDiv Child_City_chid">
                                <option value="" disabled selected  hidden class="text-white">قسم فرعي الفرعي</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="cityId" class="form-control">
                                <option value="" disabled selected hidden class="text-white">المدينة</option>
                                @foreach($cities as $city)
                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="text-center btn-edit">
                            <button type="submit" class="btn default-bg br-50 px-4" id="hiddenButton">تأكيد</button>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </div>
    @endsection

    @section('scripts')
        <script src="{{asset('website_scripts/categories&sub.js')}}" defer ></script>

@endsection
