@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')

    <body class="bg-offers text-center">

    <div class="container text-right  my-5 text-white">
        @if(count($products) > 0)
            <form id="dynamicFormSubmit" method="post" class="product-info" action="{{route('offers.store') }}"
                  enctype="multipart/form-data" data-parsley-validate novalidate>
                {{ csrf_field() }}
                <div>
                    <h6>اختر احدى منتجاتك لوضع عرض عليها</h6>
                    <ul class="row p-0 choose-offer btn-group btn-group-toggle" data-toggle="buttons">

                        @foreach($products as $product)
                            <li class="col-xl-4 valueProduct col-lg-6 btn btn-secondary waves-effect waves-light">
                                <input type="radio" name="product_id" id="option{{$product->id}}" autocomplete="off" value="{{$product->id}}">
                                <a href="#">
                                    <div class="card ">
                                        @if($product->images_path)
                                            <img class="card-img" src="{{$product->images_path[0] }}" alt="">
                                        @else
                                            <img class="card-img" src="{{asset('category.jpg') }}" alt="">
                                        @endif
                                        <div class="card-body">
                                            <h5 class="card-title default-color">{{$product->name}}  </h5>
                                            <p class="card-title default-color">سعر المنتج {{$product->price}} </p>
                                            <p class="card-text">
                                                {{$product->description}}
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <div>
                        <div class="text-center my-5 new-offer">
                            <div class="form-group new-price">
                                <input type="text" name="price" class="form-control" placeholder=" السعرالجديد" required>
                                <span class="currency">ريال سعودي</span>
                            </div>
                            <div class="form-group new-price mt-4"  >
                                <input type="text" onfocus="(this.type='date')" name="period_start" class="form-control text-right" placeholder="تاريخ بداية العرض"   required>
                                <!--<input type="date" name="period_start"  class="form-control form_datetime" placeholder="تاريخ بداية العرض"   required>-->
                                <!--<i class="fas fa-calendar-alt form-icon"></i>-->
                            </div>
                             <div class="form-group new-price mt-4">
                             <input type="text" onfocus="(this.type='date')" name="period_end"   class="form-control text-right" placeholder="تاريخ نهاية العرض" required>
                                <!--<input type="date" name="period_end" class="form-control form_datetime" placeholder="تاريخ نهاية العرض" required >-->
                                <!--<i class="fas fa-calendar-alt form-icon"></i>-->
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-xl-12 text-center my-5 ">
                    <button id="myButton" type="submit" class="btn default-bg br-50 text-dark my-5 px-4">اضافة العرض</button>
                </div>

            </form>
        @endif
    </div>
    @endsection

    @section('scripts')
        <script type="text/javascript">
            $(document).ready(function(){
                $(".form_datetime").datepicker({format: 'yyyy-mm-dd'});
            });

            $( "#myButton" ).click(function() {
                var value = $('.valueProduct').hasClass('active');

                if(value === false){
                    showErrors('{{ session()->get('errors',' يجب اختيار منتج من فضلك  ') }}');
                    return false;
                }
            });
        </script>

        <script src="{{asset('website_scripts/dynamicFormSubmit.js')}}" defer></script>

@endsection
