@extends('layouts.master')
@section('title', 'إدارة العملاء ')
@section('styles')
    <style>
        .help-block {
            color: #e74c3c !important;
            font-size: 16px !important;
            /*position: absolute;*/
            /*left: 15px;*/
            /*top: 50%;*/
            /*transform: translateY(-50%);*/

        }
        .has-error input{
            border-color: #e74c3c !important;
        }
        .validation_style {
            border: 1px solid #e74c3c !important;
        }
        .hidden-category{
            visibility: hidden !important;
        }
    </style>
@endsection
@section('content')

<body class="bg-orders">

    <div class="container text-center text-white">
        <img src="{{ asset('Site/logo.png')  }}" class="img-fluid intro-logo w-25">

    </div>
    <div class="container text-right ">
        <div class="row">
            <div class="col-xl-4 col-lg-6 col-md-6 mx-auto">
                <div class="contactForm ">
                    <p class="pb-3 text-white">
                        اترك رسالتك وسنجيب على استفساراتكم بأقرب وقت
                    </p>
                    <form method="post" action="{{route('site.sendContactUs')}}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('typeId') ? ' has-error' : '' }}">
                            <select name="typeId" class="form-control" id="select-city" required>
                                <option value="" disabled selected hidden class="text-white">الرسالة بخصوص</option>
                               @foreach($types as $type)
                                    <option value="{{$type->id}}">{{$type->name}}</option>
                               @endforeach
                            </select>
                            @if($errors->has('typeId'))
                                <p class="help-block">
                                    {{ $errors->first('typeId') }}
                                </p>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}">
                            <textarea name="message" class="form-control" required maxlength="2000" rows="5" id="comment" placeholder=" محتوى الرسالة"></textarea>
                            @if($errors->has('message'))
                                <p class="help-block">
                                    {{ $errors->first('message') }}
                                </p>
                            @endif
                        </div>

                        <div class="text-center btn-edit">
                            <button type="submit" class="btn default-bg br-50">ارسال</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
