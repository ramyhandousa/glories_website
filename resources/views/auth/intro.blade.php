@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')
    <body class="bg-intro">


    <div class="container text-center text-white">
        <img src="{{ asset('Site/logo.png')  }}" class="img-fluid intro-logo w-25">
        <p class="w-25 m-auto ">
            نبذه قصيرة عن تطبيق AR يتم كتابتها في هذه المنطقة
        </p>
        <div class="intro-btn my-5">
            <a   href="{{route('user_signup_page')}}" class="btn default-bg br-50 text-dark mx-2 my-2 ">مستخدم</a>
            <a   href="{{route('provider_signup_page')}}" class="btn provider-btn br-50 mx-2 my-2 ">مزود خدمة</a>
        </div>
        <a   href="{{route('site.allCategories')}}" class="d-block my-5 skip myLoader">تخطي</a>

        <span class="default-color">
            تصفح الخدمات المتوفرة
        </span>
    </div>
@endsection
