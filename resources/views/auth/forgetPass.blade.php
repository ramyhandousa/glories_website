@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')
<body class="bg-orders text-center">

    <div class="container text-right profile-info my-5">
        <div class="row">
            <div class="col-xl-4 mx-auto">

                <div class="my-info my-5 py-5">

                    <form id="changePass" method="POST" action="{{route('site.changePassword')}}" >
                        {{ csrf_field() }}
                        <input type="hidden" name="phone" id="valueOfPhone" value="{{request()->phone}}">
                        <div class="form-group">
                            <input type="password"  required id="password" name="password" class="form-control" placeholder="كلمة المرور الجديدة">
                        </div>
                        <div class="form-group">
                            <input type="password" id="password-verify" name="password_confirmation" class="form-control" placeholder="تأكيد كلمة المرور الجديدة">
                        </div>

                        <div class="text-center btn-edit">
                            <button id="myButton" type="submit" class="btn default-bg br-50 px-4 controlButton">تأكيد</button>
                        </div>
                    </form>
                </div>
            </div>


        </div>
    </div>

@endsection
    @section('scripts')

        <script>

            $( "#myButton" ).click(function() {

                var valueOfphone = $('#valueOfPhone').val();

                if($.trim(valueOfphone) == ''){

                    showErrors('{{ session()->get('errors',' حدث خطا ما يرجي تحديث الصفحة او محاولة  مرة اخري ') }}');

                    return false;
                }

            });

            $('#changePass').on('submit', function (e) {

                e.preventDefault();

                var formData = new FormData(this);
                var form = $(this);
                $.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        messageSuccessOrError(data)

                    },
                    error: function (data) {
                    }
                });

            });


            function messageSuccessOrError(data){
                if (data.status === true) {
                    var shortCutFunctionMessage = 'success',
                        titleMessage = 'نجاح',
                        message = data.message;
                } else {
                    var shortCutFunctionMessage = 'error',
                        titleMessage = 'فشل',
                        message = data.error[0];
                }
                var shortCutFunction = shortCutFunctionMessage;
                var msg = message;
                var title = titleMessage;
                toastr.options = {
                    positionClass: 'toast-top-left',
                    onclick: null
                };
                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;

                if (data.status === true && data.url) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }
            }


        </script>


@endsection
