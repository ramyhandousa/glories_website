@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')
    <body class="bg-orders text-center">

    <div class="container text-right profile-info my-5">
        <div class="row">
            <form style="width:100%" id="dynamicFormSubmit" method="post" action="{{route('update_profile_provider',\Illuminate\Support\Facades\Auth::id())}}" enctype="multipart/form-data">
                 {{ csrf_field() }}
                <div class="row">


                    <!-- For User    -->
                    <div class="col-xl-4 ">
                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <input name="image" class="choose_image" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                <label for="imageUpload"></label>
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview" style="background-image: url({{$user->image}});">  </div>
                            </div>
                        </div>
                        <div class="my-info my-5">
                            <h6 class="pb-3">
                                معلوماتي
                            </h6>
                            <div class="form-group">
                                <input type="text" value="{{$user->name}}" name="name" class="form-control" placeholder="الاسم">
                            </div>
                            <div class="form-group">
                                <input type="email" value="{{$user->email}}" name="email" class="form-control" placeholder="البريد الإلكتروني  ">
                            </div>
                            <div class="form-group">
                                <input type="tel" value="{{$user->phone}}" name="phone" class="form-control" placeholder="رقم الجوال">
                            </div>
                            <div class="form-group">
                                <select name="city_id" class="form-control"   id="select-city">
                                    <option value="" disabled selected hidden  class="text-white">المدينة</option>
                                    @foreach($cities as $city)
                                        <option @if($city->id == $user->city_id) selected   @endif value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="text-center btn-edit">
                                <button type="submit" class="btn default-bg br-50 px-4 w-100">تعديل البيانات</button>
                                <a href="{{route('site.getEditPassword')}}"  class="btn default-bg br-50  w-100 mt-2">تعديل كلمة المرور</a>
                            </div>

                        </div>
                    </div>
                    <!-- End  For User    -->

                        @if($user['member'])
                            <div class="col-xl-4">
                            <div class="my-subscripe">
                                <h6 class="pb-3">
                                    اشتراكي
                                </h6>
                                <div class="my-4">
                                    <span>اشتراك ذهبي</span>
                                    <span class="text-left default-color float-left">ينتهي   {{$user['member']->member_end}}</span>

                                </div>
                                <div class="my-4">
                                    <span>الاقسام</span>
                                    <span class="text-left default-color float-left">{{$user->categories_count}}/{{$user['member']->number_category}}</span>

                                </div>
                                <div class="my-4">
                                    <span>المنتجات</span>
                                    <span class="text-left default-color float-left">{{$user->products_count}}/{{$user['member']->number_product}}</span>

                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-xl-4">
                            <div class="contact-ways">
                                <h6>تواصل العملاء</h6>
                                <div class="custom-control custom-switch my-2">
                                    <input type="checkbox"  class="custom-control-input" id="switch1" name="phoneConnect"
                                           @if(optional($user->provider)->connect_phone == 1)checked @endif>
                                    <label class="custom-control-label" for="switch1">عن طريق الجوال</label>
                                </div>

                                <div class="custom-control custom-switch my-2">
                                    <input type="checkbox"  class="custom-control-input" id="switch2" name="emailConnect"
                                           @if(optional($user->provider)->connect_email == 1)checked @endif>
                                    <label class="custom-control-label" for="switch2">عن طريق البريد الالكتروني</label>
                                </div>
                                <div class="custom-labels col-xl-12 text-right pl-0 pr-4">
                                    <label class="">عن طريق الفيسبوك</label>
                                    <input type="hidden" id="myFacebook" value="0" name="faceBook" >
                                    <label class="float-left" id="contact-fb">الغاء الربط</label>
                                </div>
                                <div class="custom-labels col-xl-12 text-right pl-0 pr-4">
                                    <label class="">عن طريق تويتر</label>
                                    <input type="hidden" id="myTwitter" value="0" name="twitter" >
                                    <label class="float-left" id="contact-tw">الغاء الربط</label>
                                </div>
                                <div class="custom-labels col-xl-12 text-right pl-0 pr-4">
                                    <label class="">عن طريق سناب شات</label>
                                    <input type="hidden" id="mySnapChat" value="0" name="snapChat" >
                                    <label class="float-left" id="contact-sn">الغاء الربط</label>
                                </div>
                            </div>
                            <div class="payment-ways my-5">
                                <h6>حدد طرق الدفع المناسبة لك</h6>
                                <div class="custom-control custom-switch my-2">
                                    <input type="checkbox" class="custom-control-input"  id="switch3" name="bank"
                                           @if(optional($user->provider)->bank_transfer == 1)checked @endif>
                                    <label class="custom-control-label" for="switch3">تحويل بنكي</label>
                                </div>
                                <div class="custom-control custom-switch my-2">
                                    <input type="checkbox" class="custom-control-input" id="switch4" name="cash"
                                           @if(optional($user->provider)->cash == 1)checked @endif>
                                    <label class="custom-control-label" for="switch4">كاش</label>
                                </div>
                                <div class="custom-control custom-switch my-2">
                                    <input type="checkbox" class="custom-control-input" id="switch5" name="pay"
                                           @if(optional($user->provider)->pay == 1)checked @endif>
                                    <label class="custom-control-label" for="switch5">سداد</label>
                                </div>
                                <div class="custom-control custom-switch my-2">
                                    <input type="checkbox" class="custom-control-input" id="switch6" name="online"
                                           @if(optional($user->provider)->pay_electronic == 1)checked @endif>
                                    <label class="custom-control-label" for="switch6">(تحويل الكتروني(بايبال</label>
                                </div>

                            </div>
                        </div>


                </div>

            </form>
        </div>
    </div>
    @endsection

    @section('scripts')

        <script src="{{asset('website_scripts/imagesVaildation.js')}}" defer></script>
        <script src="{{asset('website_scripts/dynamicFormSubmit.js')}}" defer> </script>

@endsection
