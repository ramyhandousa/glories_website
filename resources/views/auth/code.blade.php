
@extends('layouts.master')
@section('title', ' تاكيد الكود ')

@section('content')
<body class="bg-orders code-bg text-center">

    <div class="container text-right profile-info my-5">
        <div class="row">
            <div class="col-xl-4  col-lg-6 col-md-6 mx-auto">

                <div class="my-info my-5 py-5 text-center">

                    <form id="resendCodePost" method="POST" action="{{ route('resendCode') }}" >
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="tel" id="valueOfPhone" name="phone"   class="form-control phone" placeholder="رقم الجوال">
                        </div>

                        <div class="text-center btn-edit">
                            <button id="myButton" type="button" class="btn default-bg br-50 px-4 mb-4" data-toggle="modal"
                                data-target="#myModal">ارسال الكود</button>
                        </div>

                        <button id="resendCode"  style="display: contents; color: white;" type="submit">اعاده ارسال الكود</button>
                    </form>
                </div>

            </div>

            <div class="modal fade" id="myModal">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="code">

                            @if(request('type'))
                                <form id="activationCode" method="POST" action="{{ route('checkCode') }}?type=resetPass">
                            @else
                                <form id="activationCode" method="POST" action="{{ route('checkCode') }}">
                            @endif

                                {{ csrf_field() }}

                                <h5>ادخل الكود</h5>
                                <input type="hidden" id="phone" name="phone"  >
                                <div class="form-group  " >
                                    <input name="code1" type="text" class="form-control m-1 empty_inputs" maxlength="1">
                                </div>
                                <div class="form-group">
                                    <input name="code2" type="text" class="form-control empty_inputs" maxlength="1">
                                </div>
                                <div class="form-group">
                                    <input  name="code3" type="text" class="form-control empty_inputs" maxlength="1">
                                </div>
                                <div class="form-group">
                                    <input name="code4" type="text" class="form-control empty_inputs" maxlength="1">
                                </div>
                                <div class="text-center btn-edit">
                                    <button   type="submit" class="btn default-bg br-50 px-4 mySubmitButton ">ارسال </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

        <script>

            $( "#myButton" ).click(function() {

                var valueOfphone = $('#valueOfPhone').val();

                if($.trim(valueOfphone) == ''){

                    showErrors('{{ session()->get('errors',' يجب إدخال رقم الهاتف من فضلك  ') }}');

                    return false;
                }
                $("#phone").val(valueOfphone);

            });

            $( "#resendCode" ).click(function() {

                var valueOfphone = $('#valueOfPhone').val();

                if($.trim(valueOfphone) == ''){

                    showErrors('{{ session()->get('errors',' يجب إدخال رقم الهاتف من فضلك  ') }}');

                    return false;
                }



            });

            $('#resendCodePost').on('submit', function (e) {

                e.preventDefault();

                var phone = $('#valueOfPhone').val();
                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: '{{ route('resendCode') }}',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        if (data.status === true) {
                            var shortCutFunction = 'success';
                            var msg = data.message;
                            var title = 'نجاح';
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                            $toastlast = $toast;
                            setTimeout(function () {
                                window.location.href = "{{ url()->current() }}";
                            }, 2000);

                        }
                        if (data.status === false) {
                            var shortCutFunction = 'error';
                            var msg = data.message;
                            var title = 'خطأ';
                            toastr.options = {
                                positionClass: 'toast-top-left',
                                onclick: null
                            };
                            var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                            $toastlast = $toast;
                        }

                    },
                    error: function (data) {
                    }
                });

            });

            $('#activationCode').on('submit', function (e) {

                e.preventDefault();

                var formData = new FormData(this);
                var form = $(this);
                    $.ajax({
                        type: 'POST',
                        beforeSend:function (){
                            $(".mySubmitButton").hide()
                        },
                        url: form.attr('action'),
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            messageSuccessOrError(data)
                        },
                        error: function (data) {
                        },
                        complete:function (){
                            $(".mySubmitButton").show()
                            $(".empty_inputs").val('')
                        }
                    });

            });


            function messageSuccessOrError(data){
                if (data.status === true) {
                    var shortCutFunctionMessage = 'success',
                        titleMessage = 'نجاح',
                        message = data.message;
                } else {
                    var shortCutFunctionMessage = 'error',
                        titleMessage = 'فشل',
                        message = data.error[0];
                }
                var shortCutFunction = shortCutFunctionMessage;
                var msg = message;
                var title = titleMessage;
                toastr.options = {
                    positionClass: 'toast-top-left',
                    onclick: null
                };
                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;

                if (data.status === true && data.url) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1500);
                }
            }


        </script>


@endsection
