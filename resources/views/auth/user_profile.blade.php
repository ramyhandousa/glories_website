@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')
    <body class="bg-orders text-center">

    <div class="container text-right profile-info my-5">
        <div class="row">
            <form style="width:100%" id="dynamicFormSubmit" method="post" action="{{route('update_profile_user',\Illuminate\Support\Facades\Auth::id())}}" enctype="multipart/form-data">
                 {{ csrf_field() }}
                <div class="row">

                    <!-- For User    -->
                    <div class="col-xl-6 ">
                        <div class="avatar-upload">
                            <div class="avatar-edit">
                                <input name="image" class="choose_image" type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                <label for="imageUpload"></label>
                            </div>
                            <div class="avatar-preview">
                                <div id="imagePreview" style="background-image: url({{$user->image}});">  </div>
                            </div>
                        </div>
                        <div class="my-info my-5">
                            <h6 class="pb-3"> معلوماتي</h6>
                            <div class="form-group">
                                <input type="text" value="{{$user->name}}" name="name" class="form-control" placeholder="الاسم">
                            </div>
                            <div class="form-group">
                                <input type="email" value="{{$user->email}}" name="email" class="form-control" placeholder="البريد الإلكتروني">
                            </div>
                            <div class="form-group">
                                <input type="tel" value="{{$user->phone}}" name="phone" class="form-control" placeholder="رقم الجوال">
                            </div>
                            <div class="form-group">
                                <select name="city_id" class="form-control"   id="select-city">
                                    <option value="" disabled selected hidden  class="text-white">المدينة</option>
                                    @foreach($cities as $city)
                                        <option @if($city->id == $user->city_id) selected   @endif value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="text-center btn-edit">
                                <button type="submit" class="btn default-bg br-50 px-4 w-100">تعديل البيانات</button>
                                <a href="{{route('site.getEditPassword')}}"  class="btn default-bg br-50  w-100 mt-2">تعديل كلمة المرور</a>
                            </div>

                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
    @endsection

@section('scripts')
         <script src="{{asset('website_scripts/imagesVaildation.js')}}" defer></script>
        <script src="{{asset('website_scripts/dynamicFormSubmit.js')}}" defer> </script>
@endsection
