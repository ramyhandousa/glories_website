@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')

<body class="bg-login text-center">

    <!-- content -->
    <div class="container login-content">
        <img src="{{ asset('Site/logo.png')  }}" class="img-fluid intro-logo w-25">

        <form method="POST"  id="submitForm"  action="{{ route('site.AuthLogin') }}" data-parsley-validate novalidate>
            <div class="form-group">
                <input type="tel" name="phone" class="form-control requiredField " required  placeholder="رقم الجوال">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control requiredField" required id="login-password" placeholder="كلمة المرور">
                <i class="far fa-eye show-pass" onclick="showLogin()"></i>
            </div>

            <button id="buttonLogin"  type="submit" class="btn default-bg br-50 px-5  ">دخول</button>
            <a href="{{route('site.search')}}" class="d-block my-5 skip myLoader">تخطي</a>

        </form>
        <div class="my-5">
            <a href="{{route('activationCode')}}?type=resetPass" class="mx-3 text-white myLoader"> نسيت كلمة المرور</a>
            <a href="{{URL('intro')}}" class="mx-3 default-color  myLoader"> تسجيل حساب جديد</a>
        </div>
    </div>
    <!-- footer -->
@endsection


    @section('scripts')

        <script>

            $('#submitForm').on('submit', function (e) {

                e.preventDefault();

                var formData = new FormData(this);
                var formValid= $('#submitForm').parsley().validate();

                if(formValid){

                    $.ajax({
                        type: 'POST',
                        url: '{{ route('site.AuthLogin') }}',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {

                            messageSuccessOrError(data)
                        },
                        error: function (data) {
                        }
                    });

                }


            });

            function messageSuccessOrError(data){
                if (data.status === true || data.status === 200) {
                    var shortCutFunctionMessage = 'success',
                        titleMessage = 'نجاح',
                        message = data.message;
                } else {
                    var shortCutFunctionMessage = 'error',
                        titleMessage = 'فشل',
                        message = data.error[0];
                }
                var shortCutFunction = shortCutFunctionMessage;
                var msg = message;
                var title = titleMessage;
                toastr.options = {
                    positionClass: 'toast-top-left',
                    onclick: null
                };
                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;

                if (data.url){
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }

            }



        </script>


@endsection
