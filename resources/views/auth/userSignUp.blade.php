@extends('layouts.master')
@section('title', 'إدارة العملاء ')
@section('styles')
    <style>
        .help-block {
            color: #e74c3c !important;
            font-size: 16px !important;
            position: absolute;
            left: 15px;
            top: 50%;
            transform: translateY(-50%);

        }
        .has-error input{
            border-color: #e74c3c !important;
        }
        .validation_style {
            border: 1px solid #e74c3c !important;
        }
        .hidden-category{
            visibility: hidden !important;
        }
    </style>
@endsection


@section('content')
    <!-- content -->
    <body class="bg-signup text-center">

    <div class="container signup-content">
        <img src="{{ asset('Site/logo.png')  }}" class="img-fluid intro-logo w-25">


            <form method="POST" id="submitForm"  action="{{ route('sign_up_user') }}" data-parsley-validate novalidate>
                {{ csrf_field() }}
                <div class="row">

                    @include('.inputs_data.name')
                    @include('.inputs_data.phone')
                    @include('.inputs_data.email')
                    @include('.inputs_data.password')
                    @include('.inputs_data.password_confirmation')
                    @include('.inputs_data.city_select_options')
                    @include('.inputs_data.model_map')

                    <div class="form-group">
                        <button   type="submit" class="btn default-bg br-50 px-5 my-4 ">تسجيل</button>
                    </div>
                </div>
                <div class="row">
                    <p class="text-white my-5 mx-auto">بالتسجيل في تطبيق AR انت توافق على<br>
                        <a href="{{route('site.terms')}}" class="text-white">الشروط والاحكام</a>
                    </p>
                </div>
            </form>

    </div>



    @endsection

    @section('scripts')
        <script >
            $('#submitForm').on('submit', function (e) {

                e.preventDefault();

                var formData = new FormData(this),
                    form = $(this);
                form.parsley().validate();

                if (form.parsley().isValid()) {
                    $.ajax({
                        type: 'POST',
                        url: '{{ route('sign_up_user') }}',
                        beforeSend:function (){
                            $('.controlButton').hide();
                        },
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            messageSuccessOrError(data)
                        },
                        error: function (data) {
                            $('.controlButton').show();
                        }
                    });
                }
            });

            function messageSuccessOrError(data){
                if (data.status === true) {
                    var shortCutFunctionMessage = 'success',
                        titleMessage = 'نجاح',
                        message = data.message;
                } else {
                    var shortCutFunctionMessage = 'error',
                        titleMessage = 'فشل',
                        message = data.error[0];
                }
                var shortCutFunction = shortCutFunctionMessage;
                var msg = message;
                var title = titleMessage;
                toastr.options = {
                    positionClass: 'toast-top-left',
                    onclick: null
                };
                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;

                if (data.status === true) {
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1500);
                }
            }

        </script>

{{--        <script src="{{asset('website_scripts/checkColumExist.js')}}"></script>--}}
        <script src="{{asset('website_scripts/websiteMap.js')}}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjBZsq9Q11itd0Vjz_05CtBmnxoQIEGK8&language={{ config('app.locale') }}&libraries=places&callback=initAutocomplete"></script>

@endsection
