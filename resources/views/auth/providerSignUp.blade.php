@extends('layouts.master')
@section('title', 'تسجيل مزود خدمة ')
@section('styles')
    <style>
        .help-block {
            color: #e74c3c !important;
            font-size: 16px !important;
            position: absolute;
            left: 15px;
            top: 50%;
            transform: translateY(-50%);

        }
        .has-error input{
            border-color: #e74c3c !important;
        }
        .validation_style {
            border: 1px solid #e74c3c !important;
        }
        .hidden-category{
            visibility: hidden !important;
        }
    </style>
@endsection


@section('content')
    <!-- content -->
    <body class="bg-signup text-center">

    <div class="container signup-content">
        <img src="{{ asset('Site/logo.png')  }}" class="img-fluid intro-logo w-25">

            <form method="POST" action="{{route('contain.signUp') }}"  data-parsley-validate novalidate>


                {{ csrf_field() }}
                <div class="row">

                    @include('.inputs_data.name')
                    @include('.inputs_data.phone')
                    @include('.inputs_data.email')
                    @include('.inputs_data.password')
                    @include('.inputs_data.password_confirmation')
                    @include('.inputs_data.city_select_options')
                    @include('.inputs_data.model_map')


                    <div class="form-group">
                            <select name="category" class="form-control category requiredField" required>
                                <option value="" disabled selected hidden class="text-white">قسم الخدمة الرئيسي</option>
                                @foreach($categories as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach

                            </select>
                        </div>
                        <div class="form-group">
                            <select name="category" class="form-control hideDiv City_chid">
                                <option value="" disabled selected hidden class="text-white">قسم الخدمات الفرعي</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <select name="category" class="form-control hideDiv Child_City_chid">
                                <option value="" disabled selected hidden class="text-white">قسم الخدمات الفرعي</option>

                            </select>
                        </div>

                    <div class="form-group">
                        <button id="hiddenButton" href="{{route('contain.signUp') }}" class="btn default-bg br-50 px-5 my-4 ">التالي</button>
                    </div>

                </div>
                <div class="row">
                    <p class="text-white my-5 mx-auto">بالتسجيل في تطبيق AR انت توافق على<br>
                        <a href="{{route('site.terms')}}" class="text-white">الشروط والاحكام</a>
                    </p>
                </div>
            </form>

    </div>



    @endsection

    @section('scripts')
        <script src="{{asset('website_scripts/categories&sub.js')}}" defer></script>
        <script src="{{asset('website_scripts/checkColumExist.js')}}" defer></script>
        <script src="{{asset('website_scripts/websiteMap.js')}}" defer></script>

        <script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjBZsq9Q11itd0Vjz_05CtBmnxoQIEGK8&language={{ config('app.locale') }}&libraries=places&callback=initAutocomplete"></script>

@endsection
