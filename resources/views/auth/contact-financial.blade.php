@extends('layouts.master')
@section('title', 'تسجيل مزود خدمة ')

@section('content')
    <body class="bg-signup text-center">
    <div class="container">
        <img src="{{ asset('Site/images/illustrations/intro_logo.png') }}" class="img-fluid intro-logo w-25">
    </div>
    <div class="container text-white">
        <form id="submitForm" method="POST" action="{{ route('sign_up_provider') }}">
            {{ csrf_field() }}
            <div class="row">

                <input type="hidden" name="name" value="{{$name}}">
                <input type="hidden" name="phone" value="{{$phone}}">
                <input type="hidden" name="email" value="{{$email}}">
                <input type="hidden" name="password" value="{{$password}}">
                <input type="hidden" name="city_id" value="{{$cityId}}">
                <input type="hidden" name="address_search" value="{{$address_search}}">
                <input type="hidden" name="latitute" value="{{$latitute}}">
                <input type="hidden" name="longitute" value="{{$longitute}}">
                <input type="hidden" name="address" value="{{$address}}">
                <input type="hidden" name="category" value="{{$category}}">
                <div class="col-xl-6">
                    <div class="contact-border float-left">
                        <h5 class="text-center">حدد كيف يتواصل العملاء معك</h5>
                        <div class="custom-control custom-switch my-2">
                            <input type="checkbox" class="custom-control-input" id="switch1" name="phoneConnect">
                            <label class="custom-control-label" for="switch1">عن طريق الجوال</label>
                        </div>
                        <div class="custom-control custom-switch my-2">
                            <input type="checkbox" class="custom-control-input" id="switch2" name="emailConnect">
                            <label class="custom-control-label" for="switch2">عن طريق البريد الالكتروني</label>
                        </div>
                        <div class="custom-labels col-xl-12 text-right pl-0 pr-4">
                            <label class="">عن طريق الفيسبوك</label>
                            <input type="hidden" id="myFacebook" value="0" name="faceBook" >
                            <label class="float-left"   id="contact-fb">الغاء الربط</label>
                        </div>
                        <div class="custom-labels col-xl-12 text-right pl-0 pr-4">
                            <label class="">عن طريق تويتر</label>
                            <input type="hidden" id="myTwitter" value="0" name="twitter" >
                            <label class="float-left"   id="contact-tw">الغاء الربط</label>
                        </div>
                        <div class="custom-labels col-xl-12 text-right pl-0 pr-4">
                            <label class="">عن طريق سناب شات</label>
                            <input type="hidden" id="mySnapChat" value="0" name="snapChat" >
                            <label class="float-left"   id="contact-sn">الغاء الربط</label>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="contact-border">
                        <h5 class="text-center">حدد طرق الدفع المناسبة لك</h5>
                        <div class="custom-control custom-switch my-2">
                            <input type="checkbox" class="custom-control-input" id="switch3" name="bank">
                            <label class="custom-control-label" for="switch3">تحويل بنكي</label>
                        </div>
                        <div class="custom-control custom-switch my-2">
                            <input type="checkbox" class="custom-control-input" id="switch4" name="cash">
                            <label class="custom-control-label" for="switch4">كاش</label>
                        </div>
                        <div class="custom-control custom-switch my-2">
                            <input type="checkbox" class="custom-control-input" id="switch5" name="pay">
                            <label class="custom-control-label" for="switch5">سداد</label>
                        </div>
                        <div class="custom-control custom-switch my-2">
                            <input type="checkbox" class="custom-control-input" id="switch6" name="online">
                            <label class="custom-control-label" for="switch6">(تحويل الكتروني(بايبال</label>
                        </div>

                    </div>
                </div>
                <div class="col-xl-12 text-center my-5">
                    {{--<a href="orders.html" type="submit" class="btn default-bg br-50 px-5 my-4">تسجيل</a>--}}
                    <button  type="submit" class="btn default-bg br-50 px-5 my-4">تسجيل</button>
                </div>


            <p class="text-white mx-auto">بالتسجيل في تطبيق AR انت توافق على<br>
                <a href="" class="text-white">الشروط والاحكام</a>
            </p>


        </div>
        </form>
    </div>
@endsection

    @section('scripts')

        <script>

            $('#contact-fb').click(function () {
                if ($('#contact-fb').hasClass('contact')){  $("#myFacebook").val(1); }
            });

            $('#contact-tw').click(function () {
                if ($('#contact-tw').hasClass('contact')){  $("#myTwitter").val(1); }
            });

            $('#contact-sn').click(function () {
                if ($('#contact-sn').hasClass('contact')){  $("#mySnapChat").val(1); }
            });



            $('#submitForm').on('submit', function (e) {

                e.preventDefault();

                var formData = new FormData(this);

                $.ajax({
                    type: 'POST',
                    url: '{{ route('sign_up_provider') }}',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        messageSuccessOrError(data)


                    },
                    error: function (data) {
                    }
                });

            });


            function messageSuccessOrError(data){
                if (data.status === true || data.status === 200) {
                    var shortCutFunctionMessage = 'success',
                        titleMessage = 'نجاح',
                        message = data.message;
                } else {
                    var shortCutFunctionMessage = 'error',
                        titleMessage = 'فشل',
                        message = data.error[0];
                }
                var shortCutFunction = shortCutFunctionMessage;
                var msg = message;
                var title = titleMessage;
                toastr.options = {
                    positionClass: 'toast-top-left',
                    onclick: null
                };
                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                $toastlast = $toast;

                if (data.url){
                    setTimeout(function () {
                        window.location.href = data.url;
                    }, 1000);
                }

            }



        </script>





@endsection
