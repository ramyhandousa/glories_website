@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')
    <body class="bg-orders text-center">

    <!-- content -->
    <div class="container text-right my-5">
        <div class="row">

            @if( request('type') == 'waiting' && count($waiting) > 0)
                <div class="col-xl-6">
                    <h6 class="text-white">طلبات جديده</h6>
                    <ul>
                        @foreach($waiting as $value)
                            <li class="order">
                                <form method="GET" action="{{ route('orders.show',$value->id) }}"  id="order{{ $value->id }}" >

                                    <a href="#" onclick="return $('#order{{ $value->id }}').submit()" class="row default-color">
                                        <div class="col-xl-6 col-6 bl-before">
                                            <div>
                                                <span class="d-inline-block text-white">إسم المتعاقد :</span>
                                                <span class="d-inline-block">{{  $value->name}}</span>
                                            </div>
                                            <div>
                                                <span class="d-inline-block text-white">إسم المنتج :</span>
                                                <span class="d-inline-block">{{optional($value->product)->name}}</span>
                                            </div>
                                            <div>
                                                <span class="d-inline-block text-white">سعر المنتج :</span>
                                                <span class="d-inline-block">{{$value->price_product}}</span>
                                            </div>
                                         </div>
                                        <div class="col-xl-6 col-6">
                                            <div>
                                                <span class="d-inline-block text-white">تاريخ الحجز :</span>
                                                <span class="d-inline-block">{{ $value->date}}</span>
                                            </div>
                                            <div>
                                                <span class="d-inline-block text-white">وقت الحجز :</span>
                                                <span class="d-inline-block">{{ $value->time}}</span>
                                            </div>
                                            <div>
                                                <span class="d-inline-block text-white">المدينة :</span>
                                                <span class="d-inline-block">{{optional($value->city)->name}}</span>
                                            </div>
                                        </div>
                                    </a>
                                </form>
                            </li>
                        @endforeach
                    </ul>

                </div>
            @endif


            @if( (request('type') == 'income' || request('type') == 'outcome' ) &&  count($current) > 0)
                <div class="col-xl-6">
                    <h6 class="text-white">طلبات جارية</h6>
                    <ul>
                        @foreach($current as $value)
                            <li class="order">
                                <form method="GET" action="{{ route('orders.show',$value->id) }}"  id="order{{ $value->id }}" >

                                    <a href="#" onclick="return $('#order{{ $value->id }}').submit()" class="row default-color">
                                        <div class="col-xl-6 col-6 bl-before">
                                            <h5>{{optional($value->product)->name}}</h5>
                                        </div>
                                        <div class="col-xl-6 col-6">
                                            <div>
                                                <span class="d-inline-block text-white">رقم الطلب :</span>
                                                <span class="d-inline-block">{{$value->id}}</span>
                                            </div>
                                            <div>
                                                <span class="d-inline-block text-white">تاريخ الطلب :</span>
                                                <span class="d-inline-block">{{$value->created_at->diffForHumans()}}  </span>
                                            </div>
                                            <div>
                                                <span class="d-inline-block text-white">التكلفة :</span>
                                                <span class="d-inline-block">{{$value->price_product}}  </span>
                                            </div>
                                        </div>
                                    </a>
                                </form>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(request('type') == 'income' && count($finish) > 0)
                <div class="col-xl-6">
                    <h6 class="text-white">طلبات منفذه</h6>
                    <ul>
                        @foreach($finish as $value)
                            <li class="order">
                                <form method="GET" action="{{ route('orders.show',$value->id) }}"  id="order{{ $value->id }}" >

                                    <a href="#" onclick="return $('#order{{ $value->id }}').submit()" class="row default-color">
                                        <div class="col-xl-6 col-6 bl-before">
                                            <h5>{{optional($value->product)->name}}</h5>
                                        </div>
                                        <div class="col-xl-6 col-6">
                                            <div>
                                                <span class="d-inline-block text-white">رقم الطلب :</span>
                                                <span class="d-inline-block">{{$value->id}}</span>
                                            </div>
                                            <div>
                                                <span class="d-inline-block text-white">تاريخ الطلب :</span>
                                                <span class="d-inline-block">{{$value->created_at->diffForHumans()}}  </span>
                                            </div>
                                            <div>
                                                <span class="d-inline-block text-white">التكلفة :</span>
                                                <span class="d-inline-block">{{$value->price_product}}  </span>
                                            </div>
                                        </div>
                                    </a>
                                </form>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif


        </div>
    </div>

@endsection
