@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')
    <body class="bg-orders text-center">

    <div class="container text-right my-5 new-order">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(isset($order))
            <ul class="row">
                <li class=" col-xl-6">
                    <div class="order default-color ">
                        <div class="row mt-4">
                            <div class="col-xl-4 col-4 bl-before">
                                <h5>{{$order->name}} </h5>
                                <span class="text-white">  إسم المتعاقد</span>
                            </div>
                            <div class="col-xl-4 col-4 bl-before">
                                <h5>{{$order->phone}} </h5>
                                <span class="text-white">  رقم المتعاقد</span>
                            </div>
                            <div class="col-xl-4 col-4">
                                <div>
                                    <span class="d-inline-block text-white">رقم الطلب :</span>
                                    <span class="d-inline-block">{{ $order->id }}</span>
                                </div>
                                <div>
                                    <span class="d-inline-block text-white">حالة الطلب :</span>
                                    <span class="d-inline-block"> {{ $order->status_translation }} </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class=" col-xl-6">
                    <div class="order default-color text-center">
                        <h6>التاريخ</h6>
                        <div class="row ">

                            <div class="col-xl-4 col-4 bl-before">
                                <h5> {{ Carbon\Carbon::parse($order->created_at)->diffForHumans()  }} </h5>
                                <span class="text-white">تاريخ الطلب</span>
                            </div>
                            @if($order->date)
                                <div class="col-xl-4 col-4 bl-before">
                                    <h5>    {{ $order->date }} </h5>
                                    <span class="text-white">تاريخ بدء الطلب</span>

                                </div>
                            @endif
                            <div class="col-xl-4 col-4">
                                <h5> {{ $order->time }}   </h5>
                                <span class="text-white">وقت بدء الطلب</span>
                            </div>

                        </div>
                    </div>
                </li>
                <li class=" col-xl-12">
                    <div class="order default-color text-center">
                        <h6>بيانات المنتج</h6>
                        <div class="row ">
                            <div class="col-xl-3 col-3 bl-before">
                                <img class="card-img" style="max-width: 100px;max-height: 100px"
                                     src="@if (optional($order->product)->images_path) {{optional($order->product)->images_path[0]}} @else{{ asset('category.jpg') }}@endIf"
                                     alt="صورة المنتج ">
                                <span class="text-white"> صورة المنتج </span>
                            </div>
                            <div class="col-xl-3 col-3 bl-before">
                                <h5> {{ optional($order->product)->name  }} </h5>
                                <span class="text-white"> إسم المنتج </span>
                            </div>

                            <div class="col-xl-2 col-2 bl-before">
                                <h5>    {{ $order->price_product }} </h5>
                                <span class="text-white">    سعر المنتج </span>
                            </div>
                            @if($order->is_offer == 1)
                                <div class="col-xl-2 col-2 bl-before">
                                    <h5>    {{ $order->price_offer }} </h5>
                                    <span class="text-white">    سعر خصم المنتج </span>
                                </div>
                            @endif
                            <div class="col-xl-2 col-2">
                                <h5> {{ optional($order->category)->name }}   </h5>
                                <span class="text-white">قسم المنتج  </span>
                            </div>

                        </div>
                    </div>
                </li>
                <li class=" col-xl-6">
                    <div class="order default-color text-center">
                        <h6>المكان</h6>
                        <div class="row ">

                            <div class="col-xl-6 col-6 bl-before">
                                <h5>{{optional($order->city)->name}}</h5>
                                <span class="text-white">المدينة</span>
                            </div>
                            <div class="col-xl-6 col-6">

                                {{--<button type="button" class="d-block mx-auto br-50 call">عرض الخريطه</button>--}}
                                <span class="text-white">العنوان</span>
                                <span class="text-white">{{$order->address}}</span>

                            </div>
                        </div>
                    </div>
                </li>
                <li class=" col-xl-6 ml-0">
                    <div class="order default-color text-center">
                        <h6>التكلفة والدفع</h6>
                        <div class="row ">
                            <div class="col-xl-6 col-6 bl-before">
                                <h5> {{$order->price_product}}    </h5>
                                <span class="text-white">التكلفة</span>
                            </div>
                            <div class="col-xl-6 col-6">
                                <h5>{{$order->payment_type_translation}}    </h5>
                                <span class="text-white">طريقه الدفع</span>
                            </div>
                        </div>
                    </div>
                </li>

                    <li class=" col-xl-6  ">
                        <div class="order default-color text-center">

                            <div class="row ">
                                <div class="col-xl-12 col-12  ">
                                    <h5> {{$order->status_translation}}    </h5>
                                </div>
                            </div>
                        </div>
                    </li>

                @if(Auth()->id() == $order->user_id)
                    @include('orders.communicate')
                @endIf
            </ul>

            @if(Auth()->id() == $order->provider_id)
               @include('orders.action')
            @endIf
        @endif

    </div>

@endsection


@section('scripts')

    <script>
        $("#mylist li:last").removeClass("bl-before");

    </script>

@endsection
