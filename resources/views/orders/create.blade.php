@extends('layouts.master')
@section('title', 'إدارة العملاء ')
@section('styles')

    <link rel="stylesheet" href="{{asset('Site/css/bootstrap-material-datetimepicker.css')}}">
@endsection
@section('content')
    <body class="bg-orders text-center">

    <div class="container text-right my-5 text-white">
        <div class="row">
            <div class="col-xl-6 mb-5">
                <div class="card br-0 serv-order-bg">
                    @if(isset($product))
                        <img class="card-img" src="@if ($product->images_path) {{$product->images_path[0]}} @else{{ asset('category.jpg') }}@endIf" alt="">

                        <div class="card-body">
                            <h5 class="card-title default-color">{{$product->name }}</h5>
                            <p class="card-text">
                                {{$product->description}}
                            </p>

                            <div>
                                <span><b class="default-color "> {{$product->price}}</b>  ريال سعودي</span>
                            </div>

                            @if($product->offer)
                                <h4>السعر بعد العرض </h4>
                                <span><b class="default-color "> {{$product->offer->price}}</b>  ريال سعودي</span>
                            @endif
                        </div>
                    @endif

                </div>
            </div>

            <div class="col-xl-6 mb-5">
                <h6 class="pb-3 text-center">طلب الخدمة</h6>

                <form id="dynamicFormSubmit" method="post" action="{{route('orders.store')}}" class="product-info w-75 mx-auto">
                    {{ csrf_field() }}

                    @include('orders.inputs.old_data')
                    @include('orders.inputs.name')
                    @include('orders.inputs.phone')
                    @include('inputs_data.city_select_options')
                    @include('orders.inputs.address')
                    @include('orders.inputs.time')
                    @include('orders.inputs.date')
                    @include('orders.inputs.payment')

                    <div class="col-xl-12 text-center my-5 add-pro-btn">
                        @if(\Illuminate\Support\Facades\Auth::check())
                            <button type="submit"   class="btn default-bg br-50 text-dark mx-2 my-2">ارسال الطلب</button>
                        @else
                            <a href="{{route('site.login')}}"  class="btn default-bg br-50 text-dark mx-2 my-2" >ارسال الطلب </a>
                        @endif
                    </div>
                </form>
            </div>

        </div>

    </div>
@endsection

@section('scripts')

    <script src="{{asset('Site/js/bootstrap-material-datetimepicker.js')}}" ></script>
        <script src="{{asset('website_scripts/dynamicFormSubmit.js')}}" defer></script>
        <script src="{{asset('website_scripts/websiteMap.js')}}" defer></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjBZsq9Q11itd0Vjz_05CtBmnxoQIEGK8&language={{ config('app.locale') }}&libraries=places&callback=initAutocomplete" defer></script>

{{--   	<script src="https://code.jquery.com/jquery-1.12.3.min.js" integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ=" crossorigin="anonymous"></script>--}}
		<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/ripples.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-material-design/0.5.10/js/material.min.js"></script>
		<script type="text/javascript" src="https://rawgit.com/FezVrasta/bootstrap-material-design/master/dist/js/material.min.js"></script>
		<script type="text/javascript" src="http://momentjs.com/downloads/moment-with-locales.min.js"></script>
	  <script src="{{asset('Site/js/bootstrap-material-datetimepicker.js')}}" ></script>

    <script>
        $('#time').bootstrapMaterialDatePicker
        ({
            date: false,
            shortTime: false,
            format: 'HH:mm'
        });
    </script>
    <script>
			(function(i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function() {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

			ga('create', 'UA-60343429-1', 'auto');
			ga('send', 'pageview');
		</script>
    <script type="text/javascript">
		$(document).ready(function()
		{
			$('#date').bootstrapMaterialDatePicker
			({
				time: false,
				clearButton: true
			});

			$('#time').bootstrapMaterialDatePicker
			({
				date: false,
				shortTime: false,
				format: 'HH:mm'
			});

			$('#date-format').bootstrapMaterialDatePicker
			({
				format: 'dddd DD MMMM YYYY - HH:mm'
			});
			$('#date-fr').bootstrapMaterialDatePicker
			({
				format: 'DD/MM/YYYY HH:mm',
				lang: 'fr',
				weekStart: 1,
				cancelText : 'ANNULER',
				nowButton : true,
				switchOnClick : true
			});

			$('#date-end').bootstrapMaterialDatePicker
			({
				weekStart: 0, format: 'DD/MM/YYYY HH:mm'
			});
			$('#date-start').bootstrapMaterialDatePicker
			({
				weekStart: 0, format: 'DD/MM/YYYY HH:mm', shortTime : true
			}).on('change', function(e, date)
			{
				$('#date-end').bootstrapMaterialDatePicker('setMinDate', date);
			});

			$('#min-date').bootstrapMaterialDatePicker({ format : 'DD/MM/YYYY HH:mm', minDate : new Date() });

			$.material.init()
		});
		</script>
@endsection



