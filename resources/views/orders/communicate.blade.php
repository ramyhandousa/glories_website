
<li class=" col-xl-4 mr-0">
    <div class="order default-color text-center">
        <h6>تواصل مع مزود الخدمة</h6>
        <div>
            <ul class="row provider-social mt-4" id="mylist">
                @if(optional($order['provider']->provider)->connect_email == 1)
                    <li class="col-xl-2 col-2 bl-before">
                        <i title="{{$order['provider']->email}}" class="fas fa-envelope"></i>
                    </li>
                @endif

                @if(optional($order['provider']->provider)->snap_chat)
                    <li class="col-xl-2 col-2 bl-before">
                        <i title="{{$order['provider']->provider->snap_chat}}"  class="fab fa-snapchat-ghost"></i>
                    </li>
                @endif

                @if(optional($order['provider']->provider)->facebook)
                    <li class="col-xl-2 col-2 bl-before">
                        <i title="{{$order['provider']->provider->facebook}}" class="fab fa-facebook-f"></i>
                    </li>
                @endif

                @if(optional($order['provider']->provider)->connect_phone == 1)
                    <li class="col-xl-2 col-2 bl-before">
                        <i title="{{$order['provider']->phone}}" class="fas fa-phone"></i>
                    </li>
                @endif

                @if(optional($order['provider']->provider)->twitter)
                    <li class="col-xl-2 col-2 ">
                        <i title="{{$order['provider']->provider->twitter}}" class="fab fa-twitter"></i>
                    </li>
                @endif

            </ul>

        </div>
    </div>
</li>
