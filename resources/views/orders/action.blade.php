<div class="newOrder-btn my-5 text-center">

    @if($order->status_info == 'processing' ||  $order->status == 1)
        <form method="post" action="{{route('finish_order',$order->id)}}" class="product-info w-75 mx-auto">
            {{ csrf_field() }}
            <button type="submit"  class="btn default-bg br-50 text-dark mx-2 my-2">إنهاء الطلب</button>
        </form>

    @endif

    @if($order->status_info == 'pending' || $order->status == 0)

        <form method="post" action="{{route('accepted_refuse_order',$order->id)}}?type=1"   class="product-info w-75 mx-auto">
            {{ csrf_field() }}
            <button type="submit"  class="btn default-bg br-50 text-dark mx-2 my-2">قبول الطلب</button>
            <button type="button" class="btn provider-btn bg-none br-50 mx-2 my-2" data-toggle="modal" data-target="#exampleModal">
                رفض الطلب
            </button>
        </form>

{{--        <form method="post" action="{{route('accepted_refuse_order',$order->id)}}?type=-1"   class="product-info w-75 mx-auto">--}}
{{--            {{ csrf_field() }}--}}
{{--            <button type="submit" class="btn provider-btn bg-none br-50 mx-2 my-2">رفض الطلب</button>--}}
{{--        </form>--}}

        <!-- Modal -->
        <div  class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">

                <div class="modal-content" style="background-color: black;color: white">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">سبب رفض الطلب </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form method="post" action="{{route('accepted_refuse_order',$order->id)}}?type=-1"   class="product-info w-75 mx-auto">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            <textarea name="message" class="form-control"  cols="30" rows="10" required maxlength="500"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn provider-btn ">رفض   </button>
                            <button type="button" class="btn provider-btn " data-dismiss="modal">إلغاء</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>

    @endIf
</div>
