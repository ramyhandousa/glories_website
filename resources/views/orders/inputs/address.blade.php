<div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}"  data-toggle="modal" data-target="#mapModal" >
    <input type="text" required id="address_search" name="address_search" class="form-control "  placeholder="العنوان" >
    <input type="hidden" name="latitute" id="lat" value="{{old('latitute')}}"/>
    <input type="hidden" name="longitute" id="lng" value="{{old('longitute')}}"/>
    <input type="hidden" name="address" id="address" value="{{old('address')}}"/>
    @if($errors->has('address'))
        <p class="help-block">
            {{ $errors->first('address') }}
        </p>
    @endif
    <i class="fas fa-map-marker-alt form-icon"></i>
</div>

<div class="modal fade" id="mapModal">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content" >
            <!-- Modal body -->
            <div class="modal-body">
                <div  id="googleMap"></div>
            </div>
        </div>
    </div>
</div>

