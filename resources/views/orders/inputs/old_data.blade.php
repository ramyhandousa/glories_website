@if(isset($product))
    <input type="hidden" name="provider_id" value="{{request('userId')}}">
    <input type="hidden" name="product_id" value="{{request('productId')}}">
    <input type="hidden" name="price_product" value="{{$product->price}}">
    <input type="hidden" name="is_offer" value="{{ $product->offer ? 1 : 0 }}">
    <input type="hidden" name="price_offer" value="{{optional($product->offer)->price}}">
    <input type="hidden" name="category_product" value="{{$product->category_id}}">
@endif
