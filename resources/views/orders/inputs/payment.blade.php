
<div class="form-group {{ $errors->has('payment_type') ? ' has-error' : '' }}">
        <select  required name="payment_type" class="form-control">
            <option value=""  disabled selected hidden class="text-white">طريقة الدفع المناسبة </option>
            @if(isset($provider))
                @if($provider->bank_transfer == 1)
                    <option value="1">تحويل بنكي</option>
                @endif

                @if($provider->cash == 1)
                    <option value="3">كاش</option>
                @endif

                @if($provider->pay == 1)
                    <option value="2">سداد</option>
                @endif

                @if($provider->pay_electronic == 1)
                    <option value="4">تحويل الكتروني</option>
                @endif
            @endif
        </select>
    @if($errors->has('payment_type'))
        <p class="help-block">
            {{ $errors->first('payment_type') }}
        </p>
    @endif
</div>
