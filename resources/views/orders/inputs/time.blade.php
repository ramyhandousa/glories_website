<div class="form-group sub-section {{ $errors->has('time') ? ' has-error' : '' }}">
    <input type="text" id="time" required name="time" class="form-control " placeholder="الوقت">
    @if($errors->has('time'))
        <p class="help-block">
            {{ $errors->first('time') }}
        </p>
    @endif
    <i class="fas fa-clock form-icon"></i>
</div>
