<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
    <input type="tel" name="phone" required class="form-control" placeholder="رقم الجوال">
    @if($errors->has('phone'))
        <p class="help-block">
            {{ $errors->first('phone') }}
        </p>
    @endif
</div>
