<div class="form-group sub-section float-left {{ $errors->has('date') ? ' has-error' : '' }}">

    <input type="text" id="date" required name="date" class="form-control " placeholder="التاريخ">
    @if($errors->has('date'))
        <p class="help-block">
            {{ $errors->first('date') }}
        </p>
    @endif
    <i class="fas fa-calendar-alt form-icon"></i>
</div>
