@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')

    <body class="bg-orders text-center">

    <div class="container text-right  my-5 text-white">
    @if(count($current) > 0 || count($last) > 0 )
        <!-- new notification -->
            @if(count($current) > 0)
                <div class="new-not">
                    <h5>تنبيهات جديدة</h5>
                    <ul class="row">
                        @foreach($current as $value)
                            <li class=" col-xl-4 col-lg-6 col-md-6">
                                <div class="order">

                                    <a href="@if($value->order){{route('orders.show',$value->order->id)}} @else#@endif" @if($value->order) target="_blank" @endif>
                                        <span class="time text-secondary ">{{$value->created_at->diffForHumans()}}  </span>
                                        <h5 class="default-color">{{$value->title}}</h5>
                                        <p class="text-white">{{$value->body}}</p>
                                    </a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <!-- old notification -->
            @if(count($last) > 0)
                <div class="old-not">
                    <h5>تنبيهات سابقه</h5>
                    <ul class="row">
                        @foreach($last as $value)
                            <li class=" col-xl-4 col-lg-6 col-md-6">
                                <div class="order">
                                    <a href="@if($value->order){{route('orders.show',$value->order->id)}} @else#@endif" target="_blank">
                                        <span class="time text-secondary ">{{$value->created_at->diffForHumans()}}  </span>
                                        <h5 class="default-color">{{$value->title}}</h5>
                                        <p class="text-white">{{$value->body}}</p>
                                    </a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        @else
            <img loading="lazy" style="width: inherit" src="{{ asset('empty_data.png')  }}">
        @endif
    </div>
@endsection
