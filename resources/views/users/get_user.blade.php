@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')

    <body class="bg-provider text-center">
    <div class="container text-right  my-5 text-white ">
        <div class="row">
            <div class="col-xl-6 text-center">
                <img src="{{$user->image}}" class="img-fluid provider-img">
                <h4 class="default-color  py-3">
                    {{$user->name}}
                </h4>

                <span class="float-left default-color rate w-100">
                    <i class="fas fa-star"></i> {{$user->rate}}
                </span>
                <div>
                    <ul class="row provider-social pt-4 mx-auto w-75">
                        <li class="col-xl-2 col-2 bl-before">
                            <i class="fas fa-envelope"></i>
                        </li>
                        <li class="col-xl-2 col-2 bl-before">
                            <i class="fab fa-snapchat-ghost"></i>
                        </li>
                        <li class="col-xl-2 col-2 bl-before">
                            <i class="fab fa-facebook-f"></i>
                        </li>
                        <li class="col-xl-2 col-2 bl-before">
                            <i class="fas fa-phone"></i>
                        </li>
                        <li class="col-xl-2 col-2 ">
                            <i class="fab fa-twitter"></i>
                        </li>
                    </ul>

                </div>
            </div>
            <div class="col-xl-6 provider-services">
                <h5>المنتجات</h5>
                <ul class=" p-0">
                    @foreach($products as $product)
                        <li>

                              <form method="GET"  action="{{ route('orders.create')}}" id="changeProduct{{ $product->id }}">
                                <div class="card ">
                                    <div class="card-body p-0 card-body-img">
                                        <a href="#" onclick="return $('#changeProduct{{ $product->id }}').submit()">
                                            <input type="hidden" name="userId" value="{{$user->id}}">
                                            <input type="hidden" name="productId" value="{{$product->id}}">
                                            <img class="card-img" loading="lazy"alt="image_product"
                                                 src="@if ($product->images_path) {{$product->images_path[0]}} @else{{ asset('category.jpg') }}@endIf">
                                        </a>
                                    </div>
                                    <div class="card-body">
                                        <a href="#" onclick="return $('#changeProduct{{ $product->id }}').submit()">
                                            <h5 class="card-title default-color">{{$product->name}}</h5>
                                            <p class="card-text">
                                                {{$product->description}}
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            </form>

                            @if(\Illuminate\Support\Facades\Auth::check())
                                <form method="post" class="myLoader"  action="{{ route('site.makeFavourite') }}" id="makeFavourite{{ $product->id }}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{$user->id}}">
                                    <input type="hidden" name="productId" value="{{$product->id}}">
                                    <div class="card-body dir">
                                        <a href="javascript:;" class="heart" onclick="return $('#makeFavourite{{ $product->id }}').submit()" >
                                            <i class="fa fa-heart" @if($product->isFavorite == 1) style="color: red" @endif aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </form>
                            @endif
                        </li>

                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    @endsection
    @section('scripts')


        <script type="text/javascript">

            {{--function favourite($productId, $userId) {--}}
            {{--    var id = $userId;--}}
            {{--    var productId = $productId;--}}

            {{--    $.ajax({--}}
            {{--        type: 'POST',--}}
            {{--        url: '{{ route('site.makeFavourite') }}',--}}
            {{--        headers:{--}}
            {{--            'Accept' : 'application/json'--}}
            {{--        },--}}
            {{--        data: {--}}
            {{--            id: id,--}}
            {{--            productId: productId--}}
            {{--        },--}}
            {{--        dataType: 'json',--}}
            {{--        beforeSend:function () {--}}
            {{--            $(".hiddenContent").toggle();--}}
            {{--            $(".hiddenLoading").toggle();--}}
            {{--        },--}}
            {{--        cache: false,--}}
            {{--        success: function (data) {--}}
            {{--            if (data.status == 200) {--}}

            {{--                var shortCutFunction = 'success';--}}
            {{--                var msg = data.message;--}}
            {{--                var title = 'نجاح';--}}
            {{--                toastr.options = {--}}
            {{--                    positionClass: 'toast-top-left',--}}
            {{--                    onclick: null--}}
            {{--                };--}}
            {{--                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists--}}
            {{--                $toastlast = $toast;--}}

            {{--            }--}}

            {{--            if (data.status == 400) {--}}
            {{--                var shortCutFunction = 'error';--}}
            {{--                var msg = data.error[0];--}}
            {{--                var title = 'خطأ';--}}
            {{--                toastr.options = {--}}
            {{--                    positionClass: 'toast-top-left',--}}
            {{--                    onclick: null--}}
            {{--                };--}}
            {{--                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists--}}
            {{--                $toastlast = $toast;--}}

            {{--            }--}}
            {{--        },--}}
            {{--        complete: function () {--}}
            {{--            $(".hiddenLoading").toggle();--}}
            {{--            $(".hiddenContent").toggle();--}}
            {{--        }--}}
            {{--    });--}}

            {{--}--}}


        </script>

@endsection
