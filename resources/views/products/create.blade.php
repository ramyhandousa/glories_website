@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('styles')
    <style>

        .hidden-category{
            visibility: hidden !important;
        }
        .help-block {
            color: #e74c3c !important;
            font-size: 16px !important;

        }

    </style>
@endsection

@section('content')

<body class="bg-orders text-center">

    <!-- content -->
    <div class="container text-right my-5 text-white">
        <div class="row">
            <div class="col-xl-4 mb-5">
                <h6 class="pb-3">معلومات الاشتراك</h6>

                <div class="order default-color text-center">
                    <h6>رصيد منتجاتك</h6>
                    <div class="row ">
                        <div class="col-xl-6 col-6 bl-before">
                            @if(isset($countproductHave))
                                <h1>{{$countproductHave}}</h1>
                            @endif
                            <span class="text-white">منتجات معروضة</span>
                        </div>
                        <div class="col-xl-6 col-6">
                            @if(isset($countproductRemind))
                                <h1>{{$countproductRemind}}</h1>
                            @endif
                            <span class="text-white">منتجات متبقية</span>
                        </div>
                    </div>
                    <p class="text-white">
                        لتتمكن من عرض منتجات وخدمات اكثر بامكانك تحديث اشتراكك
                    </p>
                    <a href="" class="default-color"> عرض الاشتراكات </a>
                </div>

            </div>
            <div class="col-xl-8">
            <form method="post" id="dynamicFormSubmit" class="product-info" action="{{route('products.store') }}" enctype="multipart/form-data" data-parsley-validate novalidate>
                {{ csrf_field() }}
                <div class="row">

                    <div class="col-xl-6 mb-5">
                        <h6 class="pb-3">صور المنتج (بامكانك اضافة حتى 5 صور)</h6>
                        @for ($i = 0; $i < 5; $i++)
                            <div class="wrap-custom-file">
                                <input type="file" name="images[]" class="choose_image"  id="image{{$i}}" accept="image/png,image/jpeg,image/jpg" />
                                <label for="image{{$i}}" id="image{{$i}}">
                                    <img src="{{ asset('Site/images/icons/prf_add_photo.png')  }}"
                                         id="add{{$i}}">
                                </label>

                            </div>
                        @endfor
                    </div>

                    <div class="col-xl-6 mb-5">
                        <h6 class="pb-3">معلومات المنتج</h6>
                        <form action="" class="product-info">
                            <div class="form-group">
                                <input name="product_name" value="{{old('product_name')}}" type="text" required class="form-control requiredField" placeholder="اسم المنتج / الخدمة">
                            </div>
                            <div class="form-group">
                                <div class="form-group">
                                    <select  name="category" class="form-control requiredField category" required>
                                        <option value="" disabled selected hidden class="text-white">القسم الرئيسي</option>
                                        @foreach($categories as $item)
                                            <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group sub-section ">

                                <select name="category" class="form-control hideDiv City_chid">
                                    <option value="" disabled selected hidden class="text-white">القسم الفرعي</option>
                                </select>

                            </div>
                            <div class="form-group sub-section float-left ">

                                <select name="category" class="form-control hideDiv Child_City_chid">
                                    <option value="" disabled selected hidden class="text-white">القسم الفرعي</option>
                                </select>

                            </div>
                            <div class="form-group">
                                <input name="description" value="{{old('description')}}" required type="text" class="form-control requiredField" placeholder="الوصف">
                            </div>
                            <div class="form-group">
                                <input name="price" value="{{old('price')}}" type="text" required class="form-control requiredField" placeholder="السعر">
                                <span class="currency">ريال سعودي</span>
                            </div>
                        </form>
                    </div>
                    <div class="col-xl-12 text-center my-5 add-pro-btn">
                        <button type="submit" id="hiddenButton" class="btn default-bg br-50 text-dark mx-2 my-2">اضافة المنتج</button>
                        <a class="btn provider-btn br-50 mx-2 my-2">الغاء</a>
                    </div>
                </div>

            </form>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
        <script src="{{asset('website_scripts/categories&sub.js')}}" defer></script>
        <script src="{{asset('website_scripts/imagesVaildation.js')}}" defer></script>
        <script src="{{asset('website_scripts/dynamicFormSubmit.js')}}" defer></script>

@endsection
