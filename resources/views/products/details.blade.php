@extends('layouts.master')
@section('title', 'إدارة العملاء ')
@section('styles')
    <link rel="stylesheet" href="{{asset('Site/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('Site/css/owl.theme.default.min.css')}}">
    <style>
        #productImages .owl-item img {
            display: block;
            width: 100%;
            height: 400px;
            border-radius: 40px;
        }

    </style>
@endsection
@section('content')
    <body class="bg-orders text-center">

    <!-- content -->
    <div class="container text-right my-5 text-white">
        <div class="row">
            <div class="col-md-6">
                <div class="owl-carousel owl-theme " id="productImages">
                    @if($product->images_path)
                        @foreach($product->images_path as $image)
                            <div class="item ">
                                <img src="{{ $image }}" class="img-fluid br-5" alt="">
                            </div>
                        @endforeach
                    @else
                        <div class="item ">
                            <img src="{{asset('Site/images/pic/1.jpeg')}}" class="img-fluid br-5" alt="صورة">
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <div class="my-subscripe h-100">
                    <h4 class="pb-3  default-color">
                             اسم المنتج / الخدمة : {{$product->name}}
                    </h4>
                    <h5 class="text-white">
                          القسم الرئيسي :  {{$product->category->parent->parent->name ?? $product->category->parent->name ?? $product->category->name }}
                    </h5>
                    <h5 class="text-white">
                        القسم الفرعي {{optional($product->category->parent)->parent ?
                                                    optional($product->category->parent) ?
                                                        optional($product->category->parent)->name
                                                    : $product->category->name
                                       : $product->category->name}}
                    </h5>
                    @if( optional($product->category->parent)->parent  && optional($product->category->parent)  )
                        <h5 class="text-white">
                            القسم الفرعي فرعي :{{$product->category->name  }}
                        </h5>
                    @endif
                    <p class="card-text pt-3">{{$product->description}} </p>
                    <h1 class="font-weight-bold default-color">{{$product->price}} ريال</h1>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('scripts')

        <script src="{{asset('Site/js/slick.min.js')}}"></script>
        <script src="{{asset('Site/js/owl.carousel.min.js')}}"></script>
        <script>
            $(document).ready(function() {
                // ======================== productImages==========================

                $('#productImages').owlCarousel({
                    loop: true,
                    items: 1,
                    center: false,
                    margin: 20,
                    nav: false,
                    dots: false,
                    rtl: true,
                    autoplay: false,
                    autoplayTimeout: 1000,
                    autoplayHoverPause: true,

                })
            });
        </script>
@endsection
