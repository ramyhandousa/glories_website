@extends('layouts.master')
@section('title', 'إدارة العملاء ')
@section('styles')
    <style>

        .hidden-category{
            visibility: hidden !important;
        }

    </style>
@endsection
@section('content')
<body class="bg-orders text-center">

    <!-- content -->
    <div class="container text-right my-5 text-white">
        <div class="row">
            <form  id="dynamicFormSubmit"  enctype="multipart/form-data"  method="post" action="{{ route('products.update', $product->id) }}  " class="product-info">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <input type="hidden" name="productId" value="{{$product->id}}">
                <div class="row">
                    <div class="col-xl-6 mb-5">
                        <h6 class="pb-3">صور المنتج (بامكانك اضافة    صور)</h6>
                        <?php
                        $images = $product->images_path ? count($product->images_path) : 0;

                        ?>
                        @if($images)
                            @foreach($product->images_path as $key => $image)
                                <div class="wrap-custom-file">
                                    <input type="file" class="choose_image" name="images[]" id="image{{$key}}"  accept=" .jpg, .png" />
                                    <label for="image{{$key}}" id="image{{$key}}">
                                        <img src=" {{asset($image)}}"
                                             id="add{{$key}}">
                                    </label>

                                </div>

                            @endforeach
                        @endif

                        @for ($i = 0; $i < (5 - $images) ; $i++)
                            <div class="wrap-custom-file">
                                <input type="file" name="images[]" class="choose_image"  id="image_new{{$i}}" accept=".jpg, .png" />
                                <label for="image_new{{$i}}" id="image_new{{$i}}">
                                    <img src="{{ asset('Site/images/icons/prf_add_photo.png')  }}"
                                         id="add_new{{$i}}">
                                </label>

                            </div>
                        @endfor
                    </div>
                    <div class="col-xl-6 mb-5">
                        <h6 class="pb-3">معلومات المنتج</h6>

                        <?php
                        // Check if Found category id of product == table category  id
                        $foundMainCategory =  $categories->contains($product->category_id );

                        // Check if Found category id of product == table category  id children
                        $foundSubCategory =  $subCategories->contains($product->category_id );

                        // Check if Found category id of product == table category  id children of children
                        $foundSubSubCategory =  $subSubCategories->contains($product->category_id );
                        ?>
                        <div class="form-group">
                            <input type="text" name="product_name" class="form-control" value="{{optional($product->translate('ar'))->name}}" placeholder="اسم المنتج / الخدمة">
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <select name="category" class="form-control  category">
                                    <option value="" disabled selected hidden class="text-white">القسم الرئيسي</option>
                                    @foreach($categories  as $value)
                                        <option  value="{{ $value->id }}"

                                         @if($value->id == $product->category_id) selected   @endif

                                        @if(optional($product->category->parent)->parent)
                                            @if($value->id == $product->category->parent->parent->id)selected   @endif
                                        @endif
                                        >
                                            {{ $value->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group sub-section ">

                            <select name="category"  class="form-control hideDiv City_chid">
                                {{--<option value="" disabled selected hidden class="text-white">القسم الفرعي</option>--}}
                                @if ($foundSubCategory == 1)

                                    <option  value="{{   $product->category->id }}">   {{   $product->category->name }}   </option>

                                @endif

                                @if($product->category->parent)
                                    <option  value="{{ $product->category->parent->id }}" @if($product->category_id == $product->category->parent->id) selected   @endif>

                                        {{ $product->category->parent->name }}
                                    </option>
                                @endif
                            </select>

                        </div>
                        <div class="form-group sub-section float-left">

                            <select name="category"  class="form-control  hideDiv Child_City_chid">
                                {{--<option value="" disabled selected hidden class="text-white">  القسم الفرعي الفرعي</option>--}}
                                @if($foundSubSubCategory == 1)

                                    <option  value="{{   $product->category->id }}">   {{   $product->category->name }}   </option>
                                @endif
                            </select>

                        </div>

                        <div class="form-group">
                            <input type="text" name="description" class="form-control"
                                    value="{{optional($product->translate('ar'))->description}}" placeholder="الوصف">
                        </div>
                        <div class="form-group">
                            <input type="text" name="price" class="form-control" value="{{$product->price}}" placeholder="السعر">
                            <span class="currency">ريال سعودي</span>
                        </div>

                    </div>
                    <div class="col-xl-12 text-center my-5 add-pro-btn">
                        <button id="hiddenButton" type="submit" class="btn default-bg br-50 text-dark mx-2 my-2">حفظ التعديلات</button>
                        <a href="javascript:;"  id="elementRow{{ $product->id }}" data-id="{{ $product->id }}"
                            class="btn provider-btn br-50 mx-2 my-2 removeElement">حذف المنتج</a>
                    </div>
                </div>

            </form>
        </div>

    </div>
@endsection

    @section('scripts')
        <script >


         function myFunction(productId , id, image) {
                var form_data = new FormData();

                form_data.append("image", image.files[0]);
                form_data.append("productId", productId);
                form_data.append("imageId", id);
                if(image){
                    $.ajax({
                    url: '{{ route('uploadImage_product') }}',
                    beforeSend:function (){
                        $('.hiddenLoading').show();
                        $('.hiddenContent').hide();
                    },
                    method:"POST",
                    data: form_data,
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (data) {

                        messageSuccessOrError(data)
                    },
                    error: function (data) {
                    },
                    complete:function (){
                        $('.hiddenLoading').hide();
                        $('.hiddenContent').show();
                    }
                });
                }
            }

            $(document).ready(function() {

                $(document).on('change', '.category', function () {
                    $('.Child_City_chid').text(" ");
                    var myButton = document.getElementById("hiddenButton");
                    var City_id =   $(this).val();
                    var div =   $(this).parent().parent().parent();
                    var op =" ";
                    var showElement =  div.find('.showElement');
                    $.ajax({
                        type:"Get",
                        url: "{{route('getSub')}}",
                        data: {'id': City_id},
                        success:function (data) {

                            if(data == 401){

                                myButton.classList.remove("hidden-category");
//                                div.find('.City_chid').html(" ");
                                $('.hideDiv').parent().hide();
//                                $(".hideDiv").css("display", "none");
                            }else {
                                $('.hideDiv').parent().show();
//                                $(".hideDiv").css("display", "block");
                                showValidation('إختار قسم فرعي من فضلك .' )
                                myButton.classList.add("hidden-category");
                                op += '<option  disabled selected>  إختر القسم الفرعي</option>';
                                for (var i = 0; i < data.length; i++) {
                                    op += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                                }
                                div.find('.City_chid').html(" ");
                                div.find('.City_chid').append(op);
                                if (data == null || data == undefined || data.length == 0) {
                                    showElement.delay(500).slideUp();
                                } else {
                                    showElement.delay(500).slideDown();

                                }

                            }

                        },
                        error:function (error) {

                        }
                    })
                });

                $(document).on('change', '.City_chid', function () {
                    $('.Child_City_chid').text(" ");
                    var myButton = document.getElementById("hiddenButton");
                    var City_id =   $(this).val();
                    var div =   $(this).parent().parent().parent();
                    var op =" ";
                    var showElement =  div.find('.showElement');
                    $.ajax({
                        type:"Get",
                        url: "{{route('getSub')}}",
                        data: {'id': City_id},
                        success:function (data) {
                            if(data == 401){

                                myButton.classList.remove("hidden-category");

                            }else {
                                showValidation('إختار قسم فرعي فرعي من فضلك')
                                op += '<option  disabled selected>  إختر الفرعي الفرعي   </option>';
                                for (var i = 0; i < data.length; i++) {
                                    op += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
                                }
                                $('.Child_City_chid').html(" ");

                                $('.Child_City_chid').append(op);

                            }
                        },
                        error:function (error) {
                            console.log(error)
                        }
                    })
                });

                $(document).on('click', '.Child_City_chid', function () {

                    var Child_City_chid =   $(this).val();
                    var myButton = document.getElementById("hiddenButton");
                    if (Child_City_chid != null || Child_City_chid != undefined ){

                        // $("#hiddenButton").show();
                        myButton.classList.remove("hidden-category");
                    }

                });


                $('body').on('click', '.removeElement', function () {
                    var id = $(this).attr('data-id');
                    var $tr = $(this).closest($('#elementRow' + id).parent().parent().parent());
                    console.log($tr);
                    swal({
                        title: "هل انت متأكد؟",
                        text: "يمكنك استرجاع المحذوفات مرة اخرى لا تقلق.",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'موافق',
                        cancelButtonText: 'إلغاء',
                        reverseButtons: true
                    }).then(function(result){
                        if (result.value) {
                            $.ajax({
                                type: 'POST',
                                url: '{{route('delete_product')}}',
                                data: {id: id},
                                dataType: 'json',
                                success: function (data) {
                                    if (data.status == true) {
                                        console.log('yes');
                                        swal(  'لقد تمت عملية الحذف بنجاح.'  )
                                        setTimeout(function () {
                                            window.location.href = "{{route('products.index')}}";
                                        }, 1000);

                                    }
                                }
                            });

                        }else if(result.dismiss === 'cancel'){
                            swal(     'تم الإلغاء '  )
                        }
                    });

                });


            });
        </script>

        <script src="{{asset('website_scripts/imagesVaildation.js')}}" defer></script>
        <script src="{{asset('website_scripts/dynamicFormSubmit.js')}}" defer></script>
@endsection
