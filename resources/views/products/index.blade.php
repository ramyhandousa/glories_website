
@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')
<body class="bg-orders text-center">

    <!-- content -->
    <div class="container text-right my-5 text-white">
        <div class="row">
            <div class="col-xl-4">
                <h6>معلومات الاشتراك </h6>

                <div class="order default-color text-center">
                    <h6>رصيد منتجاتك</h6>
                    <div class="row ">
                        <div class="col-xl-6 col-6 bl-before">
                            @if(isset($countproductHave))
                                <h1>{{$countproductHave}}</h1>
                            @endif
                            <span class="text-white">منتجات معروضة</span>
                        </div>
                        <div class="col-xl-6 col-6">
                            @if(isset($countproductRemind))
                                <h1>{{$countproductRemind}}</h1>
                            @endif
                            <span class="text-white">منتجات متبقية</span>
                        </div>
                    </div>
                    <p class="text-white">
                        لتتمكن من عرض منتجات وخدمات اكثر بامكانك تحديث اشتراكك
                    </p>
                    <a href="" class="default-color"> عرض الاشتراكات </a>
                </div>

            </div>
            <div class="col-xl-4">
                @if(count($products) > 0)
                    <h6>منتجاتي</h6>

                    <ul>
                        @foreach($products as $product)
                            <li>
                                <form method="GET" action="{{ route('products.show',$product->id) }}"  id="changeProduct{{ $product->id }}" >

                                    <a href="#" onclick="return $('#changeProduct{{ $product->id }}').submit()">
                                        <input type="hidden" name="productId" value="{{$product->id}}">
                                        <div class="card ">
                                            @if($product->images_path)
                                            <img class="card-img" src="{{$product->images_path[0] }}" alt="">
                                            @endif
                                            <div class="card-body">
                                                <h5 class="card-title default-color"> {{$product->name}}    </h5>
                                                <p class="card-text">
                                                    {{$product->description}}
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </form>
                            </li>
                        @endforeach
                    </ul>
                <div class="proPagination">

                    {{ $products->links('vendor.pagination.simple-bootstrap-4') }}
                </div>
                @else
                    <div>لا تملك منتجات </div>
               @endif

            </div>
            <div class="col-xl-4">
                {{--<h6>المنتجات المتبقية</h6>--}}
                <br>
                <ul>
                    <li>
                        <a  href="{{route('products.create')}}" target="_blank">
                            <div class="card ">
                                <div class="card-plus">
                                    <i class="fas fa-plus"></i>
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title default-color mt-4">اضافة منتج جديد</h5>
                                </div>
                            </div>
                        </a>
                    </li>

                </ul>
            </div>
        </div>

    </div>

@endsection

