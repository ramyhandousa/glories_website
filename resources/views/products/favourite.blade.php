@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')
<body class="bg-provider text-center">

    <div class="container text-right  my-5 text-white ">
        <div>
            @if(count($products) > 0)
            <ul class="row p-0 provider-services">
                @foreach($products as $product)
                    <li class="col-xl-6 ">
{{--                    <a href="{{route('orders.create')}}?productId={{$product->id}}&userId={{$product->user_id}}">--}}
                    <a href="{{route('details-product',$product->id)}}">
                        <input type="hidden" value="{{$product->id}}" name="productId">
                        <div class="card ">
                            <div class="card-body p-0 card-body-img">
                                <img class="card-img" src="{{$product->images_path ? $product->images_path[0] : asset('category.jpg') }}" alt="">
                            </div>
                            <div class="card-body">
                                <div class="card-title default-color">
                                    <span>{{$product->name}}</span> - <span>{{$product->user->name}}</span>
                                </div>
                                <p class="card-text">
                                    {{$product->description}}
                                </p>
                            </div>
                            <div class="card-body dir">
                                <i class="fas fa-chevron-left"></i>
                            </div>
                        </div>
                    </a>
                </li>
                @endforeach
            </ul>
            @else
                <img src="{{asset('empty_data.png')}}" alt="">
            @endif
        </div>

    </div>

@endsection
