@extends('layouts.master')
@section('title', 'إدارة العملاء ')

@section('content')
    <body class="bg-orders text-center">

    <div class="container text-right text-white  my-5 subs">
        <div class="row">
            <div class="col-xl-4 mb-5">
                <div class="row mx-auto">
                    <p>
                        لتتمكن من اضافة منتجات وخدمات وعروض يجب عليك الاشتراك باحدى اشتراكات التطبيق
                    </p>

                    <ul class="col-xl-12 btn-group btn-group-toggle d-block my-3" data-toggle="buttons">

                        @foreach($memberShips as $member)
                            <li class="btn btn-secondary waves-effect waves-light sub ">

                                <div class="subscripe text-center mb-2" >
                                    <input type="radio" name="check"    autocomplete="off" value="{{$member->id}}">
                                    <h4 class="default-color">{{$member->name}}</h4>
                                    <p class="px-5"> يؤهلك لاضافة   {{$member->number_product}} منتج <br>/ يؤهلك لاضافة {{$member->number_services_enjoy}} عروض
                                        <b class="d-block">قيمة الاشتراك الشهريه {{$member->price}} ريال</b>
                                    </p>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>
            <div class="col-xl-4 text-white worked-cat mb-5">
                <p>
                    للاشتراك في الاشتراك الذهبي يرجى تحويل قيمة الاشتراك الشهرية الى احدى الحسابات البنكية التالية ثم
                    تاكيد التحويل
                </p>
                <div class="text-center my-5 new-offer">
                    <h1 class="default-color mt-5 offer-price">مبلغ</h1>
                    <span class="default-color">ريال سعودي</span>
                </div>
                <div class="accounts">
                    <ul>
                        @foreach($banks as $bank)
                            <li>
                                <h5 class="d-inline-block w-50 default-color"> {{$bank->name}}</h5>
                                <div class="float-left">
                                    <span>رقم الحساب</span>
                                    <br>
                                    <b>{{$bank->acount_number}}</b>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-xl-4 mb-5">
                <p>
                    املأ بيانات استمارة تأكيد التحويل أدناه
                </p>

                <form method="POST" id="dynamicFormSubmit" action="{{route('member_ship.store')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" name="member_ship_id" id="memberShip">
                    <div class="form-group">
                        <input type="text" name="userName" class="form-control" placeholder="اسم المحول" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="bank_name" class="form-control" placeholder="البنك المحول منه" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="userAccount" class="form-control" placeholder="رقم حساب المحول" required>
                    </div>
                    <div class="form-group">
                        <input type="text" name="iban" class="form-control" placeholder="رقم ابيان التحويل" required>
                    </div>
                    <div class="form-group transaction-amount">
                        <input type="text" name="money" class="form-control" placeholder=" قيمة التحويل" required>
                        <span class="currency">ريال سعودي</span>
                    </div>
                    <div class="form-group text-center">

                        <div class="wrap-custom-file">
                            <input type="file" name="image" class="choose_image" id="image1" accept=".image/jpeg,.jpg, .png" />
                            <label for="image1" id="image"  >
                                <img src="{{ asset('Site/images/icons/prf_add_photo.png')  }}" id="add">
                                <b id="add"> صورة ايبان التحويل</b>
                            </label>

                        </div>
                    </div>
                    <div class="form-group text-center">
                        <button id="myButton" type="submit" class="btn default-bg br-50 px-5">اشتراك</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @endsection

    @section('scripts')

        <script>

            $(document).on('change', '.subscripe', function () {

                var ValueOfMemberChoose = $("input[name='check']:checked"). val();

                $("#memberShip").val(ValueOfMemberChoose);

            });

            $( "#myButton" ).click(function() {

                var value = $('#memberShip').val();

                if($.trim(value) === '' || value == null){

                    showValidation('{{ session()->get('errors',' يجب  إختيار عضوية من فضلك  ') }}');

                    return false;
                }

            });

        </script>

        <script src="{{asset('website_scripts/imagesVaildation.js')}}" defer></script>
        <script src="{{asset('website_scripts/dynamicFormSubmit.js')}}" defer></script>
@endsection
