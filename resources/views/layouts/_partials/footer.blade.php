<footer>
    <div class="download-app text-center my-5">
        <a href="">
            <img src="{{ asset('Site/images/icons/dl_app_store.png')  }}" class="img-fluid">
        </a>
        <a href="">
            <img src="{{ asset('Site/images/icons/dl_google_play.png')  }}" class="img-fluid">
        </a>
    </div>

</footer>
