<link href="https://fonts.googleapis.com/css?family=Tajawal" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
      integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<!-- Bootstrap -->
<link rel="stylesheet" href="{{asset('Site/css/tinytoggle.min.css')}}">
<link rel="stylesheet" href="{{asset('Site/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('Site/css/bootstrap-rtl.min.css')}}">
<link rel="stylesheet" href="{{asset('Site/css/style.css')}}">
<link rel="stylesheet" href="{{asset('Site/css/edit.css')}}?data={{\Carbon\Carbon::now()}}">
<link rel="stylesheet" href="{{asset('Site/css/loading.css')}}">
<link rel="stylesheet" href="{{asset('Site/css/toastr.css')}}" type="text/css" />
<link href="{{asset('Site/css/parsley.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('Site/css/sweetalert2.min.rtl.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('Site/css/fontawesome/all.min.css')}}" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

