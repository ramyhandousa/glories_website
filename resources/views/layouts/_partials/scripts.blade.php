<!--begin:: Global Mandatory Vendors -->
<script src="{{ asset('Site/js/jquery-3.2.1.min.js')  }}" ></script>
<script src="{{ asset('Site/js/bootstrap.min.js')  }}"></script>
<script src="{{ asset('Site/js/scripts.js')  }}"></script>
<script src="{{ asset('Site/js/toastr.min.js')  }}" type="text/javascript"></script>
<script type="text/javascript"  src="{{ asset('Site/js/parsley.min.js')  }}"></script>
<script src="{{ asset('Site/js/validate-ar.js')  }}" type="text/javascript" defer></script>
<script src="{{ asset('Site/js/sweetalert2.min.js')  }}" type="text/javascript"></script>
<script src="{{ asset('Site/js/jquery.tinytoggle.min.js')  }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>


{{--<script type="text/javascript" src="{{ asset('')  }}/public/Site/js/bootstrap-material-datetimepicker.js"></script>--}}
