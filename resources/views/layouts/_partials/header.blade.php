<header class="text-left">
    @if(auth()->check() )
    <div id="mySidenav" class="sidenav">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <ul>

            @if(Auth::user()->is_user == 1) {{-- this mean provider  --}}

                <li>
                    <a class="myLoader" href="{{route('orders.index')}}?type=outcome">الطلبات الواردة</a>

                </li>

                <li>
                    <a class="myLoader" href="{{route('products.index')}}">منتجاتي</a>

                </li>

                <li>
                    <a class="myLoader" href="{{route('offers.index')}}">عروضي</a>
                </li>

                <li>
                    <a class="myLoader" href="{{route('member_ship.index')}}">الاشتراكات</a>
                </li>

                <li>
                    <a class="myLoader"  href="{{route('site.categories')}}">اقسامي</a>
                </li>

                <li>
                    <a class="myLoader" href="{{route('my-profile-provider')}}">الملف الشخصي</a>

                </li>

            @else
                <li>
                    <a class="myLoader" href="{{route('my-profile-user')}}">الملف الشخصي</a>
                </li>

            @endif

            <li>
                <a class="myLoader" href="{{route('list_notifications')}}">التنبيهات</a>

            </li>

            <li>
                <a class="myLoader" href="{{route('site.allCategories')}}">الاقسام</a>

            </li>

            <li>
                <a class="myLoader" href="{{route('orders.index')}}?type=income">الطلبات الصادرة</a>

            </li>

            <li>
                <a class="myLoader" href="{{route('orders.index')}}?type=waiting">الطلبات الجديدة</a>
            </li>

            <li>
                <a class="myLoader" href="{{route('site.listFavourite')}}">  المفضلة </a>
            </li>

            <li>
                <a class="myLoader" href="{{route('site.search')}}">  البحث </a>
            </li>

            <li>
                <a class="myLoader" href="{{route('site.contactUs')}}">اتصل بنا</a>
            </li>

            <li>
                <a class="myLoader"  href="{{route('site.terms')}}">شروط الاستخدام</a>

            </li>
            <li>
                <a class="myLoader" href="{{route('site.aboutUs')}}"> من نحن </a>

            </li>
            <li>
                <a  href="{{ route('site.AuthLogOut') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">خروج</a>
            </li>
            <form id="logout-form" action="{{ route('site.AuthLogOut') }}" method="POST"
                   style="display: none;">
                {{ csrf_field() }}
            </form>
        </ul>
    </div>
    <span onclick="openNav()" class="mx-5 open-btn">&#9776; </span>
    @endif
    @if(\Illuminate\Support\Facades\Auth::check())
        <!-- back btn -->
        {{--<a href="javascript:window.history.back(); " class="back"><i class="fas fa-chevron-right"></i> رجوع </a>--}}
        <a href="javascript:;" onclick="goBack()" class="back"><i class="fas fa-chevron-right"></i> رجوع </a>
    @endif
</header>
