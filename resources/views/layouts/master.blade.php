<!DOCTYPE html>
<html lang="ar">

<head>
    <!-- Meta-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('Site/logo.ico')  }}" sizes="16x16">
    <!-- Title-->
    <title>AR @yield('title')</title>
    @include('layouts._partials.styles')
    @yield('styles')
    <style>
        .hiddenLoading{
            display: none;
        }
        .hiddenContent{

        }
    </style>

    @if(auth()->check())
        <script>
            var userId = '{{ auth()->id() }}';
            var url = '{{ route('user.update.token') }}';
            var lang = '{{ config('app.locale') }}';
        </script>
    @endif

</head>
<!-- begin::Body -->
<body style="background-size: cover !important;">

    @include('layouts._partials.header')

    <div class="hiddenContent">
        @yield('content')
    </div>

 <div class="frame hiddenLoading">

        <div class="center">
            <div class='shape shape-1'></div>
            <div class='shape shape-2'></div>
            <div class='shape shape-3'></div>
            <div class='shape shape-4'></div>
            <div class='shape shape-5'></div>
            <div class='shape shape-6'></div>
            <div class='shape shape-7'></div>
            <div class='shape shape-8'></div>
            <div class='shape shape-9'></div>
            <div class='shape shape-10'></div>
        </div>

    </div>

@include('layouts._partials.footer')

@include('layouts._partials.scripts')


{{--<script src="https://www.gstatic.com/firebasejs/5.8.5/firebase.js"></script>--}}
<!--<script src="https://www.gstatic.com/firebasejs/5.8.5/firebase.js"></script>-->

{{--<script src="{{ asset('FCM-Setup.js') }}"></script>--}}

<script>


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    @if(session()->has('success'))
        setTimeout(function () {
        showMessage('{{ session()->get('success') }}');
    }, 1000);

    @endif

    function showMessage(message) {

        var shortCutFunction = 'success';
        var msg = message;
        var title = 'نجاح!';
        toastr.options = {
            positionClass: 'toast-top-left',
            onclick: null,
            showMethod: 'slideDown',
            hideMethod: "slideUp",
        };
        var $toast = toastr[shortCutFunction](msg, title);
        // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;


    }


    @if(session()->has('warning'))
        setTimeout(function () {
        showWarning('{{ session()->get('warning') }}');
    }, 1000);

    @endif

    function showWarning(message) {

        var shortCutFunction = 'warning';
        var msg = message;
        var title = 'تحذير!';
        toastr.options = {
            positionClass: 'toast-top-left',
            onclick: null,
            showMethod: 'slideDown',
            hideMethod: "slideUp",
            preventDuplicates: true,
            preventOpenDuplicates: true
        };
        var $toast = toastr[shortCutFunction](msg, title);
        // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;


    }

    @if(session()->has('my_errors'))
        setTimeout(function () {
        showErrors('{{ session()->get('my_errors') }}');
    }, 1000);

    @endif

    function showErrors(message) {

        var shortCutFunction = 'error';
        var msg = message;
        var title = 'فشل!';
        toastr.options = {
            positionClass: 'toast-top-left',
            onclick: null,
            showMethod: 'slideDown',
            hideMethod: "slideUp",
            preventDuplicates: true,
            preventOpenDuplicates: true
        };
        $toastlast = toastr[shortCutFunction](msg, title);
    }

    @if(session()->has('validation'))
        setTimeout(function () {
        showValidation('{{ session()->get('validation') }}');
    }, 1000);

    @endif

    function showValidation(message) {

        var shortCutFunction = 'warning';
        var msg = message;
        var title = 'مراحعة البيانات!';
        toastr.options = {
            positionClass: 'toast-top-center',
            onclick: null,
            showMethod: 'slideDown',
            hideMethod: "slideUp",
            preventDuplicates: true,
            preventOpenDuplicates: true
        };
        var $toast = toastr[shortCutFunction](msg, title);
        // Wire up an event handler to a button in the toast, if it exists
        $toastlast = $toast;

    }

$(document).ready(function(){
  $("#myformLoading").on("submit", function(){
        $('.hiddenLoading').show();
        $('.hiddenContent').hide();
  });//submit


  $(".myLoader").on("click", function(){
        $('.hiddenLoading').show();
        $('.hiddenContent').hide();
  });
});


</script>

@yield('scripts')


</body>

<!-- end::Body -->
</html>
