<?php

return [

    "orders"=> "الطلبات",
    "ongoing_orders" => "الطلبات الجارية",
    "completed_orders" => "الطلبات المنتهية",
    "orders_on_way" => "الطلبات قيد التوصيل",

    "order_accepted_by_delivery"=> " تم قبول طلبك  :order_id  من المندوب :user_name",
    "order_refuse_by_delivery"=> "  تم رفض طلبك  :order_id  من المندوب :user_name بسبب :message",
    "order_completed_by_delivery"=> " تم إنتهاء طلبك  :order_id  من المندوب :user_name",

    "order_refuse"=> " تم رفض طلبك  :order_name بسبب :message_ar",
    "order_updated_by_admin"=> "تم تعديل وقبول طلبك  :order_name",

    'promo_code_not_exists' => "هذا الكود غير موجود لدينا",
    'promo_code_used' => "تم إستخدام الكود مسبقا ",
    'promo_code_finished' => "تم إنتهاء صلاحية الكود للإستخدام ",

    "rating" => "التقيمات",
    "order_rate_by_user" => " تم تقييم عرضك علي الطلب :order_name استاذ :user_name",
    "order_rate_by_offer_man" => "تم تقييم طلبك :order_name  من مقدم العرض",
    'rate_order_user' => "شكرا لك  يا :user_name  برجاء تقيم الطلب :order_name ",
    'rate_order_offer_man' => "شكرا لك  :user_name  برجاء تقيم الطلب :order_name ",
    'order_rating' => 'تم تقييم الطلب بنجاح',

    'contact_us' => "تواصل معنا" ,
//    "contact_us_body" => "  تم الرد علي رسالتك من قبل الإدارة \n "
    "contact_us_body" => "تم الرد علي رسالتك من قبل الإدارة\n الرد: \n :reply \n الرسالة الأصلية: \n :message ",
    "offer_refused" => "تم رفض العرض المقدم علي الطلب  :order_name  ",
    "complete_order_user" => "تم إكتمال الطلب  :order_name",
    "complete_order_offer" => "تم إكتمال الطلب  :order_name",
];
