<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\HomeController;
use App\Http\Controllers\website\Auth\SignUpController;
use App\Http\Controllers\website\Auth\ResetPasswordController;
use App\Http\Controllers\website\Auth\loginController;
use App\Http\Controllers\website\Auth\ForgetPasswordController;
use App\Http\Controllers\website\DeviceController;
use App\Http\Controllers\website\CategoryController;
use App\Http\Controllers\website\SettingController;
use App\Http\Controllers\website\ProductController as WebsiteProductController;
use App\Http\Controllers\website\OfferController as WebsiteOfferController;
use App\Http\Controllers\website\UserController as WebsiteUserController;
use App\Http\Controllers\website\MemberShipController as WebsiteMemberShipController;
use App\Http\Controllers\website\OrderController as WebsiteOrderController;
use App\Http\Controllers\website\SearchController as WebsiteSearchController;



Route::get('/', [HomeController::class ,'index'])->middleware('loggin');
Route::get('/intro', [HomeController::class ,'intro'])->name('site.intro')->middleware('loggin');
Route::get('/login',function (){
    return 'login page';
})->name('login');
Route::group([
    'namespace' => 'website',
], function () {

    Route::group(['prefix' => 'Auth','namespace' => 'Auth'], function () {

        Route::get('/user/signup', [SignUpController::class ,'user_signup_page'])->name('user_signup_page');
        Route::get('/provider/signup', [SignUpController::class ,'provider_signup_page'])->name('provider_signup_page');
        Route::post('/contain_signUp', [SignUpController::class ,'contactFinancial'])->name('contain.signUp');
        Route::post('/register_user', [SignUpController::class ,'create_user'])->name('sign_up_user');
        Route::post('/register_provider', [SignUpController::class ,'create_provider'])->name('sign_up_provider');
        Route::get('/activation_code',[ResetPasswordController::class,'activationCode'])->name('activationCode');
        Route::post('/activation_code',[ResetPasswordController::class,'checkCode'])->name('checkCode');
        Route::post('/resend_code',[ResetPasswordController::class,'resendCode'])->name('resendCode');
        Route::get('/login',[loginController::class,'loginIndex'])->name('site.login');
        Route::post('/login',[loginController::class,'login'])->name('site.AuthLogin');
        Route::post('/log_out',[loginController::class,'logOut'])->name('site.AuthLogOut');
        Route::get('/forget_password',[ForgetPasswordController::class,'index'])->name('site.forgetPassword');
        Route::post('/change_password',[ForgetPasswordController::class,'changePassword'])->name('site.changePassword');
        Route::get('/edit_password',[ForgetPasswordController::class,'getEditPassword'])->name('site.getEditPassword');
        Route::post('/edit_password',[ForgetPasswordController::class,'editPassword'])->name('site.editPassword');
    });

    Route::group(['middleware' => 'AuthWebSite'], function () {
        Route::get('my_profile',[WebsiteUserController::class,'my_profile'])->name('my-profile-user');
        Route::get('provider_profile',[WebsiteUserController::class,'provider_profile'])->name('my-profile-provider');
        Route::post('update-profile/{user}',[WebsiteUserController::class,'update_profile'])->name('update_profile_user');
        Route::post('update-profile-provider/{user}',[WebsiteUserController::class,'update_profile_provider'])->name('update_profile_provider');

        Route::get('/notifications',[WebsiteUserController::class,'notifications'])->name('list_notifications');
    });

    Route::get('/search',[WebsiteSearchController::class,'index'])->name('site.search');
    Route::get('/search_items',[WebsiteSearchController::class,'show'])->name('site.searchItems');
    Route::get('/get_user',[WebsiteUserController::class,'getUserById'])->name('site.getUserById');

    Route::get('/categories',[CategoryController::class,'index'])->name('site.allCategories');
    Route::get('/category/{id}/details',[CategoryController::class,'show'])->name('site.sub.category');
    Route::get('/my_categories',[CategoryController::class,'my_categories'])->name('site.categories');
    Route::post('/store_categories',[CategoryController::class,'store'])->name('site.storeCategories');

    Route::group(['prefix' => 'setting'], function () {
        Route::get('/about_us',[SettingController::class,'aboutUs'])->name('site.aboutUs');
        Route::get('/terms',[SettingController::class,'terms'])->name('site.terms');
        Route::get('/contact_us',[SettingController::class,'contactUs'])->name('site.contactUs');
        Route::post('/contact_us',[SettingController::class,'sendContactUs'])->name('site.sendContactUs');
    });
});

Route::resource('products',WebsiteProductController::class);
Route::get('details-product/{product}',[WebsiteProductController::class,'details'])->name('details-product');
Route::post('products/uploadImage',[WebsiteProductController::class,'uploadImage'])->name('uploadImage_product');
Route::post('products/delete',[WebsiteProductController::class,'delete_product'])->name('delete_product');
Route::post('makeFavourite',[WebsiteProductController::class,'makeFavourite'])->name('site.makeFavourite')->middleware(['authUser']);
Route::get('list_favourite',[WebsiteProductController::class,'listFavourite'])->name('site.listFavourite');

Route::resource('offers',WebsiteOfferController::class);

Route::resource('member_ship',WebsiteMemberShipController::class);
Route::get('my_member_ship',[WebsiteMemberShipController::class,'my_member_ship'])->name('my_member_ship');

Route::resource('orders',WebsiteOrderController::class);
Route::post('orders/accepted_refuse/{order}',[WebsiteOrderController::class,'accepted_refuse'])->name('accepted_refuse_order');
Route::post('orders/finish/{order}',[WebsiteOrderController::class,'finish_order'])->name('finish_order');


Route::get('/sub_categories', [DeviceController::class,'sub_categories'])->name('getSub');
Route::post('check/column/exist',[DeviceController::class,'check_column'])->name('check.column.exist');
Route::get('/getNumberImage',[DeviceController::class,'getNumberImage'])->name('countImage');
Route::post('user/update/token',[DeviceController::class,'update_device'])->name('user.update.token');


Route::get("update_product_images",function (){

    $product = \App\Models\Product::find(12);

    $product->update(['images' => null,'images_path' => null]);

    $data = \App\Models\uploadImages::all();
    $data->each->delete();

    array_map('unlink', glob("files/products/*.*"));
});


Route::get('provider_users_now',function (){

    $users = \App\Models\User::where('is_user',1)->get();

    $users->each->update([
        'is_active' => 1,
        'payment' => 1,
        'accepted' => 1,
        'is_suspend' => 0,
    ]);
});
