<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController ;
use App\Http\Controllers\Api\UserController ;
use App\Http\Controllers\Api\MemberShipController ;
use App\Http\Controllers\Api\CategoryController ;
use App\Http\Controllers\Api\SearchController ;
use App\Http\Controllers\Api\ProductController ;
use App\Http\Controllers\Api\OfferController ;
use App\Http\Controllers\Api\SettingController ;
use App\Http\Controllers\Api\OrderController ;
use App\Http\Controllers\Api\PaymentController ;

Route::group([
    'prefix' => 'v1',
], function () {

    Route::prefix('Auth')->group(function ($router) {
        $router->post('login', [AuthController::class,'login']);
        $router->post('register', [AuthController::class,'register']);
        $router->post('forgetPassword', [AuthController::class,'forgetPassword']);
        $router->post('resetPassword', [AuthController::class,'resetPassword']);
        $router->post('checkCode', [AuthController::class,'checkCodeActivation']);
        $router->post('checkPhoneOrEmail', [AuthController::class,'checkPhoneOrEmail']);
        $router->post('resendCode', [AuthController::class,'resendCode']);
        $router->post('changPassword', [AuthController::class,'changPassword']);
        $router->post('changePhone',[AuthController::class,'changePhone']);
        $router->post('editProfile',[AuthController::class,'editProfile']);
        $router->post('logOut',[AuthController::class,'logOut']);
    });

    Route::prefix('users')->group(function ($router) {
        $router->get('provider', [UserController::class,'get_provider_by_id']);
        $router->post('rateProvider', [UserController::class,'rateProvider']);
        $router->post('makeFavourite', [UserController::class,'makeOrUnFavourite']);
        $router->get('listFavourite', [UserController::class,'listFavourite']);
        $router->get('listMemberShip', [MemberShipController::class,'index']);
        $router->post('chooseMember', [MemberShipController::class,'chooseMember']);
        $router->post('registerMember', [MemberShipController::class,'registerMember']);
    });

    Route::apiResources([
        'categories'    => CategoryController::class,
        'products'      => ProductController::class,
        'offers'        => OfferController::class,
        'orders'        => OrderController::class,
    ]);

    Route::post('/categories/createOrEditCategory',[CategoryController::class,'createOrEditCategory']);

    Route::get('search',SearchController::class);
    Route::get('Notification/list',[UserController::class,'Notification']);

    Route::prefix('setting')->group(function ($router) {
        $router->get('cities', [SettingController::class,'cities']);
        $router->get('aboutUs', [SettingController::class,'aboutUs']);
        $router->get('terms', [SettingController::class,'terms']);
        $router->get('banks', [SettingController::class,'banks']);
        $router->get('getTypes', [SettingController::class,'getTypes']);
        $router->post('contactUs', [SettingController::class,'contactUs']);
        $router->post('bankTransfer', [SettingController::class,'bankTransfer']);
    });

    Route::prefix('payments')->group(function ($router) {
        $router->get('get/methods', [PaymentController::class,'getPaymentMethods']);
        $router->post('excute', [PaymentController::class,'excutePayment']);
        $router->get('success', [PaymentController::class,'success_payment']);
        $router->get('errors', [PaymentController::class,'errors']);
    });


    Route::post("uploadImages",[UserController::class,'upload_image']);
    Route::post("remove_image",[UserController::class,'remove_image']);

});


