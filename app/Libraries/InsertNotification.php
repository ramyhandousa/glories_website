<?php
namespace App\Libraries;

use App\Libraries\PushNotification;
use App\Models\Device;
use  App\Models\Notification;

class InsertNotification {

     public $push;
    function __construct(PushNotification $push)
    {
        $this->push = $push;
    }

       public  function NotificationDbType($type  ,$user,  $sender = null ,  $request = null  , $order = null ,$additional = null,$product = null){
             $admins= Device::whereDeviceType('web')->pluck('user_id');
              $devicesWeb= Device::whereDeviceType('web')->pluck('device');
            $userIos= Device::where('user_id','!=',$sender)->whereDeviceType('Ios')->pluck('user_id');

            switch($type){

            case $type == 1:

                // contactUs Form admin
                //  $sender  admin who send
                // admin panel
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'title' =>   trans('global.connect_us')  ,
                    'body' => $request ,
                    'type' => 1,
                  ];
                  $this->insertData($data);

             break;

            case $type == 2:

                     // contactUs Form Users
                  foreach ($admins as $admin){
                         $data = [
                           'user_id' => $admin ,
                           'sender_id' => $sender ,
                           'title' => trans('global.connect_us'),
                           'body' => $request ,
                           'type' => 2,
                         ];
                         $this->insertData($data);
                         $this->push->sendPushNotification(null, $devicesWeb, 'تواصل معنا', $request);
                  }
                  break;

            case $type == 3:

            // want To Be Provider
             foreach ($admins as $admin){
              $data = [
                'user_id' => $admin ,
                'sender_id' => $sender->id ,
                'title' => trans('global.order_joining') ,
                'body' =>  trans('global.join_as_service_provider')  . $sender->name ,
                'type' => 3,
              ];
              $this->insertData($data);
              }
             break;

            case $type == 4:
            // products added
            // $user who add product
            // additional  name of product

             foreach ($userIos as $ios){
              $data = [
                'user_id' => $ios ,
                'sender_id' => $sender ,
                'title' => trans('global.products'),
                'body' => trans('global.add_product_name')  . $additional ,
                'type' => 4,
                'product_id' => $product
              ];
              $this->insertData($data);
              }
             break;

            case $type == 5:

            // Bank Transfer For adminPanel
             foreach ($admins as $dmin){
              $data = [
                'user_id' => $dmin ,
                'sender_id' => $sender->id ,
                'title' =>  trans('global.bank_transfers') ,
                'body' =>   $sender->name . trans('global.bank_transfers_from')   ,
                'type' => 5,
              ];
              $this->insertData($data);
              }
             break;

            case $type == 6:

                // Bank Transfer For User
                // accepted
                // $user [ user or provider ]
                //  $sender admin panel
              $data = [
                'user_id' => $user ,
                'sender_id' => $sender ,
                'title' => trans('global.bank_transfers'),
                'body' =>    trans('global.bank_transfers_accepted')   ,
                'type' => 6,
              ];
              $this->insertData($data);

             break;
            case $type == 7:

                // Bank Transfer For User
                // refuse
                // $user [ user or provider ]
                //  $sender admin panel
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender ,
                    'title' => trans('global.bank_transfers'),
                    'body' =>    trans('global.bank_transfers_refuse')  ,
                    'type' => 7,
                  ];
                  $this->insertData($data);

             break;

            case $type == 8:

                // MemberShip Accepted
                // accepted
                // $user [ user or provider ]
                //  $sender admin panel
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender ,
                    'title' =>  trans('global.membership'),
                    'body' =>  trans('global.membership_accepted')   ,
                    'type' => 8,
                  ];
                  $this->insertData($data);

             break;

             case $type == 9:

                 // MemberShip Refuse
                 // refuse
                 // $user [ user or provider ]
                 //  $sender admin panel
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender ,
                    'title' => trans('global.membership'),
                    'body' =>      trans('global.membership_refuse') ,
                    'type' => 9,
                  ];
                  $this->insertData($data);

             break;

             case $type == 10:

                  $data = [
                    'user_id' => $order->provider_id ,
                    'sender_id' => $order->user_id ,
                    'order_id' => $order->id ,
                    'title' => trans('global.orders'),
                    'body' =>  trans('global.you_have_order').$sender->name   ,
                    'type' => 10,
                  ];
                  $this->insertData($data);

             break;

             case $type == 11:


                 // Accepted Order
                 // need user to tell him your order accepted
                 // need sender id
                 // need order_id
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender->id ,
                    'order_id' => $order->id,
                    'title' => trans('global.orders'),
                    'body' =>    trans('global.you_order_accepted_on') . $order->id   ,
                    'type' => 11,
                  ];
                  $this->insertData($data);

             break;

             case $type == 12:

                 // Refuse Order
                 // need user to tell him your order refuse
                 // need sender id
                 // need order_id
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender->id ,
                     'order_id' => $order->id,
                    'title' => trans('global.orders'),
                    'body' =>   trans('global.you_order_refuse_on') . $order->id  ,
                    'type' => 12,
                  ];
                  $this->insertData($data);

             break;

             case $type == 13:

                 // Rating
                 // $user who get rate
                 // $sender who make rate
                  $data = [
                    'user_id' => $user ,
                    'sender_id' => $sender->id ,
                    'order_id' => $order ,
                    'title' => trans('global.rating'),
                    'body' =>      trans('global.you_rate_from') .$sender->name ,
                    'type' => 13,
                  ];
                  $this->insertData($data);

             break;

             case $type == 14:

                 // Offers
                 // $user =>  admins To Tell Them offer Added
                 // $sender who make offer
                  foreach ($admins as $dmin){
                   $data = [
                     'user_id' => $dmin ,
                     'sender_id' => $sender->id ,
                     'title' => trans('global.offers'),
                     'body' =>  trans('global.offer_added_from') .$sender->name  ,
                     'type' => 14,
                   ];
                   $this->insertData($data);
                  }

             break;


                case $type == 15:

                    // Refuse Order
                    // need user to tell him your order refuse
                    // need sender id
                    // need order_id
                    $data = [
                        'user_id' => $user ,
                        'sender_id' => $sender->id ,
                        'order_id' => $order->id,
                        'title' => trans('global.orders'),
                        'body' =>   trans('global.finish_order')   . $order->id  ,
                        'type' => 15,
                    ];
                    $this->insertData($data);

                    break;
            default:

     }


    }



    private function insertData($data)
    {
     if (count($data) > 0) {

        $time = ['created_at' => now(),'updated_at' => now()];
           Notification::insert($data + $time);

     }
    }

}

