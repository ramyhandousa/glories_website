<?php
/**
 * Created by PhpStorm.
 * User: RAMY
 * Date: 2/24/2020
 * Time: 6:40 PM
 */

namespace App\Libraries;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class myfatoorah
{

    public $headerApiToken;
    public $token;

    public $basURL;
    public $initiatePayment;
    public $executePayment;
    public $callBackUrl;
    public $errorUrl;

    public function __construct( )
    {
        $language = request()->headers->get('lang') ? request()->headers->get('lang') : 'ar';
        app()->setLocale($language);

        // api token from header
        $this->headerApiToken = request()->headers->get('apiToken') ?  : ' ';

        $this->token = 'rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL';
//        $this->token = '7Fs7eBv21F5xAocdPvvJ-sCqEyNHq4cygJrQUFvFiWEexBUPs4AkeLQxH4pzsUrY3Rays7GVA6SojFCz2DMLXSJVqk8NG-plK-cZJetwWjgwLPub_9tQQohWLgJ0q2invJ5C5Imt2ket_-JAlBYLLcnqp_WmOfZkBEWuURsBVirpNQecvpedgeCx4VaFae4qWDI_uKRV1829KCBEH84u6LYUxh8W_BYqkzXJYt99OlHTXHegd91PLT-tawBwuIly46nwbAs5Nt7HFOozxkyPp8BW9URlQW1fE4R_40BXzEuVkzK3WAOdpR92IkV94K_rDZCPltGSvWXtqJbnCpUB6iUIn1V-Ki15FAwh_nsfSmt_NQZ3rQuvyQ9B3yLCQ1ZO_MGSYDYVO26dyXbElspKxQwuNRot9hi3FIbXylV3iN40-nCPH4YQzKjo5p_fuaKhvRh7H8oFjRXtPtLQQUIDxk-jMbOp7gXIsdz02DrCfQIihT4evZuWA6YShl6g8fnAqCy8qRBf_eLDnA9w-nBh4Bq53b1kdhnExz0CMyUjQ43UO3uhMkBomJTXbmfAAHP8dZZao6W8a34OktNQmPTbOHXrtxf6DS-oKOu3l79uX_ihbL8ELT40VjIW3MJeZ_-auCPOjpE3Ax4dzUkSDLCljitmzMagH2X8jN8-AYLl46KcfkBV';

//        $this->basURL           = 'https://api.myfatoorah.com';
        $this->basURL           = 'https://apitest.myfatoorah.com';
        $this->initiatePayment  = $this->basURL . '/v2/InitiatePayment';
        $this->executePayment   = $this->basURL . '/v2/ExecutePayment';
        $this->callBackUrl      = request()->root() . '/api/v1/payments/success?orderId=' . \request()->orderId;
        $this->errorUrl         =  request()->root() . '/api/v1/payments/errors?orderId=' . \request()->orderId;

    }


    public function listPaymentMethod(Request $request){

        $user = $request->user();

        $fileds = $this->fields($user,$request,$this->initiatePayment);

        return $this->myfatoorahResponse($this->initiatePayment,$fileds);
    }

    public function excutePaymentMethod(Request $request){

        $user = $request->user();

        $fileds = $this->fields($user,$request,$this->executePayment);

        return $this->myfatoorahResponse($this->executePayment,$fileds);
    }

    public function myfatoorahResponse($curlUrl , $fields ){

         $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $curlUrl,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $fields,
            CURLOPT_HTTPHEADER => array("Authorization: Bearer $this->token", "Content-Type: application/json"),

        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        $results =  json_decode($response, true);

        return $results;
    }

    public function fields($user , $request , $curlUrl){

        if ($curlUrl === $this->executePayment){

            $fields = '{
              "PaymentMethodId": '.$request->paymentMethodId.',
              "orderId": '.$request->orderId.',
              "CustomerName": "'.$user->first_name.'",
              "MobileCountryCode": "+966",
              "CustomerMobile": "'.$user->phone.'",
//              "CustomerEmail": "'.$user->email.'",
              "InvoiceValue": '.$request->total.',
              "CallBackUrl": "'.$this->callBackUrl.'",
              "ErrorUrl": "'.$this->errorUrl.'",
              "Language": "en",
              "CustomerAddress": {
                "Block": "",
                "Street": "",
                "HouseBuildingNo": "",
                "Address": "'.$user->address.'",
                "AddressInstructions": ""
              },

              "SupplierCode": 0,
              "InvoiceItems": [
                {
                  "ItemName": "Service",
                  "Quantity": 1,
                  "UnitPrice": '.$request->total.'
                }
              ]
            }';

        }else{

            $fields = "{\"InvoiceAmount\": $request->total  ,\"CurrencyIso\": \"SAR\"}";

        }

        return $fields;
    }

    public function success(Request $request){

        return response()->json([
            'status' => 200,
            'message'=> "Success Payment.",
            'data' => $request->all()
        ], 200);
    }

    public function errors(Request $request){

        return response()->json([
            'status' => 400,
            'message'=> "Error Payment.",
            'error' =>  (array)$request->all()
        ], 400);
    }

}
