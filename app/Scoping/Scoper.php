<?php
/**
 * Created by PhpStorm.
 * User: RAMY
 * Date: 4/10/2020
 * Time: 8:59 AM
 */

namespace App\Scoping;

use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class  Scoper {

    protected $request;

    function __construct($request)
    {
      $this->request =   $request;
    }

    public function apply(Builder $builder ,array $scopes ){

        foreach ($this->haveScope($scopes) as $key => $scope){

            if (!$scope instanceof  Scope){
                continue;
            }

           $scope->apply($builder, $this->request->get($key));
        }

        return $builder;
    }

    protected function haveScope(array  $scopes){

        return Arr::only( $scopes, array_keys($this->request->all())  );
    }


}
