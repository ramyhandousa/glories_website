<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class CityScope implements Scope
{

    public function apply(Builder $builder, $value)
    {
        return $builder->where('city_id','=',$value);
    }
}
