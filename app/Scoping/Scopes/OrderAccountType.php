<?php

namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class OrderAccountType implements Scope
{

    public function apply(Builder $builder, $value)
    {
        if($value == 'personal'){

            return $builder->where('account_type','=','personal');

        }elseif($value == 'commercial'){

            return $builder->where('account_type','=','commercial');

        }else{
            return $builder->where('account_type','=','commercial');
        }
    }
}
