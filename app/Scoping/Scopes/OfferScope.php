<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class OfferScope implements Scope
{

    public function apply(Builder $builder , $value){

        if ($value == 'offer'){
            return $builder->whereHas('offer_filter');
        }
    }
}
