<?php

namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class OrderType implements Scope
{

    public function apply(Builder $builder, $value)
    {
        if($value == 'auction'){

            return $builder->where('order_type','=','auction');

        }elseif($value == 'tender'){

            return $builder->where('order_type','=','tender');

        }else{
            return $builder->where('order_type','=','tender');
        }
    }
}
