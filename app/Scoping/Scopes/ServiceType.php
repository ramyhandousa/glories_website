<?php

namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class ServiceType implements Scope
{

    public function apply(Builder $builder, $value)
    {
        if($value == 'product'){

            return $builder->where('service_type','=','product');

        }elseif($value == 'service'){

            return $builder->where('service_type','=','service');

        }else{
            return $builder->where('service_type','=','service');
        }
    }
}
