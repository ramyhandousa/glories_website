<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class OrderByScope implements Scope
{

    public function apply(Builder $builder, $value)
    {

        if($value == 'finish'){
            return $builder->whereIn('status_info',["finish","refuse"]);

        }elseif($value == 'accepted'){

            return $builder->where('status_info','=',"processing");

        }else{
             return $builder->where('status_info','=',"pending");
        }
    }
}
