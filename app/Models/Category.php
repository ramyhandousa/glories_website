<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    use Translatable;
    public array $translatedAttributes = ['name' ,'description'];
    protected $fillable = ['name','description','image','parent_id','is_suspend','is_visible'];

    protected $hidden = ['translations'];

    public function getImageAttribute($key): string
    {

//        return  env('IMAGE_URL').$key;

        if ($key && file_exists(public_path($key))) {
            return asset($key);
        } else {
            return   asset('/category.jpg');
        }
    }
       public function users() {
	    return $this->belongsToMany(User::class);
       }

       public function children()
       {
	    return $this->hasMany(Category::class, 'parent_id');
       }


       public function parent(){
	    return $this->belongsTo(Category::class,'parent_id');
       }

       public function parent_filter(){
	    return $this->belongsTo(Category::class,'parent_id')->select('id','parent_id');
       }

       public function product(){
	    return $this->hasMany(Product::class,'category_id');
       }
       public function scopeIsActive($query)
       {
	    return $query->whereIsSuspend(0);
       }
}
