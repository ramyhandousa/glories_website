<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use Translatable;
    public $translatedAttributes = ['name'];
    protected $fillable = ['name','account_number', 'is_published'];

       protected $hidden = [
	     'translations', 'updated_at',
       ];
}
