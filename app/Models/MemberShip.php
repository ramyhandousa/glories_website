<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class MemberShip extends Model
{
       use Translatable;
       public $translatedAttributes = ['name'];
       protected $fillable = ['name','price','number_product','number_images',
				   'number_services_enjoy','number_category','period_start',
				   'period_end','is_suspend','message'];


    public function getImageAttribute($key){

        $file = URL('/').$key;
        if (file_exists($file)) {
            return $file;
        } else {
            return   URL('/') .  '/category.jpg';
        }
    }

       public function users(){

	    return $this->hasMany(Provider::class ,'member_id');
       }

}
