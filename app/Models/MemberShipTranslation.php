<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberShipTranslation extends Model
{
       public $timestamps = false;
       protected $fillable = ['name'];
}
