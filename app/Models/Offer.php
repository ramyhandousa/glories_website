<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Offer extends Model
{

    protected $fillable = ['product_id' , 'price','period_start','period_end'];

    public function product(){
       return $this->belongsTo(Product::class);
    }



}
