<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankTransfer extends Model
{
    protected $fillable = [
       'user_id', 'userName'    , 'userAccount' , 'iban' , 'money', 'bank_id','bank_name', 'member_ship_id', 'image'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }


   public function mybank(){
        return $this->belongsTo(Bank::class,'bank');
    }

}
