<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use HasFactory, Notifiable , CanBeScoped;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'phone',
        'email',
        'is_user',
        'api_token',
        'password',
        'city_id',
        'latitute',
        'longitute',
        'address',
        'image',
        'action_code',
        'is_active',
        'accepted',
        'payment',
        'is_suspend',
        'message',
        'status',
        'login_count',
        'date_subscription',
        'last_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public $appends = ['rate'];

    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->api_token = Str::random(60);
            $model->date_subscription = Carbon::today()->format('Y-m-d');
        });
    }
    public function getImageAttribute($key){

        if ($key && file_exists(public_path($key))) {

            return asset($key);

        } else {

            return   asset('/default-profile-img.png');
        }
    }

    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    public function provider()
    {
        return $this->hasOne(Provider::class);
    }

    public function verify()
    {
        return $this->hasOne(VerifyUser::class);
    }

    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function categories() {
        return $this->belongsToMany(Category::class , 'category_user','user_id','category_id')->withTimestamps();
    }

    public function my_favorites()
    {

        return $this->belongsToMany(Product::class, 'favorite_user', 'user_id', 'product_id')->withTimestamps();
    }

    public function filter_my_favorites(){

        return $this->my_favorites()->select(['products.id','products.user_id','products.images'])->with('user',function ($user){
            $user->select('id','name','phone');
        })->get();
    }

    public function favorites()
    {

        return $this->belongsToMany(User::class, 'favorite_user', 'user_id', 'provider_id')
            ->withPivot('product_id')->withTimestamps();

    }
    public function favoriteProviders()
    {
        return $this->belongsToMany(User::class, 'favorite_user', 'provider_id', 'user_id')
            ->withPivot('product_id')->withTimestamps();

    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function incomeOrder(){
        return $this->hasMany(Order::class)->latest();
    }
    public function outOrder(){
        return $this->hasMany(Order::class, 'provider_id')->latest();
    }


    public function member()
    {
        return $this->hasOne(MemberSuperscription::class , 'user_id');
    }

    public function active_member()
    {
        return $this->member()->whereDate('member_end','>=',Carbon::today());
    }

    public function my_products(){
        return $this->products()->where('is_suspend','=',0)
            ->whereHas('category',function ($category){
                $category->where('is_suspend','=',0);
        });
    }


    public function ratings(){
        return $this->hasMany(Rating::class,'rateable_id');
    }


    public function getRateAttribute()
    {
        $rate = $this->ratings()->average('rating');
        return $rate?  number_format($rate,1) : 0;
    }
}
