<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use Translatable , CanBeScoped , SoftDeletes;
    public $translatedAttributes = ['name', 'description'];
    protected $fillable = ['name', 'description','category_id','user_id' ,'price','images','images_path'];

    protected $casts = [ 'images' => 'array' ,'images_path' => 'array'];

    protected $hidden = ['translations'];

    public function user() {

	    return $this->belongsTo(User::class);
    }

    public function user_filter(){
	    return $this->belongsTo(User::class,'user_id')
		  		->select('id','name','email','phone','image');
    }



      public function provider()
      {
	     return $this->belongsTo(Provider::class,'user_id');
      }


       public function orders()
       {
        return $this->hasMany(Order::class);
       }

//       public function images()
//       {
//        return $this->hasMany(Image::class);
//       }

       public function category()
       {
        return $this->belongsTo(Category::class);
       }


        public function offer(){
	    return $this->hasOne(Offer::class)->whereStatus(0);
       }

         public function offer_filter(){
    	    return $this->hasOne(Offer::class)->whereStatus(0)->select('product_id','price','period_start','period_end');
       }

    public function active_offer()
    {
        return $this->offer()->whereDate('period_end','>=',Carbon::today());
    }

//    public function subscriptions()
//    {
//        return $this->belongsToMany(Subscription::class, 'product_subscription' ,'product_id','subscription_id');
//    }
}
