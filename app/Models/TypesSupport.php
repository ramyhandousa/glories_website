<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class TypesSupport extends Model
{
       use Translatable;
       public $translatedAttributes = ['name'];
       protected $fillable = ['name'];

       protected $hidden = ['translations'];

       public function support()
       {
	    return $this->belongsTo(Support::class);
       }
}
