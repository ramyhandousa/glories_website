<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryUserModel extends Model
{
    use HasFactory;

    protected $fillable = ['user_id' , 'category_id'];

    protected $table = 'category_user';

}
