<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberSuperscription extends Model
{
       protected $fillable = [	'user_id','member_id','name','price',
	     				'number_product','number_images',
				   	'number_services_enjoy',
	     				'number_category','period_start',
				   	'period_end','member_end'];


       public function user(){
           return $this->belongsTo(User::class);
       }

}
