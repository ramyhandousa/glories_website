<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use CanBeScoped;
   protected $fillable = [
           'user_id', 'provider_id', 'product_id', 'price_product', 'category_product', 'name','phone',
            'city_id', 'address','latitute', 'longitute','time',
        'date', 'date_action', 'payment_type', 'status','status_info', 'message'
   ];

   protected $appends = [
       'payment_type_translation','status_translation'
   ];

    public function user() {
      return $this->belongsTo(User::class);
   }

   public function provider() {

          return $this->belongsTo(User::class,'provider_id');
   }

   public function city() {

	    return $this->belongsTo(City::class);
   }

   public function category() {

	    return $this->belongsTo(Category::class,'category_product');
   }

   public function product() {

        return $this->belongsTo(Product::class,'product_id');
    }

    public function rate_order(){
        return $this->hasOne(Rating::class,'order_id');
    }

    public function getStatusTranslationAttribute()
    {
        $status = $this->attributes['status_info'];

        if ($status)
            return match ($status) {
                'pending' => __('global.pending_order'),
                'processing' => __('global.preparing_order'),
                'accepted' => __('global.accepted_order'),
                'refuse'    => __('global.refuse_order'),
                'refuse_system' => __('global.refuse_dealer'),
                'finish' => __('global.finish_order'),
                default => '',
            };
    }

    public function getPaymentTypeTranslationAttribute()
    {
        $status = $this->attributes['payment_type'];
        if ($status)
            return match ($status) {
                '1' => __('global.payment_type.bank_transfer'),
                '2' => __('global.payment_type.pay'),
                '3' => __('global.payment_type.cash'),
                '4' => __('global.payment_type.pay_electronic'),
                default => '',
            };
    }

}
