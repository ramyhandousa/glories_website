<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
       protected $fillable = [
	     'user_id',
	     'facebook',
	     'twitter',
	     'snap_chat',
	     'connect_phone',
	     'connect_email',
	     'bank_transfer',
	     'pay',
	     'cash',
	     'pay_electronic',
	     'member_id',
       ];

       public function user()
       {
	    return $this->belongsTo(User::class);
       }
}
