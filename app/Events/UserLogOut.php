<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserLogOut
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $request ;

    public function __construct( $user , $request)
    {
        $this->user     = $user;
        $this->request  = $request;
    }
}
