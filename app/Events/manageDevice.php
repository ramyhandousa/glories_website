<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class manageDevice
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $user;
    public $request ;

    public function __construct( $user , $request)
    {
        $this->user     = $user;
        $this->request  = $request;
    }


}
