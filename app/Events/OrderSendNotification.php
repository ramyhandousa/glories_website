<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderSendNotification
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

     public $devices;
     public $order;
     public $body_notification;
     public $type;
    public function __construct($devices, $order, $body_notification , $type)
    {
       $this->devices = $devices;
       $this->order = $order;
       $this->body_notification = $body_notification;
       $this->type = $type;
    }


}
