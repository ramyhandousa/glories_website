<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PhoneOrEmailChange
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $code ;
    public $change ;
    public $request ;

    public function __construct( $user , $code , $changeEmailOrPhone , $request)
    {
        $this->user     = $user;
        $this->code     = $code;
        $this->change   = $changeEmailOrPhone;
        $this->request  = $request;
    }
}
