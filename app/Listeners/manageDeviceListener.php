<?php

namespace App\Listeners;

use App\Events\manageDevice;
use App\Models\Device;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class manageDeviceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  manageDevice  $event
     * @return void
     */
    public function handle(manageDevice $event)
    {
        $request = $event->request;
        $user = $event->user;
        if ($request->deviceToken) {

            Device::updateOrCreate(['device' => $request->deviceToken],['user_id' =>$user->id ,'device' => $request->deviceToken ]);
        }
    }
}
