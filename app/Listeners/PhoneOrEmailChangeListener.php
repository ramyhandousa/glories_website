<?php

namespace App\Listeners;

use App\Events\PhoneOrEmailChange;
use App\Mail\codeActivation;
use App\Models\VerifyUser;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class PhoneOrEmailChangeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PhoneOrEmailChange  $event
     * @return void
     */
    public function handle(PhoneOrEmailChange $event)
    {
        $phone =   $event->change == 'phone' ? $event->request->phone   : $event->user->phone;
        $email =   $event->change == 'email'  ?  $event->request->phone : $event->user->email ;

        $data = [ 'email' => $email,'phone' => $phone , 'action_code'  => $event->code  ];

        VerifyUser::updateOrCreate(['user_id' => $event->user->id], $data);

        if ($event->change === 'email'){

            try {
                // Sending E-mail
                Mail::to($event->user->email)->send(new codeActivation($event->user, $event->code));

            }catch (\Exception $exception){
                Log::info('Exception Email Sending ' . $exception);
            }


        }else{


//	        Sms::sendMessage('Activation code:' . $event->code, $event->request->phone);
        }
    }
}
