<?php

namespace App\Listeners;

use App\Events\OrderSendNotification;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OrderSendNotificationListener
{
    public $push;

    public function __construct(   PushNotification $push)
    {

        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  OrderSendNotification  $event
     * @return void
     */
    public function handle(OrderSendNotification $event)
    {
        $order = $event->order;
        $type = $event->type;
        $devices = $event->devices;
        $title = 'الطلبات';
        $body_notification = $event->body_notification;

       $additional_data = ['type'=> $type,'order'=>['id'=>$order->id , 'provider_id' => $order->provider_id ]  ];

       if (count($devices) > 0){
           $this->push->sendPushNotification(null, $devices, $title,$body_notification ,$additional_data);
       }
    }

}
