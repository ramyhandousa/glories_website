<?php


namespace App\Repositories;

use App\Http\Resources\Api\Offer\OfferListResource;
use App\Http\Resources\Api\Offer\OfferListUnAuthResource;
use App\Http\Resources\Api\Offer\OfferProductResource;
use App\Models\Product;
use App\Models\User;
use App\Traits\paginationTrait;

class OfferRepository
{
    use paginationTrait ;

    public function list($request){

        $user = User::whereApiToken($request->headers->get('apiToken'))->first();

        $query = Product::whereHas('active_offer')->whereIsSuspend(0)->withCount('active_offer');

        if ($user){

            $products = $query->where('user_id',$user->id)->latest()->get();

            $count_offer = $products->sum('active_offer_count');

            $countOfferRemaining = optional($user->active_member)->number_services_enjoy - $count_offer;

            $data = OfferListResource::collection($products);

        }else{ // Un Authentication

            $products =  $query->latest()->get();

            $count_offer = $products->sum('active_offer_count');

            $countOfferRemaining = 0;

            $data = OfferListUnAuthResource::collection($products);
        }

        return [ 'countOffer' => $count_offer, 'countOfferRemaining' => $countOfferRemaining ,'data' => $data  ] ;
    }

    public function show($offer){

    }

    public function store($request){

        $product =  $request->user()->products()->where('id',$request->product_id)->first();

        $offer = $product->offer()->create($request->validated());

        return new OfferProductResource($offer);
    }


    public function update($request,$offer){

        $offer->update($request->validated());

        return new OfferProductResource($offer);
    }

    public function cancel_offer($offer){
        $offer->delete();
    }

}
