<?php

namespace App\Repositories;



use App\Http\Resources\Api\Category\ListCategory;
use App\Http\Resources\Api\Category\ListCategoryResource;
use App\Http\Resources\Api\User\ProviderCategoryResource;
use App\Models\Category;
use App\Models\CategoryUserModel;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class CategoryRepository {


    public function list($request){

        if ($request->has('users')){

            $query  = User::whereHas('active_member')->whereHas('products')
                    ->whereHas('categories' ,function ($q) use ($request) {

                        $q->whereCategoryId($request->users);

                    })->where('accepted',1)->where('is_suspend',0)->select('id','name','image')->get();

            $data = $query;

        }else{

            $parent_id = $request->subCategory ? : 0;

            $query = Category::whereParentId($parent_id)->whereIsSuspend(0)->whereIsVisible(0)->get();

            $query->map(function ($q) use ($query , $request){

                if ($q->children->count() > 0 && $q->users->count() > 0  ){

                    $query->push(['id' =>(int) $request->subCategory, 'name' => 'قسم عام ' , 'hasChildren' => false , 'isGlobal' => true ]);
                }
            }) ;

            $data  = ListCategory::collection($query);
        }

        return $data;
    }


    public function store($request){

        $user = $request->user();

        $user->categories()->attach($request->categoryId);

        return ProviderCategoryResource::collection($user->categories);
    }


    public function update_categories($request){

        $user = $request->user();

        foreach ($request->categoryId as $id){

            $data = ['user_id' => $user->id ,'category_id' => $id];

            CategoryUserModel::updateOrCreate($data , $data);
        }

        return ProviderCategoryResource::collection($user->categories);
    }

}
