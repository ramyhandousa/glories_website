<?php


namespace App\Repositories;


use App\Http\Resources\Api\Notification\NotificationListResource;
use App\Http\Resources\Api\Product\ListProductFavouriteResource;
use App\Http\Resources\Api\Product\listProductResource;
use App\Http\Resources\Api\User\ProviderShowModelResource;
use App\Models\Rating;
use App\Models\User;
use App\Scoping\Scopes\CategoryProductScope;
use Illuminate\Http\Resources\Json\JsonResource;

class UserRepository
{

    public function show_provider($request){

        $provider = User::with(['city','provider','products' => function($product){

            $product->withScopes($this->filter_product());

        }])->findOrFail($request->id);

        return new ProviderShowModelResource($provider);
    }

    public function rate_provider($request){
        $user = $request->user();

        Rating::create(['user_id' => $user->id, 'order_id' => $request->orderId ,
                        'rateable_type' => 'App\Models\User', 'rateable_id' => $request->providerId ,
                        'rating' => $request->rateValue]);
    }


    public function list_favourite($request){

        $user = $request->user();

        $products =   $user->my_favorites;

        return ListProductFavouriteResource::collection($products);
    }


    public function list_Notification($request){
        $user = $request->user();

        $notifications = $user->notifications->reverse()->values();

        $today = NotificationListResource::collection($notifications->take(2));

        $before = NotificationListResource::collection($notifications->skip(2)->values());

        return ['today' => $today , 'before' => $before];
    }


    private function filter_product(){
        return [
            'categoryId' => new CategoryProductScope(),
        ];
    }
}
