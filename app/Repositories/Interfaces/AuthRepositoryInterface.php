<?php


namespace App\Repositories\Interfaces;

interface  AuthRepositoryInterface
{

    public function register($request, $code);

    public function login($request);

    public function forgetPassword($request, $code);

    public function resetPassword($request);

    public function checkCode($request);

    public function resendCode($request, $code);

    public function changPassword($request);

    public function editProfile($request, $code);

    public function logOut($request);

}
