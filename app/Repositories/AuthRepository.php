<?php


namespace App\Repositories;

use App\Events\manageDevice;
use App\Events\PhoneOrEmailChange;
use App\Events\UserLogOut;
use App\Http\Helpers\Images;
use App\Http\Helpers\Sms;
use App\Http\Resources\Api\User\ProviderModelResource;
use App\Http\Resources\Api\User\UserModelResource;
use App\Models\User;
use App\Models\VerifyUser;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class AuthRepository implements AuthRepositoryInterface
{

    public function register($request , $code)
    {
        $user = User::create($request->validated()['user']);

        if ($request->userType == 1){
            $user->provider()->create($request->validated()['provider']);
            $user->categories()->attach($request->categoryId);
        }

        $user->update(['image' => $this->uploadImageOrDefault($request) ]);

        $this->createVerfiy($request,$user,$code);

//        Sms::sendMessage('Activation code:' . $code, $request->phone);

    }

    public function login($request)
    {
        $user =  $request->user();

        event(new manageDevice($user,$request));

        return  $user->is_user == 0 ? new UserModelResource($user) : new ProviderModelResource($user);
    }

    public function forgetPassword($request , $code)
    {
        $user = User::wherePhone($request->phone)->first();

        event(new PhoneOrEmailChange($user,$code,'phone',$request));
    }

    public function resetPassword($request)
    {
        $user = User::wherePhone( $request->phone)->first();

        if ($request->password){
            $user->update(['password' => $request->password]);
        }

        event(new manageDevice($user,$request));

        return  $user->is_user == 0 ? new UserModelResource($user) : new ProviderModelResource($user);
    }

    public function checkCode($request)
    {
        $verifyUser = VerifyUser::wherePhone($request->phone)->with('user')->first();

        $user = $verifyUser['user'];

        $user->update(['phone' => $verifyUser->phone ? : $user->phone,'is_active' => 1 ]);

        $verifyUser->delete();

        event(new manageDevice($user,$request));

        return  $user->is_user == 0 ? new UserModelResource($user) : new ProviderModelResource($user);
    }

    public function resendCode($request ,$code)
    {
        $user = VerifyUser::wherePhone($request->phone)->with('user')->first();

        event(new PhoneOrEmailChange($user['user'],$code,'phone',$request));
    }

    public function changPassword($request)
    {
        $user = Auth::user();

        if ($request->newPassword){

            $user->update( [ 'password' => $request->newPassword] );

            $message =  trans('global.password_was_edited_successfully') ;

        }else{

            $message = trans('global.password_not_edited');
        }

        return  ['message' => $message] ;
    }

    public function change_phone($request, $code){

        $user = $request->user();

        VerifyUser::updateOrCreate(['user_id' => $user->id], [ 'phone' => $request->newPhone,  'action_code'  => $code]);

//        Sms::sendMessage('Activation code:' . $code, $request->newPhone);
    }

    public function editProfile($request, $code)
    {
        $user = $request->user();
        $user->fill($request->validated()['user']);
        $user->save();

        if ($user->is_user == 1){ // If Provider Update additional data
            $user->provider()->update($request->validated()['provider']);
        }

        $this->edit_user_phone($user,$request,$code);// If Change Phone

        return  $user->is_user == 0 ? new UserModelResource($user) : new ProviderModelResource($user);
    }

    private function edit_user_phone($user,$request , $code ){
        $columnChange = 'phone';
        $requestChange = $request->phone;

        $columnExists = $user->where($columnChange, $requestChange)->exists();

        if (!$columnExists && $requestChange){
            event(new PhoneOrEmailChange($user,$code,$columnChange,$request));
        }
    }


    public function logOut($request)
    {
        $user = Auth::user();

        event(new UserLogOut($user,$request));
    }


    public function createVerfiy($request , $user , $action_code){

        $data = [ 'phone' => $request->phone,'email' => $request->email , 'action_code'  => $action_code];

        VerifyUser::updateOrCreate(['user_id' => $user->id], $data);
    }

    public function uploadImageOrDefault($request){

        if ($request->file('image')){
            $image = '/files/profile/'. Images::uploadImage($request,'image','/files/profile/','400','150');
        }else{
            $image = '/default-profile-img.png';
        }
        return $image;
    }
}
