<?php


namespace App\Repositories;


use App\Http\Resources\Api\Product\listProductResource;
use App\Http\Resources\Api\Product\showProductResource;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductRepository
{

    public function list($request){

        $user = User::whereApiToken($request->headers->get('apiToken'))->first();

        $member = optional($user)->member;

        $my_products = optional($user)->my_products;

        $data =  $my_products ? listProductResource::collection($my_products) : [];

        $countProduct = $my_products ? $my_products->count() : 0;

        $countProductRemaining =   $member ? $member->number_product - $countProduct : 0;

        return ['data' => $data ,'countProduct' => $countProduct , 'countProductRemaining' => $countProductRemaining] ;
    }

    public function show($request , $product){

        return new showProductResource($product);
    }

    public function store($request){

        $user = $request->user();

        $product = $user->products()->create($request->validated());

        return new listProductResource($product);
    }

    public function update($request ,$product ){
        $user = $request->user();

        $product->update($request->validated());

        return new listProductResource($product);
    }

    public function delete($product){
        $product->delete();
    }
}
