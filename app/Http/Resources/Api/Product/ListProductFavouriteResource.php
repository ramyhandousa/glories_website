<?php

namespace App\Http\Resources\Api\Product;

use App\Http\Resources\Api\User\ProviderCategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ListProductFavouriteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'description'   => $this->description,
            'price'          => $this->price,
            'images_path'   => $this->when( $this->images_path, $this->images_path),
            'isFavorite'    => 1,
            'categories'    => $this->when($request->has('categories'),new ProviderCategoryResource($this->category)),
        ];
    }
}
