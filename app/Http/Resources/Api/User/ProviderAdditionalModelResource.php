<?php

namespace App\Http\Resources\Api\User;

use Illuminate\Http\Resources\Json\JsonResource;

class ProviderAdditionalModelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'facebook'          => $this->when($this->facebook,$this->facebook),
            'twitter'           => $this->when($this->twitter,$this->twitter),
            'snap_chat'         => $this->when($this->snap_chat,$this->snap_chat),
            'connect_phone'     =>  $this->connect_phone    ,
            'connect_email'     =>  $this->connect_email ,
            'bank_transfer'     =>  $this->bank_transfer ,
            'pay'               => $this->when($this->pay,$this->pay),
            'cash'              => $this->when($this->cash,$this->cash),
            'pay_electronic'    =>  $this->pay_electronic ,
            'member_id'         => $this->when($this->member_id,$this->member_id),
        ];
    }
}
