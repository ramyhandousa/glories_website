<?php

namespace App\Http\Resources\Api\User;

use Illuminate\Http\Resources\Json\JsonResource;

class ProviderFilterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'phone'     => $this->phone,
            'email'     => $this->when($this->email,$this->email),
            'image'     => $this->when($this->image,$this->image),
            'rate'      => $this->when($this->rate,$this->rate),
            'provider'  => $this->when($this->provider && $request->has('provider'), new ProviderAdditionalModelResource($this->provider))
        ];
    }
}
