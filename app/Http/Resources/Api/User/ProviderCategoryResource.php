<?php

namespace App\Http\Resources\Api\User;

use Illuminate\Http\Resources\Json\JsonResource;

class ProviderCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'mainCategory'  => $this->get_main_category($this),
            'name'          => $this->name,
            'description'   => $this->description,
            'image'         => $this->when($this->image,$this->image),
            'parent_filter' => $this->when($this->parent_filter, new ProviderCategoryResource($this->parent_filter)),
        ];
    }


    private function get_main_category($main){
        if ($main->parent_id == 0 ){
            $mainCategory = 0;
        }

        if ( $main->parent_id != 0 && $main->parent_filter ){
            $mainCategory = 1;
        }

        if ( $main->parent_id != 0 && optional($main->parent_filter)->parent_filter ){
            $mainCategory = 2;
        }

        if (isset($mainCategory)) {
            return $mainCategory;
        }
    }
}
