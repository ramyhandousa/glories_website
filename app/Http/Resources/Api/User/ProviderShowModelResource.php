<?php

namespace App\Http\Resources\Api\User;

use App\Http\Resources\Api\Product\listProductResource;
use App\Http\Resources\Global\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProviderShowModelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'phone'     => $this->phone,
            'email'     => $this->when($this->email,$this->email),
            'image'     => $this->when($this->image,$this->image),
            'rate'      => $this->when($this->rate,$this->rate),
            'city'      => $this->when($this->city, new CityResource($this->city)),
            'provider'  => $this->when($this->provider , new ProviderAdditionalModelResource($this->provider)),
            'products'  => $this->when($this->provider ,listProductResource::collection($this->products))
        ];
    }
}
