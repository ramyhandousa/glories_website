<?php

namespace App\Http\Resources\Api\User;

use App\Http\Resources\Global\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProviderModelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'phone'     => $this->phone,
            'email'     => $this->email,
            'is_user'   => $this->is_user,
            'api_token' => $this->api_token,
            'is_active' => $this->is_active,
            'accepted'  => $this->accepted,
            'payment'   => $this->payment,
            'date_subscription'   => $this->when($this->date_subscription,$this->date_subscription),
            'address'   => $this->when($this->address,$this->address),
            'latitute'  => $this->when($this->latitute,$this->latitute),
            'longitute' => $this->when($this->longitute,$this->longitute),
            'image'     => $this->when($this->image,$this->image),
            'rate'      => $this->when($this->rate,$this->rate),
            'city'      => $this->when($this->city, new CityResource($this->city)),
            $this->mergeWhen($this->payment == 2 && $this->active_member,[
                'my_member_ship' => new ProviderMemberShipResource($this->active_member)
            ]),
            'provider' => new ProviderAdditionalModelResource($this->provider),
            'categories' => $this->when($this->categories , ProviderCategoryResource::collection($this->categories))
        ];
    }
}
