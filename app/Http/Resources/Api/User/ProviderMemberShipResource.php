<?php

namespace App\Http\Resources\Api\User;

use Illuminate\Http\Resources\Json\JsonResource;

class ProviderMemberShipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user           = $this->user;
        $products       = $user->products->loadCount('active_offer');
        $countProduct   =  $products->count();
        $countOffer     =  $products->sum('active_offer_count');
        return [
            "id"                    => $this->id,
            "price"                 => $this->price,
            "number_product"        => $this->number_product,
            "number_images"         => $this->number_images,
            "number_services_enjoy" => $this->number_services_enjoy,
            "number_category"       => $this->number_category,
            "countProductHave"      => $countProduct,
            "countProductRemaining" => $this->number_product - $countProduct,
            "countOfferRemaining"   => $this->number_services_enjoy - $countOffer
        ];
    }
}
