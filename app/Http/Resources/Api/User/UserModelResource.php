<?php

namespace App\Http\Resources\Api\User;

use App\Http\Resources\Global\CityResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserModelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'phone'     => $this->phone,
            'email'     => $this->email,
            'is_user'   => $this->is_user,
            'api_token' => $this->api_token,
            'is_active' => $this->is_active,
            'address'   => $this->when($this->address,$this->address),
            'latitute'  => $this->when($this->latitute,$this->latitute),
            'longitute' => $this->when($this->longitute,$this->longitute),
            'image'     => $this->when($this->image,$this->image),
            'rate'      => $this->when($this->rate,$this->rate),
            'city'      => $this->when($this->city, new CityResource($this->city))
        ];
    }
}
