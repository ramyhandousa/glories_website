<?php

namespace App\Http\Resources\Api\Notification;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"            => $this->id,
            "user_id"       => $this->user_id,
            "sender_id"     => $this->sender_id,
            "order_id"      => $this->when($this->order_id,$this->order_id),
            "product_id"      => $this->when($this->product_id,$this->product_id),
            "title"         => $this->title,
            "body"          => $this->body,
            "type"          => $this->type,
            "is_read"       => $this->is_read,
            "created_at"    => $this->created_at,
            "time"          => $this->created_at->diffForHumans(),
        ];
    }
}
