<?php

namespace App\Http\Resources\Api\MemberShip;

use Illuminate\Http\Resources\Json\JsonResource;

class ListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"                    => $this->id,
            "name"                  => $this->name,
            "price"                 => $this->price,
            "number_product"        => $this->number_product,
            "number_images"         => $this->number_images,
            "number_services_enjoy" => $this->number_services_enjoy,
            "number_category"       => $this->number_category,
            "image"                 => $this->image,
        ];
    }
}
