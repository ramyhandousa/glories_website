<?php

namespace App\Http\Resources\Api\Order;

use App\Http\Resources\Api\Product\OfferProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'description'   => $this->description,
            'price'         => $this->price,
            'images_path'   => $this->when( $this->images_path, $this->images_path),
            'offer'         => new OfferProductResource($this->offer)

        ];
    }
}
