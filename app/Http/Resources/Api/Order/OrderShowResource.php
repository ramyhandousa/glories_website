<?php

namespace App\Http\Resources\Api\Order;

use App\Http\Resources\Api\Product\showProductResource;
use App\Http\Resources\Api\User\ProviderAdditionalModelResource;
use App\Http\Resources\Api\User\ProviderCategoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            "price_product"     => $this->price_product,
            "price_offer"       => $this->when($this->price_offer,$this->price_offer,),
            "is_offer"          => $this->is_offer,
            "name"              => $this->name,
            "phone"             =>  $this->phone,
            "address"           => $this->address,
            "latitute"          => $this->latitute,
            "longitute"         => $this->longitute,
            "time"              => $this->time,
            "date"              => $this->date,
            "date_action"       => $this->date_action,
            "payment_type"      => $this->payment_type,
            "is_paid"           => $this->is_paid,
            "paymentId"         => $this->when($this->paymentId,$this->paymentId),
            "status"            => $this->status,
            "message"           => $this->when($this->message,$this->message),
            "created_at"        => $this->created_at,
            "statusName"        => $this->status_translation,
            "userName"          => optional($this->user)->name,
            "userNameProvider"  => optional($this->provider)->name,
            "userEmailProvider" => optional($this->provider)->email,
            "userPhoneProvider" => optional($this->provider)->phone,
            "userCity"          => optional(optional($this->user)->city)->name,
            "rate"              => optional($this->rate_order)->rating,
            "provider"          => new ProviderAdditionalModelResource(optional($this->provider)->provider),
            'category'          => new ProviderCategoryResource($this->category),
            'product'           => new OrderProductResource($this->product)

        ];
    }
}
