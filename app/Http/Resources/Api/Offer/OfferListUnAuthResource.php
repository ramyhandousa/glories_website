<?php

namespace App\Http\Resources\Api\Offer;

use Illuminate\Http\Resources\Json\JsonResource;

class OfferListUnAuthResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'description'   => $this->description,
            'price'          => $this->price,
            'images_path'   => $this->when( $this->images_path, $this->images_path),
            'isFavorite'    => 0,
            'offer_filter'  => new OfferModelResource($this->offer)
        ];
    }
}
