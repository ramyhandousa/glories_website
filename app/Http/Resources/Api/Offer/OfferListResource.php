<?php

namespace App\Http\Resources\Api\Offer;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::whereApiToken($request->headers->get('apiToken'))->first();

        $favorite = $user? $user->my_favorites()->wherePivot('product_id',$this->id)->first() ? 1 : 0 : 0;

        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'description'   => $this->description,
            'price'          => $this->price,
            'images_path'   => $this->when( $this->images_path, $this->images_path),
            'isFavorite'    => $favorite,
            'offer_filter'  => new OfferModelResource($this->offer)
        ];
    }
}
