<?php

namespace App\Http\Resources\Api\Offer;

use App\Http\Resources\Api\Product\listProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferModelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'product_id'    => $this->product_id,
            'price'         => $this->price,
            'period_start'  => $this->period_start,
            'period_end'    => $this->period_end,
        ];
    }
}
