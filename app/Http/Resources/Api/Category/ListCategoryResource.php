<?php

namespace App\Http\Resources\Api\Category;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ListCategoryResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Support\Collection
     */
    public function toArray($request)
    {

        return $this->collection->transform(function($category) use ($request){

            $count_children = $category->children->count();
            $count_users = $category->users->count();

            $data =  [
                'id'            => $category->id,
                'name'          => $category->name,
                'description'   => $category->description,
                'image'         => $this->when($category->image,$category->image),
                'hasChildren'   => $count_children > 0,
                'countChildren' => $count_children ,
                'hasUsers'      =>  $count_users > 0,
                'countUsers'    => $count_users,
            ];

//            if ($count_children > 0 && $count_users > 0 && $request->has('subCategory')){
//                collect($data)->push(['id' =>(int) $request->subCategory, 'name' => 'قسم عام ' , 'hasChildren' => false , 'isGlobal' => true ]);
//            }

            return $data;
        });
    }


}
