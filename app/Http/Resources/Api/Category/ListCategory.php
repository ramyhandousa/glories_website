<?php

namespace App\Http\Resources\Api\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class ListCategory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $count_children = isset($this['children']) ? $this['children']->count() : 0;
        $count_users    =  isset($this['users']) ? $this['users']->count() : 0;
        return [
            'id'            => $this['id'],
            'name'          => $this['name'],
            'description'   =>  $this->when(isset($this['description']) ,isset($this['description']) ? $this['description']  : null)  ,
            'image'         =>  $this->when(isset($this['image']) ,isset($this['image']) ? $this['image']  : null)  ,
            'hasChildren'   => $count_children > 0,
            'isGlobal'      => $this->when($request->has('subCategory'),(boolean) $this['isGlobal']) ,
            $this->mergeWhen(!$this['isGlobal'] ,[

                'countChildren' => $count_children ,
                'hasUsers'      =>  $count_users > 0,
                'countUsers'    => $count_users,
            ])
        ];
    }

}
