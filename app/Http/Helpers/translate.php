<?php
       
       namespace App\Http\Helpers;

       use Stichoza\GoogleTranslate\GoogleTranslate;
       
       class translate{
       
       
       public static function translateLang($from ,$to , $text){
       
	    $tr = new GoogleTranslate();
	    
	    $result =  $tr->setSource($from)->setTarget($to)->translate($text);
     
	    return $result;
       }
       
       }