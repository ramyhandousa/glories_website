<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeviceController extends Controller
{

    public function update_device(Request $request){
        $user = \App\Models\User::whereId($request->id)->first();

        if ($request->token) {
            $data = \App\Models\Device::where('device', $request->token)->first();
            if ($data) {
                $data->user_id = $user->id;
                $data->save();
            } else {


                $data = new \App\Models\Device;
                $data->device = $request->token;
                $data->user_id = $user->id;
                $data->device_type = 'web';
                $data->save();
            }
        }
    }


    public function check_column(Request $request){
        $model = $request->model;
        $item = $request->item;
        $column = $request->column;
        $itemId = $request->itemId;

        $myModel = $model::where($column, $item)->first();
        if (isset($itemId) && $itemId != 'undefined') {
            $myModel = $model::where([$column => $item])->where('id', '!=', $itemId)->first();
        }
        if (!empty($myModel)) {
            return response()->json([
                'status' => true,
                'data' => $myModel,
                'message' => "هذا  ". $column ." مسجل من قبل"

            ]);
        } else {
            return response()->json([
                'status' => false,
            ]);

        }
    }

    public function sub_categories(Request $request){

        $cities =\App\Models\Category::whereParentId($request->id)->get();

        if (!empty($cities) && count($cities) > 0){
            return response()->json( $cities);
        }else{

            return response()->json(401);
        }

    }

    public function getNumberImage(Request $request){

        if ($request->id && $request->id !=''&&  (int) $request->id  ){
            $member = \App\Models\MemberShip::find($request->id);
            $countImage = $member->number_images;
            return response()->json( $countImage);
        }

    }
}
