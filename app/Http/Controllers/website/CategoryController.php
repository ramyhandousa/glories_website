<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Http\Requests\Categories\StoreVaild;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['AuthWebSite','acceptedOrHaveMemberShip'])->only(['my_categories','store']);
    }

    public function index(){

        $categories = Category::where('parent_id',0)->cursorPaginate(9);

        return view('categories.index', compact('categories'));
    }

    public function my_categories(){

        $user = User::whereId(Auth::id())->with(['member','products' => function($product){
            $product->withCount('offer');
        }])->withCount('products','categories')->with('categories')->first();

        $memberShip = $user->member;

        $countOffer = $user->products->sum('offer_count');

        $categories = Category::whereParentId(0)->whereIsSuspend(0)->get();

        return view('categories.my_categories',compact('user','memberShip','countOffer','categories'));
    }

    public function show(Request $request, $id){

        $conditionUserQuery = function ($child) {
            $child->select('id','parent_id')->with(['users' => function($user) use($child){
                $user->whereHas('products')->select('users.id');
            }]);
        };

        $category = Category::with([ 'children' => $conditionUserQuery , 'children.children.users' => function($child) {
            $child->select('users.id');
        }])->findOrFail($id);

        if( $category->children->count() > 0){

            $categories = Category::whereIn('id',$category->children->pluck('id'))->cursorPaginate(9);

            return view('categories.show', compact('categories'));
        }else{

            $users = User::where('id','!=',Auth::id())
                ->whereHas('products')
                ->whereHas('categories' ,function ($q) use ($request, $id) {

                    $q->whereCategoryId( $id);

                })->select('id','name','image')->cursorPaginate(9);

            return view('categories.show', compact('users'));
        }
    }

    public function store(StoreVaild $request){
        $user = $request->user();

        $user->categories()->attach($request->category);
        session()->flash('success', "لقد تم إضافة القسم  بنجاح");
        return redirect()->back();
    }
}
