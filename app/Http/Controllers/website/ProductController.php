<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Http\Requests\Products\ImageVaid;
use App\Http\Requests\Products\StoreVaid;
use App\Http\Requests\Products\UpdateVaild;
use App\Models\Category;
use App\Models\Product;
use App\Models\uploadImages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('AuthWebSite')->except((['listFavourite','makeFavourite']));
        $this->middleware('authUser')->only(['listFavourite','makeFavourite']);
    }

    public function index(Request $request){

        $user = $request->user();
        $products   =   Product::whereUserId($user->id)->cursorPaginate(2);
        $memberShip =   $user->member;

        $countproduct = $memberShip ?  $memberShip->number_product : 0;
        $countproductHave = $products->count();
        $countproductRemind = $countproduct - $countproductHave;

        return view('products.index',compact('memberShip','products','countproductHave','countproductRemind'));
    }

    public function show(Request $request ,Product $product){

        if ($request->user()->id != $product->user_id){
            return redirect()->back();
        }
        $categories = Category::whereParentId(0)->whereIsSuspend(0)->get();

        $subSubCategories = Category::whereHas('parent',function ($q){
            return $q->whereHas('parent');
        })->doesnthave('children')->get();


        $ids = $subSubCategories->pluck('id');
        $ids->all();

        $subCategories = Category::whereNotIn('id' ,$ids)->where('parent_id','!=', 0)->get();

        return view('products.show',compact('product','categories' ,'subCategories','subSubCategories'));
    }

    public function details(Product $product){

        $product->load('category.parent.parent');

        return view('products.details',compact('product'));
    }

    public function create(Request $request){

        $user = $request->user();
        $products   =   $user->products;
        $memberShip =   $user->member;

        if (!$memberShip){
            session()->flash('my_errors',  ' للأسف لا تملك عضوية لإضافة المنتجات ');
            return   redirect()->back();
        }

        $countproduct = $memberShip ? $memberShip->number_product : 0;
        $countproductHave = $products->count();
        $countproductRemind = $countproduct - $countproductHave;

        if ($countproductRemind == 0){

            session()->flash('my_errors',  ' اقصي إمكانية للمنتجات حسب العضوية هي '.$countproduct);
            return   redirect()->back();
        }

        $categories = Category::whereParentId(0)->whereIsSuspend(0)->get();

        return view('products.create',compact('categories','countproductHave','countproductRemind'));

    }

    public function store(StoreVaid $request){

        $lang =  app()->getLocale();

        $product = new  Product();
        $product->{"name:$lang"} = $request->product_name;
        $product->{"description:$lang"} = $request->description;
        $product->user_id = $request->user()->id;
        $product->category_id = $request->category;
        $product->price = $request->price;
        $product->save();

        if ($request->hasFile('images')):

            $ids = [];
            $images_path = [];
            foreach($request->file('images') as $image)
            {
                $name = time() . '.' . $image->getClientOriginalName();
                $path  = public_path('/files/products/' .$name);
                \Intervention\Image\Facades\Image::make($image)->resize(300, 300)->save($path);

                $file_path = '/files/products/' .$name;

                $images_path[] = $file_path;

                $images =  uploadImages::create(['url' => $file_path]);

                array_push($ids,json_encode($images->id));
            }
            $product->update(['images' => $ids ,'images_path' => $images_path ]);
        endif;

        return response()->json([  'status' => 200 ,'message' => "تم إضافة المنتج بنجاح",
                            'url' => route('products.index')
        ]);
    }

    public function update(UpdateVaild $request , Product $product){

        $lang =  app()->getLocale();
        $product->{"name:$lang"} = $request->product_name;
        $product->{"description:$lang"} = $request->description;
        $product->user_id = $request->user()->id;
        $product->category_id = $request->category;
        $product->price = $request->price;
        $product->save();

        if ($request->images){
            $ids = [];
            $old_ids_upload_images = [];
            if ($product->images){
                $images_path = $product->images_path;
            }else{
                $images_path = [];
            }
            foreach ($request->file('images') as $key => $image){

                $name = $this->getNameUploadImage($image);

                $file_path = '/files/products/' .$name;

                $upload_image = uploadImages::whereId($product->images[$key] ?? null)->first();

                if ($upload_image){

                    if (in_array($upload_image->url ,$product->images_path )){
                        $array_search_key = array_search($upload_image->url ,$product->images_path );
                        $path = public_path($product->images_path[$array_search_key]);
                        if (\File::exists($path)):
                            \File::delete($path);
                        endif;
                        unset($images_path[$array_search_key]);
                    }
                    $upload_image->update(['url'=> $file_path]);
                }else{

                    $upload_image = uploadImages::create(['url'=> $file_path]);

                }

                $images = collect($product->images)->filter()->toArray();

                if (!in_array($upload_image->id,$images)){

                    $images =  collect($product->images)->filter()->merge($upload_image->id)->flatten();

                    array_push($old_ids_upload_images,$images);
                    array_push($images_path,$upload_image->url);
                }else{
                    $old_ids_upload_images = $images;
                    array_push($images_path,$file_path);
                }

            }

            $product->update(['images' => collect($ids)->merge($old_ids_upload_images)->flatten() ,'images_path' => collect($images_path)->flatten()]);
        }

        return response()->json([  'status' => 200 ,
            'message' => "تم تعديل المنتج بنجاح",
            'url' => route('products.index')
        ]);
    }

    public function listFavourite(){

        $user = Auth::user();

        $products =   $user->filter_my_favorites();

        return view('products.favourite',compact('products'));
    }


    public  function makeFavourite(Request $request) {

        $user = $request->user();

        if(!$user->favorites()->whereProviderId($request->id)->wherePivot('product_id',$request->productId)->first()){

            $user->favorites()->attach($request->id,['product_id'=>$request->productId]);

            $message = trans('global.user_add_favorite');

        }else{

            $user->favorites()->wherePivot('product_id','=',$request->productId)->detach();

            $message = trans('global.user_remove_favorite');

        }

        session()->flash('success',  $message);

        return redirect()->back();
    }

    public function uploadImage(ImageVaid $request){

        return response()->json([  'status' => 200]);
        $image = $request->image;

        $product = Product::findOrFail($request->productId);

        $upload_image = uploadImages::whereId($request->imageId)->first();

        $images_path = $product->images_path;
        foreach ($product->images_path as $key => $value){
            if ($upload_image->url == $value):
                $path = public_path($value);
                if (File::exists($path)):
                    File::delete($path);
                endif;
                unset($images_path[$key]);
            endif;
        }

        $name = time() . '.' . $image->getClientOriginalName();
        $path  = public_path('/files/products/' .$name);
        \Intervention\Image\Facades\Image::make($image)->resize(300, 300)->save($path);

        $product->update(['images_path' => $images_path]);
        $upload_image->update(['url' => '/files/products/' .$name]);
    }

    public function delete_product(Request $request){

        $product = Product::find($request->id);

//        $product->delete();
        return response()->json([
            'status' => true,
            'data' => $product->id
        ]);
    }

    private function getImages($id) {

        $product =    Product::whereId($id)->first();

        $filter =  implode(',',  $product->images) ;
        $ids = explode(',',$filter);

        return   uploadImages::whereIn('id',$ids)->get();
    }

    public function getNameUploadImage($image){
        $name = time() .Str::random(10). '.' . $image->getClientOriginalName();

        $path  = public_path('/files/products/' .$name);

        \Intervention\Image\Facades\Image::make($image)->resize(300, 300)->save($path);

        return $name;
    }
}
