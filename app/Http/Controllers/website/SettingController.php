<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\ContactUsVaild;
use App\Libraries\InsertNotification;
use App\Models\Setting;
use App\Models\Support;
use App\Models\TypesSupport;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    public  $notify;
    public function __construct(InsertNotification $notification)
    {
        $this->middleware(['AuthWebSite'])->only(['sendContactUs']);
        $this->notify = $notification;
    }

    public function aboutUs(){

        $about_us = Setting::getBody('about_us_'. app()->getLocale());

        return view('setting.about',compact('about_us'));
    }

    public function terms(){

        $lang = app()->getLocale();

        $terms = Setting::getBody('terms_user_'. $lang);

        if (Auth::check()){
            $user = Auth::user();

            if ($user->is_user == 1){
                $terms = Setting::getBody('terms_provider_'. $lang);
            }
        }

        return view('setting.terms',compact('terms'));
    }

    public function contactUs(){

        $types = TypesSupport::all();

        return view('setting.contactUs',compact('types'));
    }

    public function sendContactUs(ContactUsVaild $request){

        $user = $request->user();

        $support = new Support();
        $support->user_id = $user->id;
        $support->type_id = (int) $request->typeId;
        $support->name = $user->name;
        $support->phone = $user->phone;
        $support->message = $request->message;
        $support->save();

        $this->notify->NotificationDbType(2,null,$user->id,$request->message);

        session()->flash('success', trans('global.message_was_sent_successfully'));

        return redirect()->back();
    }
}
