<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Http\Requests\Products\StoreOfferVaild;
use App\Models\Offer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OfferController extends Controller
{
    public function __construct()
    {
        $this->middleware(['AuthWebSite','acceptedOrHaveMemberShip']);
    }

    public function index(){
        $user = Auth::user();

        $products = $user->my_products;

        if ($products->count() == 0){
            session()->flash('my_errors',  ' لا يوجد منتجات لإضافة عروض');
            return   redirect()->back();
        }

        return view('offers.index',compact('products'));
    }

    public function store(StoreOfferVaild $request){

        $offer = Offer::create($request->validated());

        return response()->json(['status' => 200,'url' => route('offers.index')]);
    }
}
