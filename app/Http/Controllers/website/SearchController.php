<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\City;
use App\Models\Product;
use App\Models\User;
use App\Scoping\Scopes\CategoryProductScope;
use App\Scoping\Scopes\CategoryScope;
use App\Scoping\Scopes\CityScope;
use App\Scoping\Scopes\OfferScope;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(){

        $categories = Category::whereParentIdAndIsSuspend(0,0)->get();

        $cities = City::whereIsSuspend(0)->get();

        return view('search.index',compact('categories','cities'));
    }


    public function show(Request $request){

        if (in_array($request->search,['product','offer'])){
            $data = $this->get_product_search($request);
        }else{
            $data = $this->get_provider_search($request);
        }

        return view('search.show',compact('data'));
    }

    public function get_provider_search($request){
        $query = User::whereHas('products')->withScopes($this->filter_provider());

        $this->checkIfLoginOrNot($query, $request);

        return $query->select('id','name','image')->paginate();
    }

    public function get_product_search($request){

        return Product::whereIsSuspend(0)
            ->withScopes($this->filter_product())
            ->whereHas('user',function ($user) use ($request){

                $this->checkIfLoginOrNot($user,$request);

            })->with('user_filter','offer')->paginate();
    }

    private function filter_provider(){
        return [
            'categoryId' => new CategoryScope(),
            'cityId'     => new CityScope(),
        ];
    }

    public function filter_product(){
        return [
            'categoryId' => new CategoryProductScope(),
            'cityId'     => new CityScope(),
            'search'     => new OfferScope()
        ];
    }

    private function checkIfLoginOrNot($query, $request){
        if ($request->user()){
            $query->where('id','!=', $request->user()->id);
        }
        $query->where('is_active',1)->where('accepted',1)
            ->where('is_suspend',0)->whereHas('active_member');
    }
}
