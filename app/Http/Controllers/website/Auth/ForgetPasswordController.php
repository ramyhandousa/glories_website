<?php

namespace App\Http\Controllers\website\Auth;

use App\Http\Requests\Auth\changePasswordVaild;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ForgetPasswordController extends Controller
{
    public function index(){

        return view('auth.forgetPass');
    }


    public function getEditPassword(){

        return view('auth.editPass');
    }

    public function editPassword ( Request $request )
    {

        $user = \Auth::user();

        if ( Hash::check( $request->oldPassword , $user->password ) ) {

            $user->update( [ 'password' => $request->newPassword ] );

            return response()->json( [
                'status' => 200 ,
                'message' =>  trans('global.password_was_edited_successfully')  ,
            ] , 200 );

        }
        else {
            return response()->json( [
                'status' => 400 ,
                'error' => (array) trans('global.old_password_is_incorrect')  ,
            ] , 200 );
        }

    }

    public function changePassword(changePasswordVaild $request){

        $user = User::wherePhone($request->phone)->first();

        $user->update(['password' => $request->password]);

        $url =  route('site.login');

        return response()->json([
            'status' => true,
            'message' => ' تم تعديل كلمة المرور بنجاح',
            'url' => $url
        ]);
    }

}
