<?php

namespace App\Http\Controllers\website\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\providerVaildData;
use App\Http\Requests\Auth\SignUpProviderVaild;
use App\Http\Requests\Auth\SignUpUserVaild;
use App\Models\Category;
use App\Models\City;
use App\Models\Provider;
use App\Models\User;
use App\Models\VerifyUser;

class SignUpController extends Controller
{

    public function __construct()
    {
        $this->middleware('loggin');
    }


    public function user_signup_page(){

        $cities = City::whereIsSuspend(0)->get();

        return view('auth.userSignUp',compact('cities'));
    }

    public function provider_signup_page(){

        $cities = City::whereIsSuspend(0)->get();

        $categories = Category::where('parent_id', 0)->whereIsSuspend(0)->get();

        return view('auth.providerSignUp',compact('cities','categories'));
    }

    public function contactFinancial( providerVaildData $request){

        $data  = $request->all();

        $name = $request->name ? $data['name'] : '';
        $phone = $request->phone ? $data['phone'] : '';
        $email = $request->email ? $data['email'] : '';
        $password = $request->password ? $data['password'] : '';
        $cityId = $request->city_id ? $data['city_id'] : '';
        $address_search = $request->address_search ? $data['address_search'] : '';
        $latitute = $request->latitute ? $data['latitute'] : '';
        $longitute = $request->longitute ? $data['longitute'] : '';
        $address = $request->address ? $data['address'] : '';
        $category = $request->category ? $data['category'] : '';

        return view('auth.contact-financial',compact('data','name','phone','email','password',
            'cityId','address_search','latitute','longitute','address','category'));
    }


    public function create_user(SignUpUserVaild  $request){

        $user = User::create($request->validated());

        $this->verify_user($user,$request);

        return response()->json([
            'status' => true,
            'message' => 'تم تسجيل بياناتك بنجاح',
            'url' => route('activationCode')
        ]);
    }

    public function create_provider(SignUpProviderVaild $request){

        $user = User::create($request->validated());

        $this->addProviderSocial($user,$request);

        $user->categories()->attach($request->category);

        $this->verify_user($user,$request);

        return response()->json([
            'status' => true,
            'message' => 'تم تسجيل بياناتك بنجاح',
            'url' => route('activationCode')
        ]);
    }

    private function addProviderSocial($user, $request){
        $provider = new  Provider();
        $provider->user_id  = $user->id;
//        $provider->facebook  = $request->facebook ? 1 : 0;
//        $provider->twitter  = $request->twitter ? 1 : 0;
//        $provider->snap_chat = $request->snap_chat ? 1 : 0;
        $provider->connect_phone = $request->phoneConnect ? 1 :0;
        $provider->connect_email = $request->emailConnect ? 1 :0;
        $provider->bank_transfer = $request->bank ? 1 :0;
        $provider->pay = $request->pay ? 1 :0;
        $provider->cash = $request->cash ? 1 :0;
        $provider->pay_electronic = $request->online ? 1 :0;
        $provider->save();
    }

    function verify_user($user, $request){
        $action_code = substr(rand(), 0, 4);
        $data = [ 'phone' => $request->phone , 'action_code'  => $action_code];
        VerifyUser::updateOrCreate(['user_id' => $user->id,'phone' => $request->phone], $data);
    }
}
