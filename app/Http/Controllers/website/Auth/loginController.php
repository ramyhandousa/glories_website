<?php

namespace App\Http\Controllers\website\Auth;

use App\Http\Requests\Auth\LoginVaild;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class loginController extends Controller
{

    public function __construct()
    {
        $this->middleware('loggin')->except('logOut');
    }

    public function loginIndex(){

        return view('auth.login');
    }

    public function login(LoginVaild $request){

        $user = User::wherePhone($request->phone)->first();

        if ($user->provider()->where('member_id',0)->first()) {

            $url =  route('site.membership');

        }else{

            $url =  route('site.allCategories');
        }

        return response()->json([  'status' => 200 ,'message' => 'تم تسجيل الدخول بنجاح','url' => $url]);

    }


    public function logOut(){

        Auth::logout();

        return redirect(route('site.login'));
    }



}
