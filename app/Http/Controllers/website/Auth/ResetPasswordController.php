<?php

namespace App\Http\Controllers\website\Auth;

use App\Http\Requests\Auth\checkCodeVaild;
use App\Models\User;
use App\Models\VerifyUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResetPasswordController extends Controller
{

    public function activationCode(){

        return view('auth.code');

    }


    public function checkCode(checkCodeVaild $request){

        $verifyUser = VerifyUser::wherePhone($request->phone)->with('user')->first();

        $user = $verifyUser['user'];

        $phone = $request->phone;

        $url = $request->type =='resetPass' ? route('site.forgetPassword',compact('phone')) : route('site.login');

//        $verifyUser->delete();

        $user->update(['is_active'=>1]);

        return response()->json([
            'status' => true,
            'message' => 'الكود صحيح',
            'url' => $url
        ]);
    }


    public function resendCode(Request $request){
        $user = VerifyUser::wherePhone($request->phone)->first();

        if (!$user){
            return response()->json([
                'status' => false,
                'message' => ' رقم الهاتف غير موجود او لم يتم ارسال كود مسبقا'
            ]);
        }

        $user->update(['action_code' => $action_code = substr(rand(), 0, 4)]);

        return response()->json([
            'status' => true,
            'message' => ' تم ارسال الكود'
        ]);
    }




}
