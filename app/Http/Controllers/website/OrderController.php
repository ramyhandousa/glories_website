<?php

namespace App\Http\Controllers\website;

use App\Events\OrderSendNotification;
use App\Http\Controllers\Controller;
use App\Http\Requests\Orders\acceptedOrRefuseVaild;
use App\Http\Requests\Orders\MakeOrderVaild;
use App\Models\City;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['AuthWebSite'])->except('create');
    }

    public function index(Request $request){

        $user = $request->user();

        if ($user->is_user == 1){

            $orders = Order::whereUserId($user->id)->orWhere('provider_id',$user->id)->latest()->cursorPaginate(9);

        }else{

            $orders = Order::whereUserId($user->id)->latest()->cursorPaginate(9);
        }

        $waiting =     $orders->filter(function ($item) {
            return data_get($item, 'status') == 0;
        })->values();

        $current =     $orders->filter(function ($item) {
            return data_get($item, 'status') == 1;
        })->values();

        $finish = $orders->whereIn('status',[-1,2])->values();

        return view('orders.index',compact('waiting','current','finish'));
    }

    public function filter($orders){
        $waiting =     $orders->filter(function ($item) {
            return data_get($item, 'status') == 0;
        })->values();

        $current =     $orders->filter(function ($item) {
            return data_get($item, 'status') == 1;
        })->values();

        $finish = $orders->whereIn('status',[-1,2])->values();
    }

    public function show(Order $order){

        $order->load('product');

        return view('orders.show',compact('order'));
    }

    public function create(Request $request){

        $product = Product::with('user.provider')->findOrFail($request->productId);

        $provider = $product['user'] ? $product['user']['provider'] : null;

        $cities = City::whereIsSuspend(0)->get();

        return view('orders.create',compact('product','provider','cities'));
    }

    public function store(MakeOrderVaild $request){

        $order = Order::create($request->validated());

        event(new OrderSendNotification($request->user(),$order,0));

        return response()->json(['status' => 200 ,'message' => 'تم إضافة طلبك بنجاح' ,
                    'url' =>  route('orders.show',$order->id)
        ]);
    }

    public function accepted_refuse(acceptedOrRefuseVaild $request ,Order $order){

        $status = $request->type  == 1 ? 1 : -1 ;

        $message = $request->type  == 1 ? 'تم قبول الطلب بنجاح': 'تم رفض الطلب بنجاح';

        $status_info = $request->type  == 1 ? 'processing' : 'refuse';

        $order->update(['status' => $status,'status_info' =>  $status_info ,'message' => $request->message,'date_action' =>   Carbon::now()]);

        event(new OrderSendNotification($request->user(),$order,$status));

        session()->flash('success', $message);

        return  redirect()->route('orders.show',$order->id);
    }

    public function finish_order(Request $request,Order $order)
    {
        $order->update(['status' => 2,'status_info' =>  "finish" ,'date_action' =>   Carbon::now()]);

        event(new OrderSendNotification($request->user(),$order,2));

        session()->flash('success',  "تم إنهاء الطلب بنجاح" );

        return  redirect()->route('orders.show',$order->id);
    }

}
