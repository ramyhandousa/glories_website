<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Http\Requests\MemberShip\StoreVaild;
use App\Models\Bank;
use App\Models\BankTransfer;
use App\Models\MemberShip;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MemberShipController extends Controller
{

    public function index(Request $request){

        $memberShips =  MemberShip::whereDate('period_end', '>=', date('Y-m-d'))->get();

        $banks = Bank::whereIsActive(1)->get();

        $transfer_before = BankTransfer::whereUserIdAndStatus($request->user()->id,0)->first();
        if($transfer_before){
            session()->flash('my_errors', 'يوجد تحويل سابق برجاء الإنتظار ..');
            return   redirect()->back();
        }

        return view('memberShip.index',compact('memberShips','banks'));
    }

    public function show(){

        $user = Auth::user();

        $memberShip = $user->member;

        return view('memberShip.show',compact('memberShip'));
    }

    public function my_member_ship(){

        $user = Auth::user();

        $memberShip = $user->member;

        $memberShips =  MemberShip::whereDate('period_end', '>=', date('Y-m-d'))->get();

        $banks = Bank::whereIsActive(1)->get();

        $transfer_before = BankTransfer::whereUserIdAndStatus($user->id,0)->first();

        return view('memberShip.have',compact('user','memberShip','memberShips','banks','transfer_before'));
    }

    public function store(StoreVaild $request){

        $user = $request->user();

        $bank_transfer = BankTransfer::create($request->validated());

        $bank_transfer->update(['image' => $this->upload_image($request)]);

        $user->update(['payment' => 1]);

        $user->provider->update(['member_id' =>$request->memberShip]);

        return response()->json(['status' => 200,'message' => $request->all(),'url' => route('my_member_ship')]);
    }



    public function upload_image($request): string
    {
        $name = time() . '.' . $request->image->getClientOriginalName();
        $path  = public_path('/files/bank_transfer/' .$name);
        \Intervention\Image\Facades\Image::make($request->image)->save($path);
        return  '/files/bank_transfer/' .$name;
    }
}
