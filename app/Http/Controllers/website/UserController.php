<?php

namespace App\Http\Controllers\website;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\updateUserVaild;
use App\Models\City;
use App\Models\Provider;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function my_profile(){

        $user = Auth::user();

        $cities = City::whereIsSuspend(0)->get();

        return view('auth.user_profile',compact('user','cities'));
    }


    public function notifications(Request $request){
        $notification =  $request->user()->notifications;

        $current =     $notification->filter(function ($item) {
            return data_get($item, 'created_at') >= Carbon::today();
        })->values();

        $last =     $notification->filter(function ($item) {
            return data_get($item, 'created_at') < Carbon::today();
        })->values();

        return view('users.notifications',compact('current','last'));
    }

    public function provider_profile()
    {
        $user = Auth::user();
        $user->loadCount(['products','categories']);
        $user->load('provider','member');

        $cities = City::whereIsSuspend(0)->get();

        return view('auth.provider_profile',compact('user','cities'));
    }


    public function update_profile(updateUserVaild $request , User $user)
    {
        $user->fill($request->validated());
        if ($request->file('image')){
            $user->image = $this->upload_image($request, $user);
        }
        $user->save();

        return response()->json(['status' => 200,'message' => 'تم تعديل البيانات بنجاح']);
    }

    public function update_profile_provider(updateUserVaild $request , User $user)
    {
        $user->fill($request->validated());
        if ($request->file('image')){
            $user->image = $this->upload_image($request, $user);
        }
        $user->save();

        $data = [
            'facebook' => $request->facebook ? $request->facebook : $user->provider->facebook,
            'twitter' => $request->twitter ? $request->twitter : $user->provider->twitter,
            'snap_chat' => $request->snapChat ? $request->snapChat : $user->provider->snapChat,
            'connect_phone' =>  $request->phoneConnect ? 1: 0,
            'connect_email' =>  $request->emailConnect ? 1 : 0,
            'bank_transfer' =>  $request->bank ? 1 : 0,
            'pay' =>  $request->pay ? 1 : 0,
            'cash' =>  $request->cash ? 1 : 0,
            'pay_electronic' =>  $request->online ? 1: 0,
        ];
        $user->provider->update($data);

        return response()->json(['status' => 200,'message' => 'تم تعديل البيانات بنجاح']);
    }

    public function getUserById(Request $request){

        $user = User::with('provider','products')->findOrfail($request->id);

        $auth = $request->user();

        $products = $user['products'];

        $products->map(function ($q) use ($user,$auth){
            $q->isFavorite = $auth ? $auth->my_favorites()->wherePivot('product_id',$q->id)->first() ? 1 : 0 :0;
        });

        return view('users.get_user',compact('user','products'));
    }


    public function listFavourite(){

        $user = Auth::user();

        $products =   $user->filter_my_favorites();

        return view('products.favourite',compact('products'));
    }


    public  function makeFavourite(Request $request) {

        $user = $request->user();

        if(!$user->favorites()->whereProviderId($request->id)->wherePivot('product_id',$request->productId)->first()){

            $user->favorites()->attach($request->id,['product_id'=>$request->productId]);

            $message = trans('global.user_add_favorite');

        }else{

            $user->favorites()->wherePivot('product_id','=',$request->productId)->detach();

            $message = trans('global.user_remove_favorite');

        }

        session()->flash('success',  $message);

        return redirect()->back();
    }



    public function upload_image($request ,$user): string
    {
        $name = time() . '.' . $request->image->getClientOriginalName();
        $path  = public_path('/files/profile/' .$name);
        \Intervention\Image\Facades\Image::make($request->image)->resize(300, 300)->save($path);

        if ($user->image){
            $parse_url = parse_url($user->image);
            $path_old = public_path($parse_url['path']);
            if (\File::exists($path_old)):
                \File::delete($path_old);
            endif;
        }
        return  '/files/profile/' .$name;
    }

}
