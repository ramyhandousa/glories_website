<?php

namespace App\Http\Controllers\Api;


use App\Events\OrderSendNotification;
use App\Http\Requests\api\Order\FinishValid;
use App\Http\Requests\api\Order\OrderAccepteOrRefuseValid;
use App\Http\Requests\api\Order\OrderStoreValid;
use App\Http\Resources\Api\Order\OrderShowResource;
use App\Models\Order;
use App\Scoping\Scopes\OrderByScope;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrderController extends MasterApiController
{
    public function __construct(  )
    {
        parent::__construct();

        $this->middleware('auth:api');
    }


    public function index(Request $request){

        $user = $request->user();

        $incomeOrder =  $user->incomeOrder->where('status',0)->values();

        $outOrder  =  $user->outOrder->where('status',0)->values();

        $data = ['incomeOrder' => $incomeOrder , 'outOrder' => $outOrder];

        return $this->success("الطلبات ", $data);
    }

    private function filter_order(){
        return [
            'filter' => new OrderByScope()
        ];
    }

    public function show(Order $order){

        return $this->success("تفاصيل الطلب", new OrderShowResource($order));
    }

    public function store(OrderStoreValid $request){

        return $this->success(trans('global.order_add_successfully'));
    }


    public function acceptedOrRefuse(OrderAccepteOrRefuseValid $request, Order $order){

        $user = $request->user();

        $devices = $order->user->devices?->pluck('device');

        $data = [];

        if ($request->type == -1) {

            $data['message'] = $request->message;
            $data['status'] = -1;
            $data['status_info'] = "refuse";
            $type = 12 ;
            $body_notification = ' تم رفض طلبك رقم '. $order->id . ' من مزود الخدمة ' . $user->name;
            $message_response = trans('global.order_refuse');
        }else{

            $data['status'] = 1;
            $data['status_info'] = "processing";
            $type = 11 ;
            $body_notification = ' تم الموافقةعلي طلبك رقم '. $order->id . ' من مزود الخدمة ' . $user->name;
            $message_response =  trans('global.order_accpted');
        }

        $order->update(array_merge($data,['date_action' =>  Carbon::now()])) ;

        event(new OrderSendNotification($devices,$order,$body_notification,$type));

        return $this->success($message_response);
    }

    public function finish(FinishValid $request , Order $order){

        $user = $request->user();

        $devices = $order->user->devices?->pluck('device');

        $body_notification = ' تم الإنتهاء من طلبك رقم '. $order->id  . ' من مزود الخدمة ' . $user->name;

        $order->update(['status' => 2,'status_info' => 'finish','date_action' =>   Carbon::now()]);

        event(new OrderSendNotification($devices,$order,$body_notification,15));

        return $this->success(trans('global.order_finish'));
    }
}
