<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\User;
use App\Scoping\Scopes\CategoryProductScope;
use App\Scoping\Scopes\CategoryScope;
use App\Scoping\Scopes\CityScope;
use App\Scoping\Scopes\OfferScope;
use Illuminate\Http\Request;

class SearchController extends MasterApiController
{
    public function __invoke(Request $request)
    {
        if (in_array($request->type,['product','offer'])){

            $data = $this->get_product_search($request);

        }else{

            $data = $this->get_provider_search($request);
        }

        return $this->success("نتائج البحث",$data);
    }

    private function get_provider_search($request){
        $query = User::whereHas('products')->withScopes($this->filter_provider());

        $this->checkIfLoginOrNot($query, $request);

        return $query->select('id','name','image')->get();
    }

    private function get_product_search($request){

        return Product::whereIsSuspend(0)
            ->withScopes($this->filter_product())
            ->whereHas('user',function ($user) use ($request){

                $this->checkIfLoginOrNot($user,$request);

            })->with('user_filter','offer')->get();
    }

    private function filter_provider(){
        return [
            'categoryId' => new CategoryScope(),
            'cityId'     => new CityScope(),
        ];
    }

    public function filter_product(){
        return [
            'categoryId' => new CategoryProductScope(),
            'cityId'     => new CityScope(),
            'type'     => new OfferScope()
        ];
    }

    private function checkIfLoginOrNot($query, $request){
        if ($request->user()){
            $query->where('id','!=', $request->user()->id);
        }
        $query->where('is_active',1)->where('accepted',1)
            ->where('is_suspend',0)->whereHas('active_member');
    }
}
