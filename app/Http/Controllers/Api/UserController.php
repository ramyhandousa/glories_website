<?php

namespace App\Http\Controllers\Api;

use App\Http\Helpers\Images;
use App\Http\Requests\api\user\FavouriteValid;
use App\Http\Requests\api\user\rateProviderVaild;
use App\Http\Requests\vaildImage;
use App\Models\uploadImages;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class UserController extends MasterApiController
{

    public function __construct( private UserRepository $userRepository )
    {
        parent::__construct();

        $this->middleware('auth:api')->only(['rateProvider','makeOrUnFavourite','listFavourite','Notification']);
    }

    public function get_provider_by_id(Request $request){

        $data = $this->userRepository->show_provider($request);

        return $this->success("بيانات مزود الخدمة", $data);
    }

    public function rateProvider(rateProviderVaild $request){

       $this->userRepository->rate_provider($request);

        return $this->success(trans('global.rate_success'));
    }

    public function makeOrUnFavourite(FavouriteValid $request){

        $user = $request->user();

        if(!$user->favorites()->whereProviderId($request->providerId)->wherePivot('product_id',$request->productId)->first()){

            $user->favorites()->attach($request->providerId,['product_id'=>$request->productId]);

            return $this->success( trans('global.user_add_favorite'));
        }else {

            $user->favorites()->wherePivot('product_id', '=', $request->productId)->detach();

            return response()->json([
                'status' => 201,
                'message' =>  trans('global.user_remove_favorite')
            ]);
        }

    }

    public function listFavourite(Request $request){

       $data =  $this->userRepository->list_favourite($request);

        return $this->success("المفضلة",$data);
    }

    public function Notification(Request $request){

        $data = $this->userRepository->list_Notification($request);

        return $this->success("الإشعارات", $data);
    }

    public function upload_image(vaildImage $request){

        $my_upload = Images::uploadImage($request,'image','/files/uploads/');

        $image = uploadImages::create(['url' =>  '/files/uploads/' .$my_upload]);

       return $this->success("البيانات",$image);
    }

    public function remove_image(Request $request){

        if ($request->image && File::delete(public_path().$request->image)){

            $image = uploadImages::whereUrl($request->image)->first();

            if ($image){
                $image->delete();
            }
            return $this->success('تم مسح الملف بنجاح');
        }
        return $this->failure('غالبا مسار الملف غير صحيح');
    }

}
