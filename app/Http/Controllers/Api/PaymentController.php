<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\myfatoorah;
use App\Models\Order;
use Illuminate\Http\Request;

class PaymentController extends MasterApiController
{

    public function __construct( private myfatoorah $myfatoorah)
    {
        parent::__construct();

        $this->middleware("auth:api")->only(['getPaymentMethods','excutePayment']);

    }

    public function getPaymentMethods(Request $request){

        $response = $this->myfatoorah->listPaymentMethod($request);

        if ($response['IsSuccess']){

            return  $this->success( "   إختر طريقة الدفع ",$response['Data']['PaymentMethods'] ) ;

        }else{

            return  $this->failure( 'يوجد خطأ ما اثناء عميلة الدفع '  ) ;
        }
    }

    public function excutePayment(Request $request){

        $response = $this->myfatoorah->excutePaymentMethod($request);

        if ($response['IsSuccess']){

            $url = $response['Data']['PaymentURL'].'&orderId=' .  $request->orderId;

            return  $this->success( "الذهاب الي اللينك مباشرة لدفع ",$url ) ;

        }else{

            return  $this->failure( 'يوجد خطأ ما اثناء عميلة الدفع '   ) ;
        }
    }


    public function success_payment(Request $request){

        $order = Order::whereId($request->orderId)->first();
        $order->is_paid = 1;
        if ($request->paymentId){
            $order->payment_type = 4;
            $order->paymentId = $request->paymentId;
        }
        $order->save();

        return view('payment.payment');
    }


    public function errors(Request $request){

        return view('payment.errorPayment');
    }
}
