<?php

namespace App\Http\Controllers\Api;


use App\Http\Requests\api\category\storeVaild;
use App\Http\Requests\api\category\updateVaild;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryController extends MasterApiController
{
    public function __construct( private CategoryRepository $categoryRepository)
    {
        parent::__construct();

        $this->middleware('auth:api')->only(['store' ,'createOrEditCategory']);
    }


    public function index(Request $request){

        $data = $this->categoryRepository->list($request);

        return $this->success('ok',$data);
    }


    public function store(storeVaild $request){

        $data = $this->categoryRepository->store($request);

        return $this->success(trans('global.category_add_success'),$data);
    }

    public function createOrEditCategory(updateVaild $request){

        $data = $this->categoryRepository->update_categories($request);

        return $this->success(trans('global.category_add_success'),$data);
    }
}
