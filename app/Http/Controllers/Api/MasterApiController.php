<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\RespondsWithHttpStatus;

class MasterApiController extends Controller
{
    use RespondsWithHttpStatus;

    public function __construct( )
    {
        app()->setLocale(request()->headers->get('lang') ?  : 'ar');
    }
}
