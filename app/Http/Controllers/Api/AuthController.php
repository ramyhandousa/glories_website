<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\api\Auth\changePassRequest;
use App\Http\Requests\api\Auth\changePhoneVaild;
use App\Http\Requests\api\Auth\checkActivation;
use App\Http\Requests\api\Auth\checkPhoneOrEmail;
use App\Http\Requests\api\Auth\checkPhoneOrEmailExist;
use App\Http\Requests\api\Auth\editUser;
use App\Http\Requests\api\Auth\login;
use App\Http\Requests\api\Auth\registerVaild;
use App\Http\Requests\api\Auth\resendCodeValid;
use App\Repositories\AuthRepository;
use Illuminate\Http\Request;

class AuthController extends MasterApiController
{

    public function __construct( private AuthRepository $authRepository)
    {
        parent::__construct();

        $this->middleware('auth:api')
            ->only(['changPassword' ,'changePhone','editProfile','logOut','refreshToken']);
    }


    public function register(registerVaild  $request){

        $code = $this->random_code_active();

        $this->authRepository->register($request ,$code );

        return $this->success_code_activation(  trans('global.activation_code_sent'),$code);
    }

    public function checkPhoneOrEmail(checkPhoneOrEmail $request){

        return $this->success('ok');
    }

    public function login(login  $request){

        $data =   $this->authRepository->login($request);

        return $this->success(trans('global.logged_in_successfully'),$data);
    }

    public function forgetPassword(checkPhoneOrEmailExist $request){

        $code = $this->random_code_active();

         $this->authRepository->forgetPassword($request,$code);

        return $this->success_code_activation(__('global.activation_code_sent'), $code);
    }

    public function resetPassword(checkPhoneOrEmailExist $request){

        $data =    $this->authRepository->resetPassword($request);

        return $this->success( trans('global.password_was_edited_successfully'),$data);
    }

    public function resendCode(resendCodeValid $request){

        $code = $this->random_code_active();

        $this->authRepository->resendCode($request, $code);

        return $this->success_code_activation(  trans('global.activation_code_sent'), $code);
    }

    public function checkCodeActivation(checkActivation $request){

        $data =    $this->authRepository->checkCode($request);

        return $this->success(  trans('global.your_account_was_activated'),$data);
    }

    public function checkCodeCorrect(checkActivation $request){

        return $this->success('الكود صحيح');
    }

    public function changPassword ( changePassRequest  $request )
    {
        $data =    $this->authRepository->changPassword($request);

        return $this->success(  $data['message']);
    }

    public function changePhone(changePhoneVaild $request){

        $code = $this->random_code_active();

        $this->authRepository->change_phone($request, $code);

        return $this->success_code_activation(  trans('global.phone_edit_success') ,$code);
    }

    public function editProfile (editUser $request )
    {
        $code = $this->random_code_active();

        $data =  $this->authRepository->editProfile($request,$code);

        return $this->success(  trans('global.profile_edit_success') ,$data);
    }


    public function logOut(Request  $request){
        $this->authRepository->logOut($request);

        return $this->success(  trans('global.logged_out_successfully'));
    }

}
