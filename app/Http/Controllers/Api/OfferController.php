<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\offer\offerDeleteVaild;
use App\Http\Requests\api\offer\offerStoreVaild;
use App\Http\Requests\api\offer\offerUpdateVaild;
use App\Models\Offer;
use App\Repositories\OfferRepository;
use Illuminate\Http\Request;

class OfferController extends MasterApiController
{
    public function __construct( private OfferRepository $offerRepository)
    {
        parent::__construct();

        $this->middleware('auth:api')->except(['index','show']);
    }

    public function index(Request $request){

        $data = $this->offerRepository->list($request);

        return response()->json( [
            'status' => 200 ,
            'countOffer' => $data['countOffer'],
            'countOfferRemaining' => $data['countOfferRemaining'],
            'data' => $data['data'] ,
        ] , 200 );
    }

    public function show(){

    }

    public function store(offerStoreVaild $request){

        $data = $this->offerRepository->store($request);

        return $this->success(trans('global.offer_add'), $data);
    }

    public function update(offerUpdateVaild $request , Offer $offer){

        $data = $this->offerRepository->update($request, $offer);

        return $this->success(trans('global.offer_edit'), $data);
    }

    public function destroy(offerDeleteVaild $request ,Offer $offer){

         $this->offerRepository->cancel_offer($offer);

        return $this->success(trans('global.offer_delete'));
    }

}
