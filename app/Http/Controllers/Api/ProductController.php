<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\product\deleteVaild;
use App\Http\Requests\api\product\storeVaild;
use App\Http\Requests\api\product\updateVaild;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;

class ProductController extends MasterApiController
{
    public function __construct( private ProductRepository $productRepository)
    {
        parent::__construct();

        $this->middleware('auth:api')->except(['index','show']);
    }

    public function index(Request $request){

        $data =  $this->productRepository->list($request);

        return response()->json( [
            'status' => 200 ,
            'countProduct' => $data['countProduct'],
            'countProductRemaining' => $data['countProductRemaining'],
            'data' =>  $data['data'] ,
        ] , 200 );
    }

    public function show(Request $request,Product $product){

        $data =  $this->productRepository->show($request , $product);

        return $this->success("بيانات المنتج", $data);
    }

    public function store(storeVaild $request){

        $data = $this->productRepository->store($request);

        return  $this->success(trans('global.product_added'),$data);
    }

    public function update(updateVaild $request,Product $product){

         $data = $this->productRepository->update($request,$product);

        return  $this->success(trans('global.product_edit'),$data);
    }

    public function destroy(deleteVaild $request , Product $product){

        $this->productRepository->delete($product);

        return  $this->success(trans('global.product_delete'));
    }

}
