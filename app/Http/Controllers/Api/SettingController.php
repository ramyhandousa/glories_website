<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\api\setting\BankTransferVaild;
use App\Http\Requests\api\setting\ContactUsVaild;
use App\Models\Bank;
use App\Models\BankTransfer;
use App\Models\City;
use App\Models\Setting;
use App\Models\Support;
use App\Models\TypesSupport;
use App\Models\User;
use Illuminate\Http\Request;

class SettingController extends MasterApiController
{

    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth:api')->only(['contactUs','bankTransfer']);
    }

    public function cities(){

        $data = City::whereIsSuspend(0)->get(['id']);

        return $this->success('المدن',$data);
    }

    public function aboutUs(){

        $lang = app()->getLocale();
        $about_us =  Setting::getBody('about_us_' . $lang);
        $facebook =  Setting::getBody('faceBook');
        $twitter =  Setting::getBody('twitter');
        $snapChat =  Setting::getBody('snapChat');
        $data = ['about_us' => $about_us , 'facebook' => $facebook , 'twitter' => $twitter , 'snapChat' => $snapChat ];

        return $this->success('عن التطبيق',$data);
    }

    public function terms(Request $request){

        $user = User::whereApiToken($request->headers->get('apiToken'))->first();

        $lang = app()->getLocale();

        $terms = Setting::getBody('terms_user_'. $lang);

        if ($user){
            if ($user->is_user == 1){
                $terms = Setting::getBody('terms_provider_'. $lang);
            }
        }

        return $this->success('الشروط والأحكام',$terms);
    }

    public function banks(){

        $data = Bank::whereIsActive(1)->get(['id','image','acount_number']);

        return $this->success('البنوك',$data);
    }

    public function getTypes(){

        $data = TypesSupport::whereIsSuspend(0)->get(['id']);

        return $this->success('أنواع الرد',$data);
    }


    public function contactUs(ContactUsVaild $request){
        $user = $request->user();

        $support = new Support();
        $support->user_id = $user->id;
        $support->type_id = $request->typeId;
        $support->name = $user->name;
        $support->phone = $user->phone;
        $support->message = $request->message;
        $support->save();

        return $this->success(trans('global.message_was_sent_successfully'));
    }

    public function bankTransfer(BankTransferVaild $request){

        $user = $request->user();

        $bank_transfer = BankTransfer::create($request->validated());

        $bank_transfer->update(['image' => $this->upload_image($request)]);

        $user->update(['payment' => 1]);

        $user->provider->update(['member_id' =>$request->memberShip]);

        return $this->success("تم التحويل بنجاح");
    }

    public function upload_image($request): string
    {
        $name = time() . '.' . $request->image->getClientOriginalName();
        $path  = public_path('/files/bank_transfer/' .$name);
        \Intervention\Image\Facades\Image::make($request->image)->save($path);
        return  '/files/bank_transfer/' .$name;
    }

}
