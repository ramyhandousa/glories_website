<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\MemberShip\ChooseMemberValid;
use App\Http\Requests\api\MemberShip\registerMemberValid;
use App\Http\Resources\Api\MemberShip\ListResource;
use App\Models\MemberShip;
use App\Models\MemberSuperscription;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class MemberShipController extends MasterApiController
{
    public function __construct(  )
    {
        parent::__construct();

        $this->middleware('auth:api')->except(['index']);
    }


    public function index(){

        $members = MemberShip::whereDate('period_end', '>=', date('Y-m-d'))->get();

        $data = ListResource::collection($members);

        return $this->success("العضويات" , $data);
    }

    public function chooseMember(ChooseMemberValid $request){

        $user = $request->user();

        $user->provider->update(['member_id' => $request->memberId]);

        return response()->json( [
            'status' => 200 ,
            'data' => (int) $request->memberId,
            'message' => trans('global.member_ship_choose'),
        ] , 200 );
    }

    public function registerMember(registerMemberValid $request){

        $user = $request->user();

        $member =   MemberShip::whereId($request->memberId)->first();

        $time = $this->get_time_memberShip($member);

        $providerData =   MemberSuperscription::create( $this->dataMember($user,$member,$time));

        return  $this->success(trans('global.member_ship_register'),$providerData );
    }

    private function get_time_memberShip($member){
        $period_start = $member->period_start;
        $period_end = $member->period_end;
        $datetime1 = new \DateTime($period_start);
        $datetime2 = new \DateTime($period_end);

        //  $interval  => this to know diff between period_start and   period_end from DB
        $interval = $datetime1->diff($datetime2);
        //  now add diff days in current time to know member will be end in this time
        $days = $interval->format('%a');
        $year = $interval->format('%y')  ;

        return Carbon::now()->addYear($year)->addDays($days)->toDateString();
    }

    private function  dataMember($user, $member, $time){
        return [
            'user_id' => $user->id,
            'member_id' => $member->id,
            'name' => $member->name,
            'price' => $member->price,
            'number_product' => $member->number_product,
            'number_images' => $member->number_images,
            'number_services_enjoy' => $member->number_services_enjoy,
            'number_category' => $member->number_category,
            'period_start' => $member->period_start,
            'period_end' => $member->period_end,
            'member_end' => $time,
        ];
    }

}
