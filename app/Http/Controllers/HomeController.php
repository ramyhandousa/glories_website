<?php

namespace App\Http\Controllers;


class HomeController extends Controller
{

    public function index(){

        return view('welcome');
    }


    public function intro(){
        return view('auth.intro');
    }
}
