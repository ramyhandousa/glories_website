<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;

class AuthLoginWebsite
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $setting = new Setting();
        if (auth()->check()) {

            if (auth()->user()->is_suspend == 1) {

                $reason = auth()->user()->message;

                Auth::logout();

                session()->flash('errors', __('trans.suspendBecause', ['phone' => $setting->getBody('phone_contact'), "reason" => $reason]));

                return redirect(route('site.login'));

            }

            if (auth()->user()->is_user == 1 &&  auth()->user()->payment == 0){

                session()->flash('errors', 'برجاء دفع العضوية.');
                return redirect(route('site.membership'));
            }



            return $next($request);

        }else


        session()->flash('errors', 'يجب تسجيل الدخول اولا.');


        return redirect(route('site.login'));

    }
}
