<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserIsLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {

            return $next($request);

        } else {

             session()->flash('errors', 'يجب تسجيل الدخول اولا.');

            return redirect( )->back();

        }
    }
}
