<?php

namespace App\Http\Middleware;

use App\Models\MemberSuperscription;
use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class checkAccptedOrMemberShip
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if (auth()->check() ){

            if (User::whereId($user->id)->where('accepted',0)->first()){

//            if($request->ajax()){
//                return "AJAX";
//            }
                session()->flash('my_errors', "يجب انتظار موافقة الادمن لدخول الصفحة");

                return redirect()->back();
            }

            if (!MemberSuperscription::whereUserId($user->id)->where('is_suspend',0)->first()){

                session()->flash('my_errors', "ليس لديك عضوية لدخول الصفحة");

                return redirect()->back();
            }

            return $next($request);
        }


        return $next($request);
//         else
//            return redirect(route('site.login'));

    }
}
