<?php

namespace App\Http\Requests\api\MemberShip;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ChooseMemberValid extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'memberId' => 'required|exists:member_ships,id',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $user = $this->user();

            if($user->payment == 1){
                $validator->errors()->add("errors",trans('global.valid_memberShip_other'));
                return;
            }

        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
