<?php

namespace App\Http\Requests\api\setting;

use App\Models\BankTransfer;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class BankTransferVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'userName'          => 'required|max:255',
            'userAccount'       => 'required|max:255',
            'iban'              => 'required|max:255',
            'money'             => 'required|max:255',
            'bankId'           => 'required|exists:banks,id',
            'member_ship_id'    => 'required|exists:member_ships,id',
            'image'             => 'required|mimes:jpeg,png,jpg|max:500000'
        ];
    }

    public function messages()
    {
        return [
            'userName.required' => 'مطلوب   اسم المحول  من فضلك ',
            'userAccount.required' => 'مطلوب   رقم حساب المحول  من فضلك ',
            'iban.required' => 'مطلوب  رقم ابيان التحويل   من فضلك ',
            'money.required' => 'مطلوب   قيمة التحويل  من فضلك ',
            'bank_name.required' => 'مطلوب  البنك المحول منه   من فضلك ',
            'member_ship_id.required' => 'مطلوب العضوية  من فضلك ',
            'image.required' => 'مطلوب إيصال التحويل  من فضلك ',
        ]; // TODO: Change the autogenerated stub
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $user = $this->user();

            $transfer_before = BankTransfer::whereUserIdAndStatus($user->id,0)->first();

            if($transfer_before){
                $validator->errors()->add("errors",'يوجد تحويل سابق برجاء الإنتظار ..');
                return;
            }

            if($user->active_member){
                $validator->errors()->add("errors",trans('global.member_ship_have'));
                return;
            }

            if($user->payment == 1){
                $validator->errors()->add("errors",trans('global.valid_memberShip_other'));
                return;
            }

        });
    }

    public function validated()
    {
        $data =   parent::validated(); // TODO: Change the autogenerated stub
        $data['user_id'] = $this->user()->id;
        $data['bank_id'] = $this->bankId;
        return collect($data)->except(['image','bankId'])->toArray();
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
