<?php

namespace App\Http\Requests\api\product;

use Illuminate\Foundation\Http\FormRequest;

class deleteVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = $this->user();

        if ($user->id == $this->route('product')->user_id){

            return  true;
        }else{

            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
