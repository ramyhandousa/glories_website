<?php

namespace App\Http\Requests\api\user;

use App\Models\Rating;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class rateProviderVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'providerId' => 'required|exists:users,id',
            'orderId' => 'required|exists:orders,id',
            'rateValue' => 'required|numeric|min:1|max:5'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $user = $this->user();

            $rate_before = Rating::whereUserIdAndOrderIdAndRateableId($user->id,$this->orderId,$this->providerId)->first();

            if($rate_before){
                $validator->errors()->add("errors",trans('global.provider_rate_before'));
                return;
            }

        });
    }


    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
