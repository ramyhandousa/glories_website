<?php

namespace App\Http\Requests\api\Auth;

use App\Models\VerifyUser;
use App\Rules\checkCodeActivation;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\DB;

class checkActivation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone'       => 'required',
            'code'        => ['required', 'exists:verify_users,action_code' , new checkCodeActivation()],
//            'code'        => ['required', 'exists:verify_users,action_code' ],
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => trans('global.required'),
            'email.email' => trans('validation.email'),
            'phone.exists' =>  trans('global.user_not_found'),
            'code.required' => 'إدخل الكود من فضلك',
            'code.exists' =>  trans('global.activation_code_not_correct'),
        ];
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
