<?php

namespace App\Http\Requests\api\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class registerVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:75',
            'phone'         => 'required|unique:users|numeric',
            'email'         => 'nullable|email|unique:users',
            'password'      => 'required|min:6|max:191',
            'cityId'        => 'required|exists:cities,id',
            'address'       => 'required|max:191',
            'latitute'      => 'required|max:191',
            'longitute'     => 'required|max:191',
            'userType'      => 'nullable',
            'facebook'      => 'nullable|max:191',
            'twitter'       => 'nullable|max:191',
            'snapChat'      => 'nullable|max:191',
            'contectPhone'  => 'nullable|boolean',
            'contectEmail'  => 'nullable|boolean',
            'bankTransfer'  => 'nullable|boolean',
            'pay'           => 'nullable|boolean',
            'cash'          => 'nullable|boolean',
            'payElectronic' => 'nullable|boolean',
            'memberId'      => 'exists:member_ships,id',
            'categoryId'    => 'exists:categories,id',
            'image'         => 'nullable|mimes:jpeg,png,jpg|max:30240'
        ];
    }


    public function validated()
    {
        $data =  parent::validated();

        $master_data = collect($data)->only(['name','phone','email','password','address','latitute','longitute']);
        $master_data['city_id'] = $this->cityId;
        $master_data['is_user'] = $this->userType ?:0;

        $data_user = collect($master_data)->toArray();

        $data_provider = collect($data)->only(['facebook','twitter','pay','cash']);
        $data_provider['snap_chat']         = $this->snapChat ?  : null;
        $data_provider['connect_phone']     = $this->contectPhone ?  : 0;
        $data_provider['connect_email']     = $this->contectEmail ?  : 0;
        $data_provider['bank_transfer']     = $this->bankTransfer ?  : 0;
        $data_provider['pay_electronic']    = $this->payElectronic ?  : 0;
        $data_provider['member_id']         =  $this->memberId ?  : null;

        return ['user' => $data_user ,'provider' => collect($data_provider)->toArray()];
    }

    protected function failedValidation(Validator $validator) {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
