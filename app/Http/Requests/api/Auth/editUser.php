<?php

namespace App\Http\Requests\api\Auth;

use App\Rules\checkOldPass;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class editUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        return [
            'name'          => 'max:75',
            'phone'         => 'numeric|digits_between:9,15|unique:users,phone,' . $user->id ,
            'email'         => 'email|unique:users,email,' . $user->id ,
            'cityId'        => 'nullable|exists:cities,id',
            'address'       => 'nullable|max:191',
            'latitute'      => 'nullable|max:191',
            'longitute'     => 'nullable|max:191',
            'userType'      => 'nullable',
            'facebook'      => 'nullable|max:191',
            'twitter'       => 'nullable|max:191',
            'snapChat'      => 'nullable|max:191',
            'contectPhone'  => 'nullable|boolean',
            'contectEmail'  => 'nullable|boolean',
            'bankTransfer'  => 'nullable|boolean',
            'pay'           => 'nullable|boolean',
            'cash'          => 'nullable|boolean',
            'payElectronic' => 'nullable|boolean',
//            'memberId'      => 'exists:member_ships,id',
//            'categoryId'    => 'exists:categories,id',
            'image'         => 'nullable|mimes:jpeg,png,jpg|max:30240'
        ];
    }


    public function messages()
    {
        return [
            'email.unique' => trans('global.unique_email'),
            'email.email' => trans('validation.email'),
            'phone.unique' => trans('global.unique_phone'),
        ];
    }

    public function validated()
    {
        $data =  parent::validated();

        $master_data = collect($data)->only(['name','email','password','address','latitute','longitute']);
        $master_data['city_id'] = $this->cityId;

        $data_user = collect($master_data)->toArray();

        $data_provider = collect($data)->only(['facebook','twitter','pay','cash']);

        (!is_null($this->snapChat)) ? ($data_provider['snap_chat'] = $this->snapChat) : "";
        (!is_null($this->contectPhone)) ? ($data_provider['connect_phone'] = $this->contectPhone) : "";
        (!is_null($this->contectEmail)) ? ($data_provider['connect_email'] = $this->contectEmail) : "";
        (!is_null($this->bankTransfer)) ? ($data_provider['bank_transfer'] = $this->bankTransfer) : "";
        (!is_null($this->payElectronic)) ? ($data_provider['pay_electronic'] = $this->payElectronic) : "";
//        $data_provider['member_id']         =  $this->memberId ?  : null;

        return ['user' => $data_user ,'provider' => collect($data_provider)->toArray()];
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
