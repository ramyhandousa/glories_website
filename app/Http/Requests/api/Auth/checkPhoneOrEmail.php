<?php

namespace App\Http\Requests\api\Auth;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class checkPhoneOrEmail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	      'email' => 'nullable|unique:users|max:75',
	      'phone' => 'nullable|unique:users|numeric',
        ];
    }

       public function messages()
       {
	    return [

		  'email.unique' => trans('global.unique_email'),
		  'phone.unique' => trans('global.unique_phone'),
		  'phone.required' => trans('global.required'),

	    ];
       }


       protected function failedValidation(Validator $validator) {

	    $values = $validator->errors()->all();

	    throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
       }
}
