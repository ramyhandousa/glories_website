<?php

namespace App\Http\Requests\api\Auth;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class login extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'phone' => 'required',
            'password' => 'required',
        ];
    }

   public function messages()
   {
    return [
      'phone.required' => trans('global.required'),
      'password.required' => trans('global.required'),
    ];
   }


    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            if ($this->phone && $this->password){

                if (! $token = auth()->attempt(['phone' => $this->phone, 'password' => $this->password])) {

                    $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
                    return;
                }else{
                    $user = $this->user();

                    if ($user->is_suspend == 1){
                        $validator->errors()->add('unavailable', trans('global.your_account_suspended'));
                        return;
                    }
                }
            }

        });
    }

      protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
