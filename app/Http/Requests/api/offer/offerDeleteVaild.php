<?php

namespace App\Http\Requests\api\offer;

use Illuminate\Foundation\Http\FormRequest;

class offerDeleteVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $offer = $this->route('offer');
        $user = $this->user();
        if ($user->id == $offer->product->user_id){
            return  true;
        }else{
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
