<?php

namespace App\Http\Requests\Products;

use App\Models\Product;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreOfferVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id'    => 'required|exists:products,id',
            'price'         => 'required|numeric',
            'period_start'  => 'required|date|date_format:Y-m-d|after:today',
            'period_end'    => 'required|date|date_format:Y-m-d|after:period_start',
        ];
    }

    public function messages()
    {
        return [
            'price.required' => 'من فضلك إدخل سعر العرض',
            'price.numeric' => 'سعر العرض يجب ان يكون رقما ولا يحتوي علي اي حروف',
            'period_start.required' => 'تاريخ بداية العرض',
            'period_end.required' => 'تاريخ نهاية العرض  ',
            'period_start.date_format' => 'تأكد من صيغة تاريخ بداية العرض',
            'period_start.after' => ' تأكد من  تاريخ بداية العرض بعد اليوم',
            'period_end.date_format' => 'تأكد من صيغة تاريخ نهاية العرض',
            'period_end.after' => 'تأكد من أن نهاية العرض بعد بداية تاريخ بداية العرض',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $product = Product::with('active_offer')->find($this->product_id);

            if ($this->price > $product->price){
                $validator->errors()->add("errors"," برجاء التأكد من سعر العرض لانه اكبر من سعر المنتج وهو " .$product->price);
                return;
            }

            if ($product->active_offer){
                $validator->errors()->add("errors"," لديك عرض مسبقا علي هذا المنتج");
                return;
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
