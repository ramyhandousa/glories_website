<?php

namespace App\Http\Requests\Auth;

use App\Models\VerifyUser;
use App\Rules\checkCodeActivation;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class checkCodeVaild extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone'       => 'required|exists:verify_users',
        ];
    }

    public function messages()
    {
        return [
            'phone.required'    => trans('global.required'),
            'phone.exists'      =>  'هذا التليفون غير موجود لدينا',
            'code.required'     => 'إدخل الكود من فضلك',
            'code.exists'       =>  trans('global.activation_code_not_correct'),
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $code = $this->code1 . $this->code2 . $this->code3 . $this->code4;

            if (!$code) {

                $validator->errors()->add('unavailable', 'إدخل الكود من فضلك');
            }
            if (!VerifyUser::wherePhoneAndActionCode($this->phone,$code)->first()) {

                $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
