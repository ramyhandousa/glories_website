<?php

namespace App\Http\Requests\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class providerVaildData extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|max:191',
            'phone'     => 'required|unique:users',
            'email'     => 'required|unique:users',
            'address'      => 'required|max:191',
            'latitute'      => 'required|max:191',
            'longitute'      => 'required|max:191',
            'password'  => 'required|min:6|max:191|same:password_confirmation',
            'password_confirmation'   => 'min:6',
            'city_id'   => 'exists:cities,id',
            'category'   => 'exists:categories,id',
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => trans('global.unique_email'),
            'email.email' => trans('validation.email'),
            'phone.unique' => trans('global.unique_phone'),
            'address.required' => "برجاء التأكد من إختيار العنوان من علي الخريطة",
            'latitute.required' => "برجاء التأكد من إختيار العنوان من علي الخريطة",
            'longitute.required' => "برجاء التأكد من إختيار العنوان من علي الخريطة",
            'password.same' => "برجاء التأكد من كلمات المرور متساوية",
        ];
    }

//
//    protected function failedValidation(Validator $validator)
//    {
//        $values = $validator->errors()->all();
//
//        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
//    }
}
