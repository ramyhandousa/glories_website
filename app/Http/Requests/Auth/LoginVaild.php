<?php

namespace App\Http\Requests\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class LoginVaild extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'phone' => 'required',
            'password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => trans('global.required'),
            'password.required' => trans('global.required'),
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            if (! $token = auth()->attempt(['phone' => $this->phone, 'password' => $this->password])) {

                $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
                return;
            }else{
                $user =  Auth::user();

                if ($user->is_suspend == 1){
                    $validator->errors()->add('unavailable', trans('global.your_account_suspended'));
                    return;
                }

                if ($user->is_active == 0){
                    $validator->errors()->add('unavailable', trans('global.your_account_not_activated_yet'));
                    return;
                }
            }


        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}
