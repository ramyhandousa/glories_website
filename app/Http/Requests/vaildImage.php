<?php

namespace App\Http\Requests;


class vaildImage extends MasterFormRequestApi
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'image'     => 'required|image|mimes:jpeg,png,jpg|max:30240'
        ];
    }
}
