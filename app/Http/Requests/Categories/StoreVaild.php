<?php

namespace App\Http\Requests\Categories;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

class StoreVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required|exists:categories,id'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $user = $this->user();

            $check = DB::table('category_user')
                ->whereUserId($user->id)
                ->where('category_id',$this->category)->count();

            if ($check){
                $validator->errors()->add('unavailable',  "لديك هذا القسم سابقا");
                return ;
            }
        });
    }

}
