<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class checkOldPass implements Rule
{

    protected $message;

    public function __construct( )
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = Auth::user();

        if (!Hash::check( $value , $user->password )){

            return $this->fail(trans('global.old_password_is_incorrect'));
        }

        return true;
    }

    protected function fail($message){

        $this->message = $message;

        return false;
    }

    public function message()
    {
        return $this->message;
    }
}
