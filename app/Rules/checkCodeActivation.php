<?php

namespace App\Rules;

use App\Models\VerifyUser;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\DB;

class checkCodeActivation implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $columnDatabase = 'phone';
        // E-mail Or phone
            $requestPass = request()->phone;

        return VerifyUser::where($columnDatabase, $requestPass)->where('action_code',$value)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('global.activation_code_not_correct') ;
    }
}
